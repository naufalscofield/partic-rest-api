<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $user = Auth::user();
        // dd($user);
        if($user) {
            foreach($roles as $role) {
                
                if($role == "ceo" && $user->Ceo()) {
                    return $next($request);
                }

                if($role == "regional" && $user->Regional()) {
                    return $next($request);
                }

                if($role == "cabang" && $user->Cabang()) {
                    return $next($request);
                }

                if($role == "mitra" && $user->Mitra()) {
                    return $next($request);
                }

                if($role == "member" && $user->Member()) {
                    return $next($request);
                }
                
            }
        }

        return response()->json([
            'code' => 500,
            'message' => 'Permission denied! '.$user->role
        ], 500);
    }
}
