<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Exceptions\Handler;
use App\Services\InternalService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;
Use Symfony\Component\HttpFoundation\File\UploadedFile;

class PerusahaanController extends Controller
{
    use ApiResponser;
    public $InternalService;

    public function __construct(InternalService $InternalService)
    {
        $this->InternalService = $InternalService;
    }

  /**
    * Verify an email using email and token from email.
    *
    * @param  Request  $request
    * @return Response
    */
   

    public function store(Request $request)
    {
            $foto_diri          = $request->file('file_foto_diri');
            $foto_diri_name     = $request->foto_diri;
            $path_foto_diri     = storage_path('/app/images/foto_diri');
            $foto_diri->move($path_foto_diri, $foto_diri_name);

            $foto_ktp          = $request->file('file_foto_ktp');
            $foto_ktp_name     = $request->foto_ktp;
            $path_foto_ktp     = storage_path('/app/images/foto_ktp');
            $foto_ktp->move($path_foto_ktp, $foto_ktp_name);

            $foto_npwp          = $request->file('file_foto_npwp');
            $foto_npwp_name     = $request->foto_npwp;
            $path_foto_npwp     = storage_path('/app/images/foto_npwp');
            $foto_npwp->move($path_foto_npwp, $foto_npwp_name);

            $logo               = $request->file('file_logo');
            $logo_name          = $request->logo;
            $path_logo          = storage_path('/app/images/logo');
            $logo->move($path_logo, $logo_name);
     
        //Insert Perusahaan
        $dataPerusahaan = [
            'nama_perusahaan'   => $request->nama_perusahaan,
            'alamat'            => $request->alamat_perusahaan,
            'email'             => $request->email_perusahaan,
            'no_telp'           => $request->no_telp_perusahaan,
            'logo'              => $request->logo,
        ];

        $storePerusahaan = json_decode($this->InternalService->storePerusahaan($dataPerusahaan));
        
        //Insert User
        $user = new User;
        
        $user->email            = $request->email;
        $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $user->id_perusahaan    = $storePerusahaan->id;
        $user->id_regional      = NULL;
        $user->id_cabang        = NULL;
        $user->role             = 'ceo';
        $user->verifikasi       = 2;
        $user->email_token      = $request->email_token;

        $user->save();
        
        //Insert Biodata
        $biodata = new Biodata;
        
        $biodata->id_user          = $user->id;
        $biodata->nama_lengkap     = $request->nama_lengkap;
        $biodata->tanggal_lahir    = $request->tanggal_lahir;
        $biodata->tempat_lahir     = $request->tempat_lahir;
        $biodata->no_telp          = $request->no_telp;
        $biodata->alamat           = $request->alamat;
        $biodata->foto_ktp         = $request->foto_ktp;
        $biodata->foto_npwp        = $request->foto_npwp;
        $biodata->foto_diri        = $request->foto_diri;
        $biodata->nik              = $request->nik;

        $biodata->save();

        //Data Response
        $resp = [
            'DataPerusahaan'    => $storePerusahaan,
            'DataUser'          => $user,
            'DataBiodata'       => $biodata,
        ];

        return response()->json($resp,200);
    }

    public function showPerusahaan(Request $request, $id)
    {
        //Show Regional
        $showUser = User::find($id);
        $show = json_decode($this->InternalService->showPerusahaan($showUser->id_perusahaan));
        
        return response()->json($show,200);
    }

    public function updatePerusahaan(Request $request, $id)
    {
        //Update Regional
        $showUser = User::find($id);
        $update = json_decode($this->InternalService->updatePerusahaan($request->all(), $showUser->id_perusahaan));

        return response()->json($update,200);
    }

    public function get()
    {
        $get = json_decode($this->InternalService->getPerusahaan());

        return response()->json($get, 200);
    }

    public function getPengiriman($idPerusahaan)
    {
        $get = json_decode($this->InternalService->getPengirimanByPerusahaan($idPerusahaan));

        return response()->json($get, 200);
    }

    public function showPengiriman($idPengiriman)
    {
        $pengiriman = json_decode($this->InternalService->showPengiriman($idPengiriman));
        return response()->json($pengiriman, 200);
    }

    public function updatePengirimanCoor(Request $request)
    {
        $update = json_decode($this->InternalService->updatePengirimanCoor($request->all()));
        return response()->json($update,200);
    }

    

}
