<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Models\Biodata;
use App\Traits\ApiResponser;
use App\Services\InternalService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use ApiResponser;
    public $InternalService;
    public function __construct(InternalService $InternalService)
    {
        $this->InternalService  = $InternalService;
    }

    public function login(Request $request)
    {
        $roleCheck = User::where('email', $request->email)->first();
        if (is_null($roleCheck)) {
            $resp = [
                'code' => 401,
                'message' => 'User tidak ditemukan!',
            ];
            return response()->json($resp);
        }else{
                if ($roleCheck->role == 'mitra') {
                    $userCheck = User::where('email', $request->email)->first();
                    $biodata = Biodata::where('id_user', $userCheck->id)->first();

                    if (is_null($userCheck)) {
                        $resp = [
                            'code' => 401,
                            'message' => 'User tidak ditemukan!',
                        ];
                    } else {
                        if (Hash::check($request->password, $userCheck->password)) {
                            $tokenRequest = $request->create(
                                'http://localhost:8080/oauth/token',
                                'POST'
                            );
                    
                            $tokenRequest->request->add([
                                "client_id"     => '2',
                                "client_secret" => 'z2wCSR6tClX9HJYFropSsLQqrZhvT5ULHds64njL',
                                "username" => $request->email,
                                "password" => $request->password,
                                "grant_type"    => 'password',
                                "scope"          => '*',
                            ]);
                    
                            $response = app()->handle($tokenRequest);

                            return $response;
                        }else {
                            $resp = [
                                'code' => 401,
                                'message' => 'Password tidak sesuai!',
                            ];
                            return response()->json($resp);
                        }
                    }
                } else if ($roleCheck->role == 'member') {
                    $userCheck = User::where('email', $request->email)->first();
                    $biodata = Biodata::where('id_user', $userCheck->id)->first();

                    if (is_null($userCheck)) {
                        $resp = [
                            'code' => 401,
                            'message' => 'User tidak ditemukan!',
                        ];
                    } else {
                        if (Hash::check($request->password, $userCheck->password)) {
                            $tokenRequest = $request->create(
                                'http://localhost:8080/oauth/token',
                                'POST'
                            );
                    
                            $tokenRequest->request->add([
                                "client_id"     => '6',
                                "client_secret" => 'szyMaHGwdRp8WgbOCh8KRMFGfBBlAo4x46EfkTOa',
                                "username" => $request->email,
                                "password" => $request->password,
                                "grant_type"    => 'password',
                                "scope"          => '*',
                            ]);
                    
                            $response = app()->handle($tokenRequest);

                            return $response;
                        }else {
                            $resp = [
                                'code' => 401,
                                'message' => 'Password tidak sesuai!',
                            ];
                            return response()->json($resp);
                        }
                    }
                } else {
                    $userCheck = User::where('id_perusahaan',$request->id_perusahaan)
                                        ->where('email', $request->email)
                                        ->first();

                    $biodata = Biodata::where('id_user', $userCheck->id)->first();

                    if (is_null($userCheck)) {
                       $resp = [
                            'code' => 401,
                            'message' => 'User tidak ditemukan!',
                        ];
                    } else {
                        if (Hash::check($request->password, $userCheck->password)) {
                            
                            $tokenRequest = $request->create(
                                'http://localhost:8080/oauth/token',
                                'POST'
                            );

                    
                            $tokenRequest->request->add([
                                "client_id"     => '2',
                                "client_secret" => 'z2wCSR6tClX9HJYFropSsLQqrZhvT5ULHds64njL',
                                "username" => $request->email,
                                "password" => $request->password,
                                "grant_type"    => 'password',
                                "scope"          => '*',
                            ]);
                    
                            $response = app()->handle($tokenRequest);

                            return $response;
                        }else {
                            $resp = [
                                'code' => 401,
                                'message' => 'Password tidak sesuai!',
                            ];
                            return response()->json($resp);
                        }
                    }
                }
            }        
    }

    public function loginMobile(Request $request)
    {
        $check  = json_decode($this->InternalService->loginMobile($request->all()));

        return response()->json($check, 200);
    }

    //
}
