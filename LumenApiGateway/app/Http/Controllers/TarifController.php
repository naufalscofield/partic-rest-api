<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;

class TarifController extends Controller
{
    use ApiResponser;
    public $InternalService;

    public function __construct(InternalService $InternalService)
    {
        $this->InternalService = $InternalService;
    }

    public function getByIdPerusahaan(Request $request, $idPerusahaan)
    {
        $getTarif = json_decode($this->InternalService->getTarif($idPerusahaan));
        
        return response()->json($getTarif,200);
    }

    public function getTarifPelanggan(Request $request, $idPerusahaan)
    {
        $getTarif = json_decode($this->InternalService->getTarifPelanggan($idPerusahaan));

        return response()->json($getTarif, 200);
    }

    public function showTarifPelanggan(Request $request, $idTarif)
    {
        $showTarif = json_decode($this->InternalService->showTarifPelanggan($idTarif));

        return response()->json($showTarif, 200);
    }

    public function showTarifPelangganByParams($idPerusahaan, $jenisPengangkutan, $jenisPengiriman, $namaBarang, $kotaAsal, $kotaTujuan)
    {
        $showTarif = json_decode($this->InternalService->showTarifPelangganByParams($idPerusahaan, $jenisPengangkutan, $jenisPengiriman, $namaBarang, $kotaAsal, $kotaTujuan));

        return response()->json($showTarif, 200);
    }
    
    public function storeTarifPelanggan(Request $request, $idPerusahaan)
    {
        $storeTarif = json_decode($this->InternalService->storeTarifPelanggan($request->all(), $idPerusahaan));
        
        return response()->json($storeTarif, 200);
    }
    
    public function updateTarifPelanggan(Request $request, $idPerusahaan)
    {
        $updateTarif = json_decode($this->InternalService->updateTarifPelanggan($request->all(), $idPerusahaan));
        
        return response()->json($updateTarif, 200);
    }
    
    public function deleteTarifPelanggan(Request $request, $idTarif)
    {
        $deleteTarif = json_decode($this->InternalService->deleteTarifPelanggan($idTarif));

        return response()->json($deleteTarif, 200);
    }
    

}
