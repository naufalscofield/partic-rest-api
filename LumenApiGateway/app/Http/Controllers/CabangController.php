<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Services\ExternalService;
use App\Services\PrometheeService;
use App\Services\SmartService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Models\Biodata;

class CabangController extends Controller
{
    use ApiResponser;
    public $InternalService;
    public $ExternalService;
    public $PrometheeService;
    public $SmartService;

    public function __construct(InternalService $InternalService, ExternalService $ExternalService, PrometheeService $PrometheeService, SmartService $SmartService)
    {
        $this->InternalService = $InternalService;
        $this->ExternalService = $ExternalService;
        $this->PrometheeService = $PrometheeService;
        $this->SmartService = $SmartService;
    }

    public function showByIdPerusahaan(Request $request, $idPerusahaan)
    {
        //Show Cabang By Id Perusahaan        
        $show = json_decode($this->InternalService->showCabangByIdPerusahaan($idPerusahaan));
        
        return response()->json($show,200);
    }

    public function show(Request $request, $id)
    {
        //Show Cabang By Id Perusahaan        
        $show = json_decode($this->InternalService->showCabang($id));
        
        return response()->json($show,200);
    }

    public function store(Request $request)
    {
        $store = json_decode($this->InternalService->storeCabang($request->all()));

        return response()->json($store, 200);
    }

    public function update(Request $request, $id)
    {
        $update = json_decode($this->InternalService->updateCabang($request->all(), $id));

        return response()->json($update,200);
    }
    
    public function delete($id)
    {
        $delete = json_decode($this->InternalService->deleteCabang($id));

        return response()->json($delete,200);
    }

    public function storePegawai(Request $request)
    {
        //Insert User
        $user = new User;
        
        $user->email            = $request->email;
        $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $user->id_perusahaan    = $request->id_perusahaan;
        $user->id_regional      = $request->id_regional;
        $user->id_cabang        = $request->id_cabang;
        $user->role             = 'cabang';
        $user->verifikasi       = 2;

        $user->save();
        
        //Insert Biodata
        $biodata = new Biodata;
        
        $biodata->id_user          = $user->id;
        $biodata->nama_lengkap     = $request->nama_lengkap;
        $biodata->tanggal_lahir    = $request->tanggal_lahir;
        $biodata->tempat_lahir     = $request->tempat_lahir;
        $biodata->alamat           = $request->alamat;
        $biodata->no_telp          = $request->no_telp;
        $biodata->foto_ktp         = $request->foto_ktp;
        $biodata->foto_npwp        = $request->foto_npwp;
        $biodata->foto_diri        = $request->foto_diri;
        $biodata->nik              = $request->nik;

        $biodata->save();

        //Data Response
        $resp = [
            'DataUser'          => $user,
            'DataBiodata'       => $biodata,
        ];

        return response()->json($resp, 200);
    }

    public function getPegawai($idPerusahaan, $idRegional, $idCabang)
    {
        $getPegawai = User::where('id_perusahaan',$idPerusahaan)
                            ->where('id_regional',$idRegional)
                            ->where('id_cabang',$idCabang)->get();

        return response()->json($getPegawai,200);
    }

    public function deletePegawai($id)
    {
        $deleteUser = User::find($id);
        $deleteUser->delete();
        $deleteBiodata = Biodata::where('id_user',$id)->first();
        $deleteBiodata->delete();
        $resp = [
        'deleteUser' => $deleteUser,
        'deleteBiodata' => $deleteBiodata
        ];
        return response()->json($resp,200);
    }

    public function getPengirimanByCabang($idCabang)
    {
        $get = json_decode($this->InternalService->getPengirimanByCabang($idCabang));
        
        return response()->json($get, 200);
    } 

    public function storePengiriman(Request $request)
    {
        $store = json_decode($this->InternalService->storePengiriman($request->all()));
        
        return response()->json($store, 200);
    } 

    public function updateStatusPengiriman(Request $request, $id)
    {
        $store = json_decode($this->InternalService->updateStatusPengiriman($request->all(), $id));
        
        return response()->json($store, 200);
    }
    
    public function promethee(Request $request, $idKelompokPengiriman)
    {
        $getPengiriman = json_decode($this->InternalService->showKelompokPengiriman($idKelompokPengiriman));
        $asal = str_replace(' ', '-', $getPengiriman->asal);
        $tujuan = str_replace(' ', '-', $getPengiriman->tujuan);
        
        // return response()->json($getPengiriman, 200);
        $getMitra = json_decode($this->ExternalService->getMitraByKota($asal, $tujuan));
        $getReportMitra = json_decode($this->ExternalService->getReportMitra($getPengiriman->id_perusahaan));
        
        $dataPromethee = [
            'dataPengiriman' => $getPengiriman,
            'dataMitra' => $getMitra,
            'dataReport' => $getReportMitra
        ];

        $getPromethee = json_decode($this->PrometheeService->promethee($dataPromethee));

        return response()->json($getPromethee, 200);
        
    }
    
    public function storePromethee(Request $request)
    {
        $updateKelompok = json_decode($this->InternalService->updateKelompokPengiriman($request->all()));

        return response()->json($updateKelompok, 200);
    }
    // public function promethee(Request $request)
    // {
    //     $storePengiriman = json_decode($this->InternalService->storePengiriman($request->all()));
        
    //     $getMitra = json_decode($this->ExternalService->getMitraByKota($storePengiriman->id_perusahaan, $storePengiriman->kota_asal));
        
    //     $getReportMitra = json_decode($this->ExternalService->getReportMitra($storePengiriman->id_perusahaan));
        
    //     $dataPromethee = [
    //         'dataPengiriman' => $storePengiriman,
    //         'dataMitra' => $getMitra,
    //         'dataReport' => $getReportMitra
    //     ];

    //     $getPromethee = json_decode($this->PrometheeService->promethee($dataPromethee));

    //     return response()->json($getPromethee, 200);
        
    // }

    public function getPicCabangByIdRegional($id_perusahaan, $id_regional)
    {
        $picRegional = DB::table('users')
                        ->where('users.id_perusahaan', $id_perusahaan)
                        ->where('users.deleted_at', NULL)
                        ->join('regional', 'regional.id', 'users.id_regional')
                        ->join('biodata', 'biodata.id_user', 'users.id')
                        ->join('cabang', 'cabang.id', 'users.id_cabang')
                        ->select('users.*', 'regional.*', 'cabang.*', 'biodata.*')
                        ->get();

        return response()->json($picRegional, 200);
    }

    public function showByIdRegional($id_regional)
    {
        $get = json_decode($this->InternalService->getCabangByIdRegional($id_regional));

        return response()->json($get, 200);
    }

    public function smart(Request $request)
    {
        // return response()->json($request->all(), 200);
        $getKelompok = json_decode($this->InternalService->showKelompokPengiriman($request->id_kelompok_pengiriman));
        // $asal = str_replace(' ', '-', $getPengiriman->asal);
        // $tujuan = str_replace(' ', '-', $getPengiriman->tujuan);
        
        // return response()->json($getPengiriman, 200);
        // $getMitra = json_decode($this->ExternalService->getMitraByKota($getPengiriman->id_perusahaan, $asal, $tujuan));
        // $getReportMitra = json_decode($this->ExternalService->getReportMitra($getPengiriman->id_perusahaan));
        
        // $dataPromethee = [
        //     'dataPengiriman' => $getPengiriman,
        //     'dataMitra' => $getMitra,
        //     'dataReport' => $getReportMitra
        // ];

        $data = [
            'kelompok'  => $getKelompok,
            'durasi'    => $request->durasi,
            'tarif'     => $request->tarif,
            'catatan_hitam'    => $request->catatan_hitam,
        ];

        // return response()->json($data, 200);
        $smart = json_decode($this->SmartService->smart($data));

        return response()->json($smart, 200);
        
    }

    public function showPegawai(Request $request, $id)
    {
        //Get User Biodata PIC Cabang
        $showUser = User::find($id);
        $showBiodata = Biodata::where('id_user',$id)->first();
        $regional = json_decode($this->InternalService->showRegional($showUser->id_regional));
        $cabang = json_decode($this->InternalService->showCabang($showUser->id_cabang));
        $resp = [
            'DataUser'          => $showUser,
            'DataBiodata'       => $showBiodata,
            'DataRegional'       => $regional,
            'DataCabang'       => $cabang,
        ];
        return response()->json($resp);
    }

}
