<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Services\ExternalService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;

class MitraController extends Controller
{
    use ApiResponser;
    public $ExternalService;
    public $InternalService;

    public function __construct(ExternalService $ExternalService, InternalService $InternalService)
    {
        $this->ExternalService = $ExternalService;
        $this->InternalService = $InternalService;
    }

    public function store(Request $request)
    {
        
        //Insert User
        $user = new User;
        
        $user->email            = $request->email;
        $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $user->id_perusahaan    = NULL;
        $user->id_regional      = NULL;
        $user->id_cabang        = NULL;
        $user->role             = 'mitra';
        $user->verifikasi       = 2;

        $user->save();
        
        //Insert Biodata
        $biodata = new Biodata;
        
        $biodata->id_user          = $user->id;
        $biodata->nama_lengkap     = $request->nama_lengkap;
        $biodata->tanggal_lahir    = $request->tanggal_lahir;
        $biodata->tempat_lahir     = $request->tempat_lahir;
        $biodata->alamat           = $request->alamat;
        $biodata->foto_ktp         = $request->foto_ktp;
        $biodata->foto_npwp        = $request->foto_npwp;
        $biodata->foto_diri        = $request->foto_diri;
        $biodata->nik              = $request->nik;
        $biodata->no_telp              = $request->no_telp;

        $biodata->save();

        //Insert Mitra
        $id_user = $user->id;
        $dataMitra = [
            'id_user'           => $id_user,
            'jenis_kendaraan'   => $request->jenis_kendaraan,
            'nama_kendaraan'    => $request->nama_kendaraan,
            'plat_kendaraan'    => $request->plat_kendaraan,
            'tahun_kendaraan'   => $request->tahun_kendaraan,
            'domisili'          => $request->domisili,
            'id_bbm'          => $request->id_bbm,
            'foto_stnk'         => $request->foto_stnk,
            'foto_sim'          => $request->foto_sim,
            'verifikasi'          => 1,
            'status_ketersediaan'          => 'Available',
        ];

        $storeMitra = json_decode($this->ExternalService->storeMitra($dataMitra));

        //Data Response
        $resp = [
            'DataMitra'         => $storeMitra,
            'DataUser'          => $user,
            'DataBiodata'       => $biodata,
        ];

        return response()->json($resp,200);
    }

   public function getByIdPerusahaan(Request $request, $idPerusahaan)
    {
        //Get Tarif By Id Perusahaan
        $getTarif = json_decode($this->InternalService->getTarif($idPerusahaan));

        
        $resp = [];
        foreach ($getTarif as $tarif)
        {
            //Get All Mitra
            $getMitra = json_decode($this->ExternalService->getMitra());
            foreach ($getMitra as $mitra)
            {
                if ($tarif->id_mitra == $mitra->id)
                {
                $getUser     = User::find($mitra->id_user);
                $getBiodata  = Biodata::where('id_user', $mitra->id_user)->first();
                $data        = [
                    'dataUser'       => $getUser,
                    'dataBiodata'    => $getBiodata,
                    'dataMitra'      => $mitra,
                ];
                array_push($resp, $data);
                }
            }
        }
            return response()->json($resp,200);
    }

    public function storePengajuanTarif(Request $request)
    {
        // return response()->json($request->all());
        // print_r($request->all());die;
        $storePengajuanTarif = json_decode($this->InternalService->storePengajuanTarif($request->all()));
        return response()->json($storePengajuanTarif, 200);
    }

    public function updatePengajuanTarif(Request $request, $id)
    {
        // return response()->json($id);
        $updatePengajuanTarif = json_decode($this->InternalService->updatePengajuanTarifByMitra($request->all(), $id));
        return response()->json($updatePengajuanTarif, 200);
    }
    
    public function deletePengajuanTarif($id)
    {
        // return $id;
        $deletePengajuanTarif = json_decode($this->InternalService->deletePengajuanTarif($id));
        return response()->json($deletePengajuanTarif, 200);
    }

    public function deletePengajuanTarif2($id)
    {
        // return $id;
        $deletePengajuanTarif = json_decode($this->InternalService->deletePengajuanTarif2($id));
        return response()->json($deletePengajuanTarif, 200);
    }

    public function storeDetailPengajuanTarif(Request $request)
    {
        $storeDetailPengajuanTarif = json_decode($this->InternalService->storeDetailPengajuanTarif($request->all()));
        return response()->json($storeDetailPengajuanTarif, 200);
    }

    public function updateDetailPengajuanTarif(Request $request, $id)
    {
        $updateDetailPengajuanTarif = json_decode($this->InternalService->updateDetailPengajuanTarif($request->all(), $id));
        return response()->json($updateDetailPengajuanTarif, 200);
    }
    
    public function deleteDetailPengajuanTarif($id)
    {
        $deleteDetailPengajuanTarif = json_decode($this->InternalService->deleteDetailPengajuanTarif($id));
        return response()->json($deleteDetailPengajuanTarif, 200);
    }

    public function pickupPengiriman($id)
    {

        $removeLobiPengiriman = json_decode($this->InternalService->removeLobiPengiriman($id));
        // $dataPengiriman = json_decode($this->InternalService->getPengirimanById($id));

        // $id_mitra = $dataPengiriman->id_mitra;

        // $dataMitra = json_decode($this->ExternalService->showMitra($id_mitra));
        // $dataBiodata = Biodata::where('id_user', $dataMitra->id_user)->first();

        // $data = [
        //     'nama_mitra' => $dataBiodata->nama_lengkap,
        //     'mitra' => $dataMitra,
        // ];

        // $pickup = json_decode($this->InternalService->pickup($data, $id));
        // return response()->json($pickup, 200);
    }

    public function getNamaPerusahaan()
    {
        $getNamaPerusahaan = json_decode($this->InternalService->getNamaPerusahaan());
        return response()->json($getNamaPerusahaan, 200);
    }

    public function getTarifByIdMitra($idMitra)
    {
        $getTarifByIdMitra = json_decode($this->InternalService->getTarifByIdMitra($idMitra));
        return response()->json($getTarifByIdMitra, 200);
    }

    public function getLobiMitraByIdMitra($id)
    {
        $getLobiMitraByIdMitra = json_decode($this->InternalService->getLobiMitraByIdMitra($id));
        return response()->json($getLobiMitraByIdMitra, 200);
    }

    public function getPengirimanByIdKelompokPengiriman($id)
    {
        // return $id;
        $getPengirimanByIdKelompokPengiriman = json_decode($this->InternalService->getPengirimanByIdKelompokPengiriman($id));
        return response()->json($getPengirimanByIdKelompokPengiriman, 200);
    }

    public function getPengirimanPickUpByIdKelompokPengiriman($id)
    {
        // return $id;
        $getPengirimanPickUpByIdKelompokPengiriman = json_decode($this->InternalService->getPengirimanPickUpByIdKelompokPengiriman($id));
        return response()->json($getPengirimanPickUpByIdKelompokPengiriman, 200);
    }


    public function updateStatusPickUp($id)
    {
        // return $id;
        $updateStatusPickUp = json_decode($this->InternalService->updateStatusPickUp($id));
        return response()->json($updateStatusPickUp, 200);
    }

    public function inputStatusPengiriman(Request $request)
    {
        // return response()->json($request->all());
        $inputStatusPengiriman = json_decode($this->InternalService->inputStatusPengiriman($request->all()));
        return response()->json($inputStatusPengiriman, 200);
    }

    public function updateTanggalSampai($id)
    {
        // return response()->json($id);
        $updateTanggalSampai = json_decode($this->InternalService->updateTanggalSampai($id));
        return response()->json($updateTanggalSampai, 200);
    }

    public function getRiwayatPengirimanPickUpByIdKelompokPengiriman($id)
    {
        // return $id;
        $getRiwayatPengirimanPickUpByIdKelompokPengiriman = json_decode($this->InternalService->getRiwayatPengirimanPickUpByIdKelompokPengiriman($id));
        return response()->json($getRiwayatPengirimanPickUpByIdKelompokPengiriman, 200);
    }

    public function cekResiStatus($no_resi)
    {
        // return $no_resi;
        // return response()->json($request, 200);
        $cekResiStatus = json_decode($this->InternalService->cekResiStatus($no_resi));
        return response()->json($cekResiStatus, 200);
    }

    public function cekResiKeterangan($no_resi)
    {
        // return $no_resi;
        // return response()->json($request, 200);
        $cekResiKeterangan = json_decode($this->InternalService->cekResiKeterangan($no_resi));
        return response()->json($cekResiKeterangan, 200);
    }

    public function getKendaraan($id)
    {
        // return $id;
        $getKendaraan = json_decode($this->ExternalService->getKendaraan($id));
        return response()->json($getKendaraan, 200);
    }

    public function getPendapatanMitra($id)
    {
        // return $id;
        $getPendapatanMitra = json_decode($this->InternalService->getPendapatanMitra($id));
        return response()->json($getPendapatanMitra, 200);
    }

    public function inputPengiriman(Request $request)
    {
        // return $id;
        $inputPengiriman = json_decode($this->InternalService->inputPengiriman($request->all()));
        return response()->json($inputPengiriman, 200);
    }

    public function updateStatusMitra($id)
    {
        // return $id;
        $updateStatusMitra = json_decode($this->ExternalService->updateStatusMitra($id));
        return response()->json($updateStatusMitra, 200);
    }

    public function getBbmByIdBbm($id)
    {
        // return $id;
        $getBbmByIdBbm = json_decode($this->ExternalService->getBbmByIdBbm($id));
        return response()->json($getBbmByIdBbm, 200);
    }

    public function updateJumlahHariTelat($id,$hari)
    {
        // return $id;
        $updateJumlahHariTelat = json_decode($this->InternalService->updateJumlahHariTelat($id,$hari));
        return response()->json($updateJumlahHariTelat, 200);
    }

    public function updateKendaraanMitra(Request $request, $id)
    {
        // return $id;
        $updateKendaraanMitra = json_decode($this->ExternalService->updateKendaraanMitra($request->all(),$id));
        return response()->json($updateKendaraanMitra, 200);
    }

    public function getBbm()
    {
        // return $id;
        $getBbm = json_decode($this->ExternalService->getBbm());
        return response()->json($getBbm, 200);
    }
}
