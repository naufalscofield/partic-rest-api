<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Services\ExternalService;
use App\Services\SmartService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;
use DB;

class RegionalController extends Controller
{
    use ApiResponser;
    public $InternalService;
    public $SmartService;
    public function __construct(InternalService $InternalService, ExternalService $ExternalService, SmartService $SmartService)
    {
        $this->InternalService  = $InternalService;
        $this->ExternalService  = $ExternalService;
        $this->SmartService     = $SmartService;
    }

    public function store(Request $request)
    {
        //Insert Regional
        $storeRegional = json_decode($this->InternalService->storeRegional($request->all()));

        return response()->json($storeRegional,200);
    }

    public function get(Request $request, $idPerusahaan)
    {
        //Get Regional
        $get = json_decode($this->InternalService->getRegional($idPerusahaan));

        return response()->json($get,200);
    }

    public function show(Request $request, $id)
    {
        //Show Regional
        // return response()->json($id);
        $show = json_decode($this->InternalService->showRegional($id));
        
        return response()->json($show,200);
    }

    public function update(Request $request, $id)
    {
        //Update Regional
        // return response()->json($request->all());
        $update = json_decode($this->InternalService->updateRegional($request->all(), $id));

        return response()->json($update,200);
    }

    public function delete($id)
    {
        //Delete Regional
        $delete = json_decode($this->InternalService->deleteRegional($id));
        
        return response()->json($delete,200);
    }

    public function getPegawai($idPerusahaan)
    {
        //Get Pegawai Regional Sebuah Perushaaan
        $getPegawai = DB::table('users')
        ->where('users.id_perusahaan', $idPerusahaan)
        ->where('users.deleted_at', NULL)
        ->join('biodata', 'users.id', 'biodata.id_user')
        ->join('regional', 'users.id_regional', 'regional.id')
        ->select('users.*', 'biodata.*', 'regional.alamat as alamat_regional', 'regional.*', 'biodata.alamat as alamat_biodata')
        ->get();

        return response()->json($getPegawai,200);
    }

    public function storePegawai(Request $request)
    {
        
        //Insert User
        $user = new User;
        
        $user->email            = $request->email;
        $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $user->id_perusahaan    = $request->id_perusahaan;
        $user->id_regional      = $request->id_regional;
        $user->id_cabang        = NULL;
        $user->role             = 'regional';
        $user->verifikasi       = 2;

        $user->save();
        
        //Insert Biodata
        $biodata = new Biodata;
        
        $biodata->id_user          = $user->id;
        $biodata->nama_lengkap     = $request->nama_lengkap;
        $biodata->tanggal_lahir    = $request->tanggal_lahir;
        $biodata->tempat_lahir     = $request->tempat_lahir;
        $biodata->alamat           = $request->alamat;
        $biodata->no_telp          = $request->no_telp;
        $biodata->foto_ktp         = $request->foto_ktp;
        $biodata->foto_npwp        = $request->foto_npwp;
        $biodata->foto_diri        = $request->foto_diri;
        $biodata->nik              = $request->nik;

        $biodata->save();
        //Data Response
        $resp = [
            'DataUser'          => $user,
            'DataBiodata'       => $biodata,
        ];

        return response()->json($resp,200);
    }

    public function showPegawai(Request $request, $id)
    {
        //Get User Biodata PIC Regional
        $showUser = User::find($id);
        $showBiodata = Biodata::where('id_user',$id)->first();
        $regional = json_decode($this->InternalService->showRegional($showUser->id_regional));
        $resp = [
            'DataUser'          => $showUser,
            'DataBiodata'       => $showBiodata,
            'DataRegional'       => $regional,
        ];
        return response()->json($resp);
    }

    public function updatePegawai(Request $request, $id)
    {
        return response($request->all());
        $updateUser = User::find($id);
        $updateUser->email            = $request->email;
        $updateUser->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $updateUser->id_regional      = $request->id_regional;
        $updateUser->save();

        $updateBiodata = Biodata::where('id_user',$id)->first();
        $updateBiodata->nama_lengkap     = $request->nama_lengkap;
        $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
        $updateBiodata->tempat_lahir     = $request->tempat_lahir;
        $updateBiodata->alamat           = $request->alamat;
        $updateBiodata->foto_ktp         = $request->foto_ktp;
        $updateBiodata->foto_npwp        = $request->foto_npwp;
        $updateBiodata->foto_diri        = $request->foto_diri;
        $updateBiodata->nik              = $request->nik;
        $updateBiodata->save();

        $resp = [
            'DataUser'          => $updateUser,
            'DataBiodata'       => $updateBiodata,
        ];

        return response()->json($resp,200);
    }

    public function deletePegawai($id)
    {
        $deleteUser = User::find($id);
        $deleteUser->delete();
        $deleteBiodata = Biodata::where('id_user',$id)->first();
        $deleteBiodata->delete();
        $resp = [
        'deleteUser' => $deleteUser,
        'deleteBiodata' => $deleteBiodata
        ];
        return response()->json($resp,200);
    }

    public function verifikasiMitra($idMitra)
    {
        $verifikasiMitra = json_decode($this->ExternalService->verifikasiMitra($idMitra));
        
        return response()->json($verifikasiMitra, 200);
    }

    public function getMitraByKota($idPerusahaan, $kota)
    {
        $getMitra = json_decode($this->ExternalService->getMitraByKota($idPerusahaan, $kota));
        return response()->json($getMitra, 200);
    }

    public function showMitra($idMitra)
    {
        $showMitra = json_decode($this->ExternalService->showMitra($idMitra));
        return response()->json($showMitra, 200);
    }

    public function getTarifByKota($idPerusahaan, $kota)
    {
        $getTarif = json_decode($this->InternalService->getTarifByKota($idPerusahaan, $kota));

        return response()->json($getTarif, 200);
    }

    public function getPengajuanTarifByKota($idPerusahaan, $kota)
    {
        $getTarif = json_decode($this->InternalService->getPengajuanTarifByKota($idPerusahaan, $kota));

        return response()->json($getTarif, 200);
    }

    public function updatePengajuanTarif(Request $request)
    {
        $updateTarif = json_decode($this->InternalService->updatePengajuanTarif($request->all()));

        return response()->json($updateTarif, 200);
    }

    public function getPengirimanByKota($idPerusahaan, $kota)
    {

        $pengiriman = json_decode($this->InternalService->getPengirimanByKota($idPerusahaan, $kota));
        return response()->json($pengiriman, 200);
    }

    public function verifikasiPengiriman(Request $request, $id)
    {
        $verifikasi = json_decode($this->InternalService->verifikasiPengiriman($id, $request->all()));
        
        return response()->json($verifikasi, 200);
    }

    public function showPengiriman($idPengiriman)
    {
        $pengiriman = json_decode($this->InternalService->showPengiriman($idPengiriman));
        return response()->json($pengiriman, 200);
    }

    public function verifikasiMember($id)
    {
        $verifikasi = json_decode($this->ExternalService->verifikasiMember($id));
        
        return response()->json($verifikasi, 200);
    }

    public function getKelompokPengiriman($idRegional)
    {
        $kelompok = json_decode($this->InternalService->getKelompokPengiriman($idRegional));
        

        return response()->json($kelompok, 200);
    }

    public function storeKelompokPengiriman(Request $request)
    {
        $kelompok = json_decode($this->InternalService->storeKelompokPengiriman($request->all()));
        

        return response()->json($kelompok, 200);
    }

    public function getFieldKelompokPengiriman($kota, $id_perusahaan)
    {
        $field = json_decode($this->InternalService->getFieldKelompokPengiriman($kota, $id_perusahaan));

        return response()->json($field, 200);
    }

    public function getField2KelompokPengiriman($tujuan, $kota, $id_perusahaan)
    {
        $field = json_decode($this->InternalService->getField2KelompokPengiriman($tujuan, $kota, $id_perusahaan));

        return response()->json($field, 200);
    }

    public function getField3KelompokPengiriman($tujuan, $jenis_pengangkutan,$kota, $id_perusahaan)
    {
        $field = json_decode($this->InternalService->getField3KelompokPengiriman($tujuan, $jenis_pengangkutan, $kota, $id_perusahaan));

        return response()->json($field, 200);
    }

    public function getField4KelompokPengiriman($tujuan, $jenis_pengangkutan,$jenis_pengiriman, $kota, $id_perusahaan)
    {
        $field = json_decode($this->InternalService->getField4KelompokPengiriman($tujuan, $jenis_pengangkutan, $jenis_pengiriman, $kota, $id_perusahaan));

        return response()->json($field, 200);
    }

    public function smart(Request $request)
    {
        // return response()->json($request->all(), 200);
        $getKelompok = json_decode($this->InternalService->showKelompokPengiriman($request->id_kelompok_pengiriman));
        // $asal = str_replace(' ', '-', $getPengiriman->asal);
        // $tujuan = str_replace(' ', '-', $getPengiriman->tujuan);
        
        // return response()->json($getKelompok, 200);
        // $getMitra = json_decode($this->ExternalService->getMitraByKota($getPengiriman->id_perusahaan, $asal, $tujuan));
        // $getReportMitra = json_decode($this->ExternalService->getReportMitra($getPengiriman->id_perusahaan));
        
        // $dataPromethee = [
        //     'dataPengiriman' => $getPengiriman,
        //     'dataMitra' => $getMitra,
        //     'dataReport' => $getReportMitra
        // ];

        $data = [
            'kelompok'  => $getKelompok,
            'durasi'    => $request->durasi,
            'tarif'     => $request->tarif,
            'catatan_hitam'    => $request->catatan_hitam,
        ];

        // return response()->json($data, 200);
        $smart = json_decode($this->SmartService->smart($data));

        return response()->json($smart, 200);
        
    }

    public function smartFinal(Request $request)
    {
        $updateKelompok = json_decode($this->InternalService->updateKelompokPengirimanSmart($request->all()));
        return response()->json($updateKelompok, 200);
    }
    

}
