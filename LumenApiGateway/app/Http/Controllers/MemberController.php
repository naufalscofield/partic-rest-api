<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Services\ExternalService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;

class MemberController extends Controller
{
    use ApiResponser;
    public $ExternalService;
    public $InternalService;

    public function __construct(ExternalService $ExternalService, InternalService $InternalService)
    {
        $this->ExternalService = $ExternalService;
        $this->InternalService = $InternalService;
    }

    public function store(Request $request)
    {
        
        //Insert User
        $user = new User;
        
        $user->email            = $request->email;
        $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
        $user->id_perusahaan    = NULL;
        $user->id_regional      = NULL;
        $user->id_cabang        = NULL;
        $user->role             = 'member';
        $user->verifikasi       = 2;

        $user->save();
        
        //Insert Biodata
        $biodata = new Biodata;
        
        $biodata->id_user          = $user->id;
        $biodata->nama_lengkap     = $request->nama_lengkap;
        $biodata->tanggal_lahir    = $request->tanggal_lahir;
        $biodata->tempat_lahir     = $request->tempat_lahir;
        $biodata->alamat           = $request->alamat;
        $biodata->foto_ktp         = $request->foto_ktp;
        $biodata->foto_npwp        = $request->foto_npwp;
        $biodata->foto_diri        = $request->foto_diri;
        $biodata->nik              = $request->nik;
        $biodata->no_telp              = $request->no_telp;

        $biodata->save();

        //Data Response
        $resp = [
            'DataUser'          => $user,
            'DataBiodata'       => $biodata,
        ];

        return response()->json($resp,200);
    }

   public function getByIdPerusahaan(Request $request, $idPerusahaan, $no)
    {
        $getMember = json_decode($this->ExternalService->getMemberByIdPerusahaan($idPerusahaan, $no));

        $resp = [];
        foreach ($getMember as $member)
        {
            $getUser     = User::find($member->id_user);
            $getBiodata  = Biodata::where('id_user', $member->id_user)->first();
            $data        = [
                'user'       => $getUser,
                'biodata'    => $getBiodata,
                'member'      => $member,
            ];
            array_push($resp, $data);
        }
            return response()->json($resp,200);
    }

    public function getNamaPerusahaan()
    {
        // return $id;
        $getNamaPerusahaan = json_decode($this->InternalService->getNamaPerusahaan());
        
        return response()->json($getNamaPerusahaan,200);
    }

    public function getMemberByIdUsers($id)
    {
        // return $id;
        $getMemberByIdUsers = json_decode($this->ExternalService->getMemberByIdUsers($id));
        
        return response()->json($getMemberByIdUsers,200);
    }

    public function getPengirimanByIdUsers($id)
    {
        // return $id;
        $getPengirimanByIdUsers = json_decode($this->ExternalService->getPengirimanByIdUsers($id));
        
        return response()->json($getPengirimanByIdUsers,200);
    }

    public function getCabangByIdPerusahaan($id)
    {
        // return $id;
        $getCabangByIdPerusahaan = json_decode($this->InternalService->getCabangByIdPerusahaan($id));
        
        return response()->json($getCabangByIdPerusahaan,200);
    }

    public function pengajuanMember(Request $request)
    {
        // return response()->json($request->all());
        $pengajuanMember = json_decode($this->ExternalService->pengajuanMember($request->all()));
        
        return response()->json($pengajuanMember,200);
    }

    public function getRiwayatPengirimanByIdUsers($id)
    {
        // return response()->json($request->all());
        $getRiwayatPengirimanByIdUsers = json_decode($this->InternalService->getRiwayatPengirimanByIdUsers($id));
        
        return response()->json($getRiwayatPengirimanByIdUsers,200);
    }

    public function delete($id)
    {
        $delete = json_decode($this->ExternalService->deleteMember($id));
        
        return response()->json($delete,200);
    }

    // public function getByIdPerusahaan(Request $request, $idPerusahaan)
    // {
    //     //Get Regional
    //     $getTarif = json_decode($this->InternalService->getTarif($idPerusahaan));
    //     return response()->json($getTarif);
    //     $dataMitra = [];
    //     foreach ($dataTarif as $tarif){
    //     $getMitra = json_decode($this->ExternalService->getMitra($tarif->id_mitra));
    //     array_push($dataMitra, $getMitra);
    //     }
    //     return response()->json($dataMitra);

    //     $getBiodata = Biodata::where('id_user', $getMitra->id_user)->get();
    //     // return response()->json($showCabang,200);

    //     $resp = [
    //         'BiodataMitra'         => $getBiodata,
    //         'DataMitra'          => $dataMitra,
    //         'DataTarif'          => $getTarif
    //     ];

    //     return response()->json($resp,200);
    // }

    // public function show(Request $request, $id)
    // {
    //     //Show Regional
    //     // return response()->json($id);
    //     $show = json_decode($this->InternalService->showRegional($id));
        
    //     return response()->json($show,200);
    // }

    // public function update(Request $request, $id)
    // {
    //     //Update Regional
    //     // return response()->json($request->all());
    //     $update = json_decode($this->InternalService->updateRegional($request->all(), $id));

    //     return response()->json($update,200);
    // }

    // public function delete($id)
    // {
    //     //Delete Regional
    //     $delete = json_decode($this->InternalService->deleteRegional($id));
        
    //     return response()->json($delete,200);
    // }

    // public function getPegawai($idPerusahaan, $idRegional)
    // {
    //     //Get Pegawai Regional Sebuah Perushaaan
    //     $getPegawai = User::where('id_perusahaan',$idPerusahaan)
    //                         ->where('id_regional',$idRegional)->get();

    //     return response()->json($getPegawai,200);
    // }

    // public function storePegawai(Request $request)
    // {
        
    //     //Insert User
    //     $user = new User;
        
    //     $user->email            = $request->email;
    //     $user->password         = password_hash($request->password, PASSWORD_BCRYPT);
    //     $user->id_perusahaan    = $request->id_perusahaan;
    //     $user->id_regional      = $request->id_regional;
    //     $user->id_cabang        = NULL;
    //     $user->role             = 'regional';
    //     $user->verifikasi       = 2;

    //     $user->save();
        
    //     //Insert Biodata
    //     $biodata = new Biodata;
        
    //     $biodata->id_user          = $user->id;
    //     $biodata->nama_lengkap     = $request->nama_lengkap;
    //     $biodata->tanggal_lahir    = $request->tanggal_lahir;
    //     $biodata->tempat_lahir     = $request->tempat_lahir;
    //     $biodata->alamat           = $request->alamat;
    //     $biodata->foto_ktp         = $request->foto_ktp;
    //     $biodata->foto_npwp        = $request->foto_npwp;
    //     $biodata->foto_diri        = $request->foto_diri;
    //     $biodata->nik              = $request->nik;

    //     $biodata->save();

    //     //Data Response
    //     $resp = [
    //         'DataUser'          => $user,
    //         'DataBiodata'       => $biodata,
    //     ];

    //     return response()->json($resp,200);
    // }

    // public function showPegawai(Request $request, $id)
    // {
    //     //Get User Biodata PIC Regional
    //     $showUser = User::find($id);
    //     // return response()->json($showUser);
    //     $showBiodata = Biodata::where('id_user',$id)->get();
    //     $resp = [
    //         'DataUser'          => $showUser,
    //         'DataBiodata'       => $showBiodata,
    //     ];
    //     return response()->json($resp);
    // }

    // public function updatePegawai(Request $request, $id)
    // {
    //     return response($request->all());
    //     $updateUser = User::find($id);
    //     $updateUser->email            = $request->email;
    //     $updateUser->password         = password_hash($request->password, PASSWORD_BCRYPT);
    //     $updateUser->id_regional      = $request->id_regional;
    //     $updateUser->save();

    //     $updateBiodata = Biodata::where('id_user',$id)->first();
    //     $updateBiodata->nama_lengkap     = $request->nama_lengkap;
    //     $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
    //     $updateBiodata->tempat_lahir     = $request->tempat_lahir;
    //     $updateBiodata->alamat           = $request->alamat;
    //     $updateBiodata->foto_ktp         = $request->foto_ktp;
    //     $updateBiodata->foto_npwp        = $request->foto_npwp;
    //     $updateBiodata->foto_diri        = $request->foto_diri;
    //     $updateBiodata->nik              = $request->nik;
    //     $updateBiodata->save();

    //     $resp = [
    //         'DataUser'          => $updateUser,
    //         'DataBiodata'       => $updateBiodata,
    //     ];

    //     return response()->json($resp,200);
    // }

    //  public function deletePegawai($id)
    // {
    //     $deleteUser = User::find($id);
    //     $deleteUser->delete();
    //     $deleteBiodata = Biodata::where('id_user',$id)->first();
    //     $deleteBiodata->delete();
    //     $resp = [
    //     'deleteUser' => $deleteUser,
    //     'deleteBiodata' => $deleteBiodata
    //     ];
    //     return response()->json($resp,200);
    // }

}
