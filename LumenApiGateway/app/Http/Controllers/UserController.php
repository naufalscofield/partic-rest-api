<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    use ApiResponser;

    public function get()
    {
        $users = User::all();
        return response()->json($users);
    }

    public function aktivasi(Request $request)
    {
        $user   = User::find($request->id);

        if ($user) 
        {
            if ($user->email_token != $request->token)
            {
                $resp = [
                    'status' => 401,
                    'message' => 'Oops, Kesalahan aktivasi akun. Periksa kembali link aktivasi'
                ];

                return response($resp);
            } else 
            {
                $user->email_verified_at = Carbon::now();
                $user->verifikasi = 1;
                $user->save();
                $resp = [
                    'status' => 200,
                    'message' => 'Sukses aktivasi akun!'
                ];

                return response($resp);
            }
        } else
        {
            $resp = [
                'status' => 404,
                'message' => 'Oops, Gagal aktivasi karna akun tidak ditemukan. Periksa kembali link aktivasi'
            ];

            return response($resp);
        }

        
    }

    public function aktivasiAkunEksternal($id)
    {
        $user   = User::find($id);

        if ($user) 
        {
            $user->email_verified_at = Carbon::now();
            $user->verifikasi = 1;
            $user->save();
            $resp = [
                'status' => 200,
                'message' => 'Sukses aktivasi akun!'
            ];

            return response($resp);
        } else
        {
            $resp = [
                'status' => 404,
                'message' => 'Oops, Gagal aktivasi karna akun tidak ditemukan. Periksa kembali link aktivasi'
            ];

            return response($resp);
        }

        
    }

}
