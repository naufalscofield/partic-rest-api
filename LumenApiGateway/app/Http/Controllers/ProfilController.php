<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\InternalService;
use App\Services\ExternalService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\User;
use App\Models\Biodata;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProfilController extends Controller
{
    use ApiResponser;
    public $InternalService;
    public $ExternalService;

    public function __construct(InternalService $InternalService, ExternalService $ExternalService)
    {
        $this->InternalService = $InternalService;
        $this->ExternalService = $ExternalService;
    }

    public function show(Request $request, $id)
    {
        $showUser = User::find($id);
        $showBiodata = Biodata::where('id_user', $showUser->id)->first();
        $resp = [
            'DataUser'          => $showUser,
            'DataBiodata'       => $showBiodata
        ];
        return response()->json($resp,200);
    }

    public function showByEmail(Request $request)
    {
        $showUser = User::where('email', $request->email)->first();
        $showBiodata = Biodata::where('id_user', $showUser->id)->first();
        $showPerusahaan = json_decode($this->InternalService->showPerusahaan($showUser->id_perusahaan));
        // return response($showPerusahaan);

        $resp = [
            'DataUser'              => $showUser,
            'DataBiodata'           => $showBiodata,
            'DataPerusahaan'        => $showPerusahaan,
        ];
        return response()->json($resp,200);
    }

    public function showByEmailEksternal(Request $request)
    {
        $showUser = User::where('email', $request->email)->first();
        $showBiodata = Biodata::where('id_user', $showUser->id)->first();
        // return response($showPerusahaan);

        $resp = [
            'DataUser'              => $showUser,
            'DataBiodata'           => $showBiodata,
        ];
        return response()->json($resp,200);
    }

    public function showByEmailMitra(Request $request)
    {
        $showUser = User::where('email', $request->email)->first();
        // print_r($showUser->id); die;
        $showBiodata = Biodata::where('id_user', $showUser->id)->first();
        $showMitra = json_decode($this->ExternalService->showMitraByIdUser($showUser->id));
        // return response($showMitra);

        $resp = [
            'DataUser'              => $showUser,
            'DataBiodata'           => $showBiodata,
            'DataMitra'           => $showMitra
        ];
        return response()->json($resp,200);
    }

    public function updateTokenEmail(Request $request)
    {
        $user = User::find($request->id);

        $user->email_token = str::random(32);
        $user->save();

        return response($user, 200);
    }

    public function update(Request $request, $id)
    {

        // Kondisi tidak update semua foto
        if (!isset($request->foto_ktp) && !isset($request->foto_npwp) && !isset($request->foto_diri))
        {
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri dan ktp
        else if (!isset($request->foto_diri) && !isset($request->foto_ktp))
        {
            $foto_npwp          = $request->file('file_foto_npwp');
            $foto_npwp_name     = $request->foto_npwp;
            $path_foto_npwp     = storage_path('/app/images/foto_npwp');
            $foto_npwp->move($path_foto_npwp, $foto_npwp_name);

            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri dan npwp
        else if (!isset($request->foto_diri) && !isset($request->foto_npwp))
        {
            $foto_ktp          = $request->file('file_foto_ktp');
            $foto_ktp_name     = $request->foto_ktp;
            $path_foto_ktp     = storage_path('/app/images/foto_ktp');
            $foto_ktp->move($path_foto_ktp, $foto_ktp_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto ktp dan npwp
        else if (!isset($request->foto_ktp) && !isset($request->foto_npwp))
        {
            $foto_diri          = $request->file('file_foto_diri');
            $foto_diri_name     = $request->foto_diri;
            $path_foto_diri     = storage_path('/app/images/foto_diri');
            $foto_diri->move($path_foto_diri, $foto_diri_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri
        else if (!isset($request->foto_diri))
        {
            $foto_npwp          = $request->file('file_foto_npwp');
            $foto_npwp_name     = $request->foto_npwp;
            $path_foto_npwp     = storage_path('/app/images/foto_npwp');
            $foto_npwp->move($path_foto_npwp, $foto_npwp_name);

            $foto_ktp          = $request->file('file_foto_ktp');
            $foto_ktp_name     = $request->foto_ktp;
            $path_foto_ktp     = storage_path('/app/images/foto_ktp');
            $foto_ktp->move($path_foto_ktp, $foto_ktp_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto ktp
        else if (!isset($request->foto_ktp))
        {
            $foto_npwp          = $request->file('file_foto_npwp');
            $foto_npwp_name     = $request->foto_npwp;
            $path_foto_npwp     = storage_path('/app/images/foto_npwp');
            $foto_npwp->move($path_foto_npwp, $foto_npwp_name);

            $foto_diri          = $request->file('file_foto_diri');
            $foto_diri_name     = $request->foto_diri;
            $path_foto_diri     = storage_path('/app/images/foto_diri');
            $foto_diri->move($path_foto_diri, $foto_diri_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto npwp
        else if (!isset($request->foto_npwp))
        {
            $foto_diri          = $request->file('file_foto_diri');
            $foto_diri_name     = $request->foto_diri;
            $path_foto_diri     = storage_path('/app/images/foto_diri');
            $foto_diri->move($path_foto_diri, $foto_diri_name);

            $foto_ktp          = $request->file('file_foto_ktp');
            $foto_ktp_name     = $request->foto_ktp;
            $path_foto_ktp     = storage_path('/app/images/foto_ktp');
            $foto_ktp->move($path_foto_ktp, $foto_ktp_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi update semua foto
        else 
        {
            $foto_npwp          = $request->file('file_foto_npwp');
            $foto_npwp_name     = $request->foto_npwp;
            $path_foto_npwp     = storage_path('/app/images/foto_npwp');
            $foto_npwp->move($path_foto_npwp, $foto_npwp_name);

            $foto_ktp          = $request->file('file_foto_ktp');
            $foto_ktp_name     = $request->foto_ktp;
            $path_foto_ktp     = storage_path('/app/images/foto_ktp');
            $foto_ktp->move($path_foto_ktp, $foto_ktp_name);

            $foto_diri          = $request->file('file_foto_diri');
            $foto_diri_name     = $request->foto_diri;
            $path_foto_diri     = storage_path('/app/images/foto_diri');
            $foto_diri->move($path_foto_diri, $foto_diri_name);
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        }
    }

    public function updateProfilMitra(Request $request, $id)
    {
        // return response()->json($request->all());

        // Kondisi tidak update semua foto
        if ($request->foto_ktp == '-' && $request->foto_npwp == '-' && $request->foto_diri == '-')
        {
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri dan ktp
        else if ($request->foto_diri == '-' && $request->foto_ktp == '-')
        {

            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri dan npwp
        else if ($request->foto_diri == '-' && $request->foto_npwp == '-')
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto ktp dan npwp
        else if ($request->foto_ktp == '-' && $request->foto_npwp == '-')
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto diri
        else if ($request->foto_diri == '-')
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto ktp
        else if ($request->foto_ktp == '-')
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi tidak update foto npwp
        else if ($request->foto_npwp == '-')
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        } 
        // Kondisi update semua foto
        else 
        {
            
            $updateUser = User::find($id);
            $updateUser->email = $request->email;
            $updateUser->save();

            $updateBiodata = Biodata::where('id_user', $updateUser->id)->first();
            $updateBiodata->nama_lengkap     = $request->nama_lengkap;
            $updateBiodata->tanggal_lahir    = $request->tanggal_lahir;
            $updateBiodata->tempat_lahir     = $request->tempat_lahir;
            $updateBiodata->alamat           = $request->alamat;
            $updateBiodata->foto_npwp        = $request->foto_npwp;
            $updateBiodata->foto_ktp         = $request->foto_ktp;
            $updateBiodata->foto_diri        = $request->foto_diri;
            $updateBiodata->nik              = $request->nik;
            $updateBiodata->no_telp          = $request->no_telp;
            $updateBiodata->save();

            $resp = [
                'UpdateUser'          => $updateUser,
                'UpdateBiodata'       => $updateBiodata
            ];
            return response()->json($resp,200);
        }
    }

    public function changePassword(Request $request)
    {
        $user = User::find($request->id);

        $newPass = password_hash($request->password_baru, PASSWORD_BCRYPT);

        if (Hash::check($request->password_lama, $user->password)) {
            $user->password = $newPass;
            $user->save();
            $resp = [
                'code' => 200,
                'msg' => 'Password berhasil diperbarui!'
            ];
            return response()->json($resp, 200);
        } else {
            $resp = [
                'code' => 401,
                'msg' => 'Password lama yang anda masukan tidak sesuai!'
            ];
            return response()->json($resp ,401);
        }

        
    }
    

}
