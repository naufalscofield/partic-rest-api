<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;
    use SoftDeletes; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $primarykey = 'id';

    protected $fillable = [
        'id', 'name', 'email', 'super_company_id', 'company_id', 'branch_id', 'role'
    ];

    public function Ceo() {
        return $this->role == 'ceo' ? true : false;
    }

    public function Regional() {
        return $this->role == 'regional' ? true : false;
    }

    public function Cabang() {
        return $this->role == 'cabang' ? true : false;
    }

    public function Mitra() {
        return $this->role == 'mitra' ? true : false;
    }

    public function Member() {
        return $this->role == 'member' ? true : false;
    }

    



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
