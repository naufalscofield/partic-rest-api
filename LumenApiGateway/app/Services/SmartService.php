<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class SmartService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume Perusahaans service
     * @var string
     */
    public $baseUri;

    /**
     * Perusahaanization secret to pass to Perusahaan api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.smart.base_uri');
        $this->secret = config('services.smart.secret');
    }

    //Metode smart
    public function smart($datasmart)
    {
        return $this->performRequest('POST', "/smart", $datasmart);
    }
    

}

