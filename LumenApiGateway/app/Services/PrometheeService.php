<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class PrometheeService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume Perusahaans service
     * @var string
     */
    public $baseUri;

    /**
     * Perusahaanization secret to pass to Perusahaan api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.promethee.base_uri');
        $this->secret = config('services.promethee.secret');
    }

    //Metode Promethee
    public function promethee($dataPromethee)
    {
        return $this->performRequest('POST', "/promethee", $dataPromethee);
    }
    

}

