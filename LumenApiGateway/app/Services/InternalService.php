<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class InternalService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume Perusahaans service
     * @var string
     */
    public $baseUri;

    /**
     * Perusahaanization secret to pass to Perusahaan api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.internal.base_uri');
        $this->secret = config('services.internal.secret');
    }


    /**
     * Create Perusahaan
     */
    public function storePerusahaan($dataPerusahaan)
    {
        return $this->performRequest('POST', "/perusahaan", $dataPerusahaan);
    }

     /**
     * Show Regional
     */
    public function showPerusahaan($idPerusahaan)
    {
        return $this->performRequest('GET', "/perusahaan/showPerusahaan/$idPerusahaan");
    }

    public function getPerusahaan()
    {
        // return $id;
        return $this->performRequest('GET', "/perusahaan");
    }
    
    /**
     * Update Regional
     */
    public function updatePerusahaan($dataPerusahaan, $idPerusahaan)
    {
        return $this->performRequest('PUT', "/perusahaan/updatePerusahaan/{$idPerusahaan}", $dataPerusahaan);
    }

    // ===========================================================================================

    /**
     * Create Regional
     */
    public function storeRegional($dataRegional)
    {
        return $this->performRequest('POST', "/regional", $dataRegional);
    }

     /**
     * Get Regional
     */
    public function getRegional($idPerusahaan)
    {
        return $this->performRequest('GET', "/regional/{$idPerusahaan}");
    }

     /**
     * Show Regional
     */
    public function showRegional($id)
    {
        return $this->performRequest('GET', "/regional/show/{$id}");
    }
    
    
    /**
     * Update Regional
     */
    public function updateRegional($updateRegional, $id)
    {
        return $this->performRequest('PUT', "/regional/update/{$id}", $updateRegional);
    }
    
    /**
     * Delete Regional
     */
    public function deleteRegional($id)
    {
        return $this->performRequest('DELETE', "/regional/delete/{$id}");
    }


    // ===========================================================================================
    
    /**
    * Show Cabang By Id Perusahaan
    */
    public function showCabangByIdPerusahaan($idPerusahaan)
    {
       return $this->performRequest('GET', "/cabang/perusahaan/$idPerusahaan");
    }

    public function showCabang($id)
    {
        return $this->performRequest('GET', "/cabang/{$id}");
    }

    public function updateCabang($updateCabang, $id)
    {
        return $this->performRequest('PUT', "/cabang/{$id}", $updateCabang);
    }

    public function deleteCabang($id)
    {
        return $this->performRequest('DELETE', "/cabang/{$id}");
    }

   // ===========================================================================================

   /**
    * Get Tarif By Id Perusahaan
    */
    public function getTarif($idPerusahaan)
    {
       return $this->performRequest('GET', "/perusahaan/getTarif/{$idPerusahaan}");
    }

   /**
    * Store Cabang
    */

    public function storeCabang($data)
    {
       return $this->performRequest('POST', "/cabang", $data);
    }

    // Get Tarif By Kota dan Id Perusahaan
    public function getTarifByKota($idPerusahaan, $kota)
    {
        return $this->performRequest('GET', "/perusahaan/getTarifByKota/{$idPerusahaan}/{$kota}");
    }

    // Get Pengajuan Tarif By Kota dan Id Perusahaan
    public function getPengajuanTarifByKota($idPerusahaan, $kota)
    {
        return $this->performRequest('GET', "/perusahaan/getPengajuanTarifByKota/{$idPerusahaan}/{$kota}");
    }

    // Update Pengajuan Tarif
    public function updatePengajuanTarif($data)
    {
        return $this->performRequest('PUT', "/perusahaan/updatePengajuanTarif", $data);
    }

    //Verifikasi pengiriman
    public function verifikasiPengiriman($id, $data)
    {
        return $this->performRequest('PUT', "pengiriman/verifikasi/{$id}", $data);
    }

    //Get pengiriman
    public function getPengirimanByKota($idPerusahaan, $kota)
    {
        return $this->performRequest('GET', "pengiriman/perusahaan/kota/{$idPerusahaan}/{$kota}");
    }

    //Get pengiriman
    public function getPengirimanByPerusahaan($idPerusahaan)
    {
        return $this->performRequest('GET', "pengiriman/perusahaan/{$idPerusahaan}");
    }
   
    //Show pengiriman
    public function showPengiriman($idPengiriman)
    {
        return $this->performRequest('GET', "pengiriman/{$idPengiriman}");
    }
    
    //Store Pengiriman
    public function storePengiriman($data)
    {
        return $this->performRequest('POST', "pengiriman", $data);
    }

    //Get Pengiriman By Id
    public function getPengirimanById($id)
    {
        return $this->performRequest('GET', "pengiriman/{$id}");
    }
  
    //Get Pengiriman By Cabang
    public function getPengirimanByCabang($idCabang)
    {
        return $this->performRequest('GET', "pengiriman/cabang/{$idCabang}");
    }
   
    //Get Kelompok Pengiriman
    public function getKelompokPengiriman($idRegional)
    {
        return $this->performRequest('GET', "kelompok_pengiriman/{$idRegional}");
    }

    //Show Kelompok Pengiriman
    public function showKelompokPengiriman($idKelompokPengiriman)
    {
        return $this->performRequest('GET', "kelompok_pengiriman/show/{$idKelompokPengiriman}");
    }
   
    //Store Kelompok Pengiriman
    public function storeKelompokPengiriman($data)
    {
        return $this->performRequest('POST', "kelompok_pengiriman", $data);
    }
   
    //Update Kelompok Pengiriman
    public function updateKelompokPengiriman($data)
    {
        return $this->performRequest('PUT', "kelompok_pengiriman", $data);
    }
    //Update Kelompok Pengiriman Final
    public function updateKelompokPengirimanSmart($data)
    {
        return $this->performRequest('PUT', "kelompok_pengiriman/smart", $data);
    }

    //Update Status Pengiriman
    public function updateStatusPengiriman($data, $id)
    {
        return $this->performRequest('PUT', "pengiriman/{$id}", $data);
    }

    //Insert Pengajuan Tarif
    public function storePengajuanTarif($data)
    {
        // print_r($data); die;
        return $this->performRequest('POST', "pengajuanTarif", $data);
    }

    // //Update Pengajuan Tarif
    // public function updatePengajuanTarif($data, $id)
    // {
    //     return $this->performRequest('PUT', "pengajuanTarif/{$id}", $data);
    // }
    
    //Delete Pengajuan Tarif
    public function deletePengajuanTarif($id)
    {
        return $this->performRequest('DELETE', "pengajuanTarif/{$id}");
    }

    public function deletePengajuanTarif2($id)
    {
        return $this->performRequest('DELETE', "deletePengajuanTarif/{$id}");
    }

    //Insert Detail Pengajuan Tarif
    public function storeDetailPengajuanTarif($data)
    {
        return $this->performRequest('POST', "detailPengajuanTarif", $data);
    }

    //Update Detail Pengajuan Tarif
    public function updateDetailPengajuanTarif($data, $id)
    {
        return $this->performRequest('PUT', "detailPengajuanTarif/{$id}", $data);
    }
    
    //Delete Detail Pengajuan Tarif
    public function deleteDetailPengajuanTarif($id)
    {
        return $this->performRequest('DELETE', "detailPengajuanTarif/{$id}");
    }

    //Pickup Pengiriman
    public function pickup($data, $id)
    {
        return $this->performRequest('PUT', "pickup/{$id}", $data);
    }

    //Remove Lobi Pengiriman
    public function removeLobiPengiriman($id)
    {
        return $this->performRequest('DELETE', "removeLobiPengiriman/{$id}");
    }

    //Get Tarif Pelanggan
    public function getTarifPelanggan($idPerusahaan)
    {
        return $this->performRequest('GET', "perusahaan/tarifPelanggan/{$idPerusahaan}");
    }

    //Show Tarif Pelanggan
    public function showTarifPelanggan($idTarif)
    {
        return $this->performRequest('GET', "perusahaan/tarifPelanggan/show/{$idTarif}");
    }
   
    //Show Tarif Pelanggan
    public function showTarifPelangganByParams($idPerusahaan, $jenisPengangkutan, $jenisPengiriman, $namaBarang, $kotaAsal, $kotaTujuan)
    {
        return $this->performRequest('GET', "perusahaan/tarifPelanggan/showByParams/{$idPerusahaan}/{$jenisPengangkutan}/{$jenisPengiriman}/{$namaBarang}/{$kotaAsal}/{$kotaTujuan}");
    }

    //Store Tarif Pelanggan
    public function storeTarifPelanggan($data, $idPerusahaan)
    {
        return $this->performRequest('POST', "perusahaan/tarifPelanggan/{$idPerusahaan}", $data);
    }

    //Update Tarif Pelanggan
    public function updateTarifPelanggan($data, $idPerusahaan)
    {
        return $this->performRequest('PUT', "perusahaan/tarifPelanggan/{$idPerusahaan}", $data);
    }

    //Delete Tarif Pelanggan
    public function deleteTarifPelanggan($idTarif)
    {
        return $this->performRequest('DELETE', "perusahaan/tarifPelanggan/{$idTarif}");
    }

    //Get PIC Cabang By Id Regional
    public function getPicCabangByIdRegional($idRegional)
    {
        return $this->performRequest('GET', "regional/picCabang/regional/{$idRegional}");
    }

    public function getCabangByIdRegional($id_regional)
    {
        return $this->performRequest('GET', "regional/cabang/regional/{$id_regional}");
    }

    //
    public function getFieldKelompokPengiriman($kota, $id_perusahaan)
    {
        return $this->performRequest('GET', "field_kelompok_pengiriman/{$kota}/{$id_perusahaan}");
    }
    //
    public function getField2KelompokPengiriman($tujuan, $kota, $id_perusahaan)
    {
        return $this->performRequest('GET', "field2_kelompok_pengiriman/{$tujuan}/{$kota}/{$id_perusahaan}");
    }
    //
    public function getField3KelompokPengiriman($tujuan, $jenis_pengangkutan, $kota, $id_perusahaan)
    {
        return $this->performRequest('GET', "field3_kelompok_pengiriman/{$tujuan}/{$jenis_pengangkutan}/{$kota}/{$id_perusahaan}");
    }
    //
    public function getField4KelompokPengiriman($tujuan, $jenis_pengangkutan, $jenis_pengiriman, $kota, $id_perusahaan)
    {
        return $this->performRequest('GET', "field4_kelompok_pengiriman/{$tujuan}/{$jenis_pengangkutan}/{$jenis_pengiriman}/{$kota}/{$id_perusahaan}");
    }

    public function getNamaPerusahaan()
    {
        return $this->performRequest('GET', "getNamaPerusahaan");
    }

    public function getTarifByIdMitra($idMitra)
    {
        // return $idMitra;
        return $this->performRequest('GET', "getTarifByIdMitra/{$idMitra}");
    }

    public function getLobiMitraByIdMitra($id)
    {
        // return $idMitra;
        return $this->performRequest('GET', "getLobiMitraByIdMitra/{$id}");
    }

    public function getPengirimanByIdKelompokPengiriman($id)
    {
        // return $idMitra;
        return $this->performRequest('GET', "getPengirimanByIdKelompokPengiriman/{$id}");
    }

    public function getPengirimanPickUpByIdKelompokPengiriman($id)
    {
        // return $id;
        return $this->performRequest('GET', "getPengirimanPickUpByIdKelompokPengiriman/{$id}");
    }


    public function updateStatusPickUp($id)
    {
        // return $id;
        return $this->performRequest('PUT', "updateStatusPickUp/{$id}");
    }

    public function inputStatusPengiriman($data)
    {
        // return response()->json($data);
        return $this->performRequest('POST', "inputStatusPengiriman", $data);
    }

    public function updateTanggalSampai($id)
    {
        // return response()->json($data);
        return $this->performRequest('PUT', "updateTanggalSampai/{$id}");
    }

    public function getRiwayatPengirimanPickUpByIdKelompokPengiriman($id)
    {
        // return $id;
        return $this->performRequest('GET', "getRiwayatPengirimanPickUpByIdKelompokPengiriman/{$id}");
    }

    // Update Pengajuan Tarif
    public function updatePengajuanTarifByMitra($data, $id)
    {
        // return $id;
        return $this->performRequest('PUT', "/updatePengajuanTarifByMitra/{$id}", $data);
    }

    public function cekResiStatus($no_resi)
    {
        // return $no_resi;
        return $this->performRequest('GET', "cekResiStatus/{$no_resi}");
    }

    public function cekResiKeterangan($no_resi)
    {
        // return $no_resi;
        return $this->performRequest('GET', "cekResiKeterangan/{$no_resi}");
    }

    public function getKendaraan($id)
    {
        // return $id;
        return $this->performRequest('GET', "getKendaraan/{$id}");
    }

    public function getCabangByIdPerusahaan($id)
    {
        // return $id;
        return $this->performRequest('GET', "getCabangByIdPerusahaan/{$id}");
    }

    public function getRiwayatPengirimanByIdUsers($id)
    {
        // return $id;
        return $this->performRequest('GET', "getRiwayatPengirimanByIdUsers/{$id}");
    }

    public function getPendapatanMitra($id)
    {
        // return $id;
        return $this->performRequest('GET', "getPendapatanMitra/{$id}");
    }

    public function inputPengiriman($data)
    {
        // return $id;
        return $this->performRequest('POST', "inputPengiriman",$data);
    }

    public function updateJumlahHariTelat($id,$hari)
    {
        // return response()->json($data);
        return $this->performRequest('PUT', "updateJumlahHariTelat/{$id}/{$hari}");
    }

    public function loginMobile($data)
    {
        // return response()->json('$data');
        return $this->performRequest('POST', "loginMobile",$data);
    }

    public function updatePengirimanCoor($data)
    {
        // return response()->json('$data');
        return $this->performRequest('PUT', "updatePengirimanCoor",$data);
    }
}

