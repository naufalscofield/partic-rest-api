<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class ExternalService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume Perusahaans service
     * @var string
     */
    public $baseUri;

    /**
     * Perusahaanization secret to pass to Perusahaan api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.external.base_uri');
        $this->secret = config('services.external.secret');
    }

    public function storeMitra($dataMitra)
    {
        return $this->performRequest('POST', "/mitra", $dataMitra);
    }

    public function getMitra()
    {
        return $this->performRequest('GET', "/mitra");
    }

    public function showMitra($id)
    {
        return $this->performRequest('GET', "/mitra/{$id}");
    }

    public function showMitraByIdUser($idUser)
    {
        return $this->performRequest('GET', "/mitra/getMitraByIdUser/{$idUser}");
    }

    public function verifikasiMitra($idMitra)
    {
        return $this->performRequest('PUT', "/mitra/verifikasiMitra/{$idMitra}");
    }

    public function getMitraByKota($kota, $tujuan)
    {
        return $this->performRequest('GET', "/mitra/getMitraByKota/{$kota}/{$tujuan}");
    }

    public function getReportMitra($idPerusahaan)
    {
        return $this->performRequest('GET', "/mitra/getReportMitra/{$idPerusahaan}");
    }

    // =====================================================================================

    //Verifikasi member
    public function verifikasiMember($id)
    {
        return $this->performRequest('PUT', "member/verifikasi/{$id}");
    }

    public function storeMember($dataMember)
    {
        return $this->performRequest('POST', "/member", $dataMember);
    }

    public function getMemberByIdPerusahaan($idPerusahaan, $no)
    {
        // return $idPerusahaan;
        return $this->performRequest('GET', "/member/getMemberByIdPerusahaan/{$idPerusahaan}/{$no}");
    }

    public function deleteMember($id)
    {
        return $this->performRequest('DELETE', "/member/delete/{$id}");
    }

    public function getKendaraan($id)
    {
        // return $id;
        return $this->performRequest('GET', "/getKendaraan/{$id}");
    }

    public function getMemberByIdUsers($id)
    {
        // return $id;
        return $this->performRequest('GET', "/getMemberByIdUsers/{$id}");
    }

    public function getPengirimanByIdUsers($id)
    {
        // return $id;
        return $this->performRequest('GET', "/getPengirimanByIdUsers/{$id}");
    }

    public function pengajuanMember($dataMember)
    {
        // return $id;
        return $this->performRequest('POST', "/pengajuanMember", $dataMember);
    }

    public function updateStatusMitra($id)
    {
        // return $id;
        return $this->performRequest('PUT', "/updateStatusMitra/{$id}");
    }

    public function getBbmByIdBbm($id)
    {
        // return $id;
        return $this->performRequest('GET', "/getBbmByIdBbm/{$id}");
    }

    public function updateKendaraanMitra($data, $id)
    {
        // return $id;
        return $this->performRequest('PUT', "/updateKendaraanMitra/{$id}", $data);
    }

    public function getBbm()
    {
        // return $id;
        return $this->performRequest('GET', "/getBbm");
    }
}