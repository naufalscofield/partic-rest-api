<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->integer('id_perusahaan')->nullable();
            $table->integer('id_regional')->nullable();
            $table->integer('id_cabang')->nullable();
            $table->string('role');
            $table->integer('verifikasi');
            $table->integer('verifikasi');
            $table->timestamps('email_verified_at')->nullable();
            $table->longText('email_token');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
