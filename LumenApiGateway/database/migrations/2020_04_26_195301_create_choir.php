<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choir', function (Blueprint $table) {
            $table->increments('id');
            $table->string('umur');
            $table->string('jenis_kelamin');
            $table->string('memiliki_penyakit_pendamping');
            $table->string('masih_rutin_beraktifitas_diluar_rumah');
            $table->string('berdomisil_di_wilayah_epicentrum');
            $table->string('memiliki_riwayat_berpergian_ke_luar_negeri');
            $table->string('pernah_kontak_dengan_pasien_covid19');
            $table->string('berpotensi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('choir');
    }
}
