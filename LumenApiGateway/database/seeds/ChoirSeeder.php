<?php

use Illuminate\Database\Seeder;

class ChoirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Choir::class, 300)->create();
    }
}
