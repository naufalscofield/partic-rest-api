<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//Route Umum
$router->post('loginMobile', 'LoginController@loginMobile');
$router->post('login', 'LoginController@login');
$router->post('aktivasiAkun', 'UserController@aktivasi');
$router->post('aktivasiAkunEksternal/{id}', 'UserController@aktivasiAkunEksternal');
$router->post('registrasiPerusahaan', 'PerusahaanController@store');
$router->post('registrasiMitra', 'MitraController@store'); 
$router->post('registrasiMember', 'MemberController@store'); 
$router->get('perusahaan', 'PerusahaanController@get');
$router->get('cekResiStatus/{noResi}', 'MitraController@cekResiStatus');
$router->get('cekResiKeterangan/{noResi}', 'MitraController@cekResiKeterangan');
$router->put('updatePengirimanCoor', 'PerusahaanController@updatePengirimanCoor'); 
$router->get('getBbm', 'MitraController@getBbm'); 


//Route Untuk Semua Role Internal
$router->group(['middleware' => ['role:ceo,regional,cabang,mitra,member', 'client.credentials']], function() use ($router) {
    $router->get('showProfil/{id}', 'ProfilController@show'); 
    $router->post('updateProfil/{id}', 'ProfilController@update');
    $router->put('updateProfilMitra/{id}', 'ProfilController@updateProfilMitra');
    $router->put('changePassword', 'ProfilController@changePassword'); 
    $router->post('profil', 'ProfilController@showByEmail'); 
    $router->post('profilMitra', 'ProfilController@showByEmailMitra'); 
    $router->post('profilMember', 'ProfilController@showByEmailMember'); 
    $router->post('profilEksternal', 'ProfilController@showByEmailEksternal'); 
    // $router->post('profilEksternal', 'ProfilController@showByEmailEksternal'); 
    $router->post('updateTokenEmail', 'ProfilController@updateTokenEmail'); 
});

//Route Untuk CEO Perusahaan
$router->group(['prefix' => 'ceo', 'middleware' => ['role:ceo', 'client.credentials']], function() use ($router) {
    $router->post('regional', 'RegionalController@store'); 

    $router->get('cabang/perusahaan/{idPerusahaan}', 'CabangController@showByIdPerusahaan');

    $router->get('regional/perusahaan/{id_perusahaan}', 'RegionalController@get'); 
    $router->get('regional/{id}', 'RegionalController@show'); 
    $router->put('regional/{id}', 'RegionalController@update'); 
    $router->delete('regional/{id}', 'RegionalController@delete'); 

    $router->get('perusahaan/showPerusahaan/{id}', 'PerusahaanController@showPerusahaan'); 
    $router->put('perusahaan/updatePerusahaan/{id}', 'PerusahaanController@updatePerusahaan');
    
    $router->get('mitra/perusahaan/{idPerusahaan}', 'MitraController@getByIdPerusahaan'); 

    $router->get('tarif/perusahaan/{idPerusahaan}', 'TarifController@getByIdPerusahaan'); 
    
    $router->get('tarifPelanggan/perusahaan/{idPerusahaan}', 'TarifController@getTarifPelanggan'); 

    $router->get('pengiriman/{idPerusahaan}', 'PerusahaanController@getPengiriman'); 
    $router->get('pengiriman/id/{idpengiriman}', 'PerusahaanController@showPengiriman'); 


    
});

//Route Untuk CEO Perusahaan & PIC / Pegawai Regional
$router->group(['middleware' => ['role:ceo,regional', 'client.credentials']], function() use ($router) {
    $router->post('regional/storePegawai', 'RegionalController@storePegawai');
    $router->get('regional/getPegawai/{idPerusahaan}', 'RegionalController@getPegawai');
    $router->get('regional/showPegawai/{id}', 'RegionalController@showPegawai');
    $router->put('regional/updatePegawai/{id}', 'RegionalController@updatePegawai');
    $router->delete('regional/deletePegawai/{id}', 'RegionalController@deletePegawai');

});

//Route Untuk Pegawai / PIC Regional
$router->group(['prefix' => 'regional', 'middleware' => ['role:regional', 'client.credentials']], function() use ($router) {
    $router->post('cabang', 'CabangController@store');
    $router->get('cabang/{id}', 'CabangController@show');
    $router->put('cabang/{id}', 'CabangController@update');
    $router->delete('cabang/{id}', 'CabangController@delete');
    $router->get('cabang/perusahaan/{idPerusahaan}', 'CabangController@showByIdPerusahaan');

    $router->get('cabang/regional/{id_regional}', 'CabangController@showByIdRegional');

    $router->get('picCabang/{id_perusahaan}/{id_regional}', 'CabangController@getPicCabangByIdRegional');
    $router->delete('picCabang/{id}', 'CabangController@deletePegawai');


    $router->put('mitra/verifikasi/{idMitra}', 'RegionalController@verifikasiMitra');
    $router->get('mitra/{idPerusahaan}/{kota}', 'RegionalController@getMitraByKota');
    $router->get('mitra/{idMitra}', 'RegionalController@showMitra');
    $router->get('tarif/{idPerusahaan}/{kota}', 'RegionalController@getTarifByKota');

    $router->get('pengajuanTarif/{idPerusahaan}/{kota}', 'RegionalController@getPengajuanTarifByKota');
    $router->put('pengajuanTarif', 'RegionalController@updatePengajuanTarif');

    $router->put('pengiriman/vefifikasi/{id}', 'RegionalController@verifikasiPengiriman'); 
    $router->get('pengiriman/kota/{idPerusahaan}/{kota}', 'RegionalController@getPengirimanByKota'); 
    $router->get('pengiriman/{idpengiriman}', 'RegionalController@showPengiriman'); 

    $router->get('kelompok_pengiriman/{idRegional}', 'RegionalController@getKelompokPengiriman'); 
    $router->post('kelompok_pengiriman', 'RegionalController@storeKelompokPengiriman'); 
    $router->get('field_kelompok_pengiriman/{kota}/{id_perusahaan}', 'RegionalController@getFieldKelompokPengiriman'); 
    $router->get('field2_kelompok_pengiriman/{tujuan}/{kota}/{id_perusahaan}', 'RegionalController@getField2KelompokPengiriman'); 
    $router->get('field3_kelompok_pengiriman/{tujuan}/{jenis_pengangkutan}/{kota}/{id_perusahaan}', 'RegionalController@getField3KelompokPengiriman'); 
    $router->get('field4_kelompok_pengiriman/{tujuan}/{jenis_pengangkutan}/{jenis_pengiriman}/{kota}/{id_perusahaan}', 'RegionalController@getField4KelompokPengiriman'); 

    $router->put('member/verifikasi/{id}', 'RegionalController@verifikasiMember'); 
    $router->get('member/{idPerusahaan}/{no}', 'MemberController@getByIdPerusahaan'); 
    $router->delete('member/{id}', 'memberController@delete'); 

    $router->get('regional/{id}', 'RegionalController@show'); 
    $router->get('regional/perusahaan/{id_perusahaan}', 'RegionalController@get'); 
    
    $router->put('updateRegional/{id}', 'RegionalController@update');

    $router->get('tarifPelanggan/perusahaan/{idPerusahaan}', 'TarifController@getTarifPelanggan'); 
    $router->post('tarifPelanggan/{idPerusahaan}', 'TarifController@storeTarifPelanggan'); 
    $router->get('tarifPelanggan/{idTarif}', 'TarifController@showTarifPelanggan'); 
    $router->put('tarifPelanggan/{idPerusahaan}', 'TarifController@updateTarifPelanggan'); 
    $router->delete('tarifPelanggan/{idTarif}', 'TarifController@deleteTarifPelanggan'); 

    $router->get('promethee/{idKelompokPengiriman}', 'CabangController@promethee');
    $router->put('promethee', 'CabangController@storePromethee');

    $router->post('smart', 'RegionalController@smart');
    $router->put('smartFinal', 'RegionalController@smartFinal');

});

//Route Untuk Pegawai / PIC Regional & Pegawai / PIC Cabang
$router->group(['middleware' => ['role:regional, cabang', 'client.credentials']], function() use ($router) {
    $router->post('cabang/storePegawai', 'CabangController@storePegawai');
    $router->get('cabang/showPegawai/{id}', 'CabangController@showPegawai');

});

//Route Untuk Pegawai Cabang
$router->group(['prefix' => 'cabang', 'middleware' => ['role:cabang', 'client.credentials']], function() use ($router) {
    $router->get('pengiriman/{idCabang}', 'CabangController@getPengirimanByCabang');
    $router->post('pengiriman', 'CabangController@storePengiriman');
    $router->get('pengiriman/id/{idpengiriman}', 'PerusahaanController@showPengiriman'); 
    
    $router->put('pengiriman/updateStatus/{id}', 'CabangController@updateStatusPengiriman');
    
    $router->get('/cabang/{id}', 'CabangController@show');
    $router->put('/cabang/{id}', 'CabangController@update');

    $router->get('regional/{id}', 'RegionalController@show'); 

    $router->get('tarifPelanggan/{idPerusahaan}/{jenisPengangkutan}/{jenisPengiriman}/{namaBarang}/{kotaAsal}/{kotaTujuan}', 'TarifController@showTarifPelangganByParams'); 
    
    $router->get('/getPegawai/{idPerusahaan}/{idRegional}/{idCabang}', 'CabangController@getPegawai');
    $router->delete('/deletePegawai/{id}', 'CabangController@deletePegawai');
    
    $router->get('promethee/{idKelompokPengiriman}', 'CabangController@promethee');
});


//Route Untuk Mitra
$router->group(['prefix' => 'mitra', 'middleware' => ['role:mitra', 'client.credentials']], function() use ($router) {
    $router->post('pengajuanTarif', 'MitraController@storePengajuanTarif');
    $router->post('detailPengajuanTarif', 'MitraController@storeDetailPengajuanTarif');

    $router->put('pengajuanTarif/{id}', 'MitraController@updatePengajuanTarif');
    $router->put('detailPengajuanTarif/{id}', 'MitraController@updateDetailpengajuanTarif');
    
    $router->delete('pengajuanTarif/{id}', 'MitraController@deletePengajuanTarif');
    $router->delete('detailPengajuanTarif/{id}', 'MitraController@deleteDetailpengajuanTarif');

    $router->put('pickup/{id}', 'MitraController@pickupPengiriman');

    $router->get('getNamaPerusahaan', 'MitraController@getNamaPerusahaan');


    $router->get('getTarifByIdMitra/{id}', 'MitraController@getTarifByIdMitra');
    // $router->get('getTarifByIdKelompokPengiriman/{id}', 'MitraController@getTarifByIdKelompokPengiriman');
    $router->put('updatePengajuanTarif/{id}', 'MitraController@updatePengajuanTarif');
    $router->delete('deletePengajuanTarif/{id}', 'MitraController@deletePengajuanTarif2');

    $router->get('getLobiMitraByIdMitra/{id}', 'MitraController@getLobiMitraByIdMitra');
    $router->get('getPengirimanByIdKelompokPengiriman/{id}', 'MitraController@getPengirimanByIdKelompokPengiriman');
    $router->get('getPengirimanPickUpByIdKelompokPengiriman/{id}', 'MitraController@getPengirimanPickUpByIdKelompokPengiriman');

    $router->put('updateStatusPickUp/{id}', 'MitraController@updateStatusPickUp');
    $router->post('inputStatusPengiriman', 'MitraController@inputStatusPengiriman');

    $router->put('updateTanggalSampai/{id}', 'MitraController@updateTanggalSampai');

    $router->get('getRiwayatPengirimanPickUpByIdKelompokPengiriman/{id}', 'MitraController@getRiwayatPengirimanPickUpByIdKelompokPengiriman');
    
    $router->get('getKendaraan/{id}', 'MitraController@getKendaraan');
    $router->get('getPendapatanMitra/{id}', 'MitraController@getPendapatanMitra');
    $router->post('inputPengiriman', 'MitraController@inputPengiriman');
    $router->put('updateStatusMitra/{id}', 'MitraController@updateStatusMitra');
    $router->get('getBbmByIdBbm/{id}', 'MitraController@getBbmByIdBbm');
    $router->put('updateJumlahHariTelat/{id}/{hari}', 'MitraController@updateJumlahHariTelat');
    $router->put('updateKendaraanMitra/{id}', 'MitraController@updateKendaraanMitra');
});

//Route Untuk Mitra
$router->group(['prefix' => 'member', 'middleware' => ['role:member', 'client.credentials']], function() use ($router) {
    $router->get('getRiwayatPengirimanByIdUsers/{$id}', 'MemberController@getRiwayatPengirimanByIdUsers');
    $router->get('getNamaPerusahaan', 'MemberController@getNamaPerusahaan');
    $router->get('getMemberByIdUsers/{id}', 'MemberController@getMemberByIdUsers');
    $router->get('getPengirimanByIdUsers/{id}', 'MemberController@getPengirimanByIdUsers');
    $router->get('getCabangByIdPerusahaan/{id}', 'MemberController@getCabangByIdPerusahaan');
    $router->get('getRiwayatPengirimanByIdUsers/{id}', 'MemberController@getRiwayatPengirimanByIdUsers');
    $router->post('pengajuanMember', 'MemberController@pengajuanMember');
});