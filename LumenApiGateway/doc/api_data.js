define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "C:\\xampp74\\htdocs\\partic-restapi\\LumenApiGateway\\doc\\main.js",
    "groupTitle": "C:\\xampp74\\htdocs\\partic-restapi\\LumenApiGateway\\doc\\main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/cabang/perusahaan/{idPerusahaan}",
    "title": "",
    "version": "0.1.0",
    "name": "Cabang_By_Id_Perusahaan",
    "group": "Role_CEO",
    "permission": [
      {
        "name": "role based"
      }
    ],
    "description": "<p>digunakan untuk melihat semua cabang yang dimiliki suatu perusahaan.</p>",
    "examples": [
      {
        "title": "Cara penggunaan:",
        "content": "http://localhost:9000/ceo/cabang/perusahaan/{idPerusahaan}",
        "type": "json"
      }
    ],
    "filename": "./index.js",
    "groupTitle": "Role_CEO"
  },
  {
    "type": "post",
    "url": "/registrasiPerusahaan",
    "title": "",
    "version": "0.1.0",
    "name": "Registrasi_Perusahaan",
    "group": "Umum",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk registrasi perusahaan dan registrasi akun CEO perusahaan.</p>",
    "examples": [
      {
        "title": "Cara penggunaan:",
        "content": "http://localhost:9000/registrasiPerusahaan",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nama_perusahaan",
            "description": "<p>nama perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "alamat_perusahaan",
            "description": "<p>alamat perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "kota",
            "description": "<p>kota perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "logo",
            "description": "<p>logo perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>email untuk CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>password untuk CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nama_lengkap",
            "description": "<p>nama lengkap CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tanggal_lahir",
            "description": "<p>tanggal lahir CEO perusahaan logistik yang akan didaftarkan (YYYY-MM-DD)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tempat_lahir",
            "description": "<p>tempat lahir CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "foto_ktp",
            "description": "<p>nama file foto ktp CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "foto_npwp",
            "description": "<p>nama file foto npwp (jika ada) CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nik",
            "description": "<p>nik CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "foto_diri",
            "description": "<p>nama file foto diri CEO perusahaan logistik yang akan didaftarkan</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "alamat",
            "description": "<p>alamat tinggal CEO perusahaan logistik yang akan didaftarkan</p>"
          }
        ]
      }
    },
    "filename": "./index.js",
    "groupTitle": "Umum"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "",
    "version": "0.1.0",
    "name": "login",
    "group": "Umum",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk login semua user semua platform.</p>",
    "examples": [
      {
        "title": "Cara penggunaan:",
        "content": "http://localhost:9000/login",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>email yang sudah terregistrasi</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>password yang sudah terregistrasi</p>"
          }
        ]
      }
    },
    "filename": "./index.js",
    "groupTitle": "Umum"
  }
] });
