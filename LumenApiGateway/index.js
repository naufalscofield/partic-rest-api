/**
* @api {post} /login
* @apiVersion 0.1.0
* @apiName login
* @apiGroup Umum
* @apiPermission public
*
* @apiDescription digunakan untuk login semua user semua platform.
*
* @apiExample Cara penggunaan:
* http://localhost:9000/login
*
* @apiParam {string} email email yang sudah terregistrasi
* @apiParam {string} password password yang sudah terregistrasi
*/

/**
* @api {post} /registrasiPerusahaan
* @apiVersion 0.1.0
* @apiName Registrasi Perusahaan
* @apiGroup Umum
* @apiPermission public
*
* @apiDescription digunakan untuk registrasi perusahaan dan registrasi akun CEO perusahaan.
*
* @apiExample Cara penggunaan:
* http://localhost:9000/registrasiPerusahaan
*
* @apiParam {string} nama_perusahaan nama perusahaan logistik yang akan didaftarkan
* @apiParam {string} alamat_perusahaan alamat perusahaan logistik yang akan didaftarkan
* @apiParam {string} kota kota perusahaan logistik yang akan didaftarkan
* @apiParam {string} logo logo perusahaan logistik yang akan didaftarkan
* @apiParam {string} email email untuk CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} password password untuk CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} nama_lengkap nama lengkap CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} tanggal_lahir tanggal lahir CEO perusahaan logistik yang akan didaftarkan (YYYY-MM-DD)
* @apiParam {string} tempat_lahir tempat lahir CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} foto_ktp nama file foto ktp CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} foto_npwp nama file foto npwp (jika ada) CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} nik nik CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} foto_diri nama file foto diri CEO perusahaan logistik yang akan didaftarkan
* @apiParam {string} alamat alamat tinggal CEO perusahaan logistik yang akan didaftarkan
*/

/**
* @api {get} /cabang/perusahaan/{idPerusahaan}
* @apiVersion 0.1.0
* @apiName Cabang By Id Perusahaan
* @apiGroup Role CEO
* @apiPermission role based
*
* @apiDescription digunakan untuk melihat semua cabang yang dimiliki suatu perusahaan.
*
* @apiExample Cara penggunaan:
* http://localhost:9000/ceo/cabang/perusahaan/{idPerusahaan}
*
*/

