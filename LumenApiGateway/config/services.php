<?php

return [
    'internal'   =>  [
        'base_uri'  =>  env('INTERNAL_SERVICE_BASE_URL'),
        'secret'  =>  env('INTERNAL_SERVICE_SECRET'),
    ],
    'external'   =>  [
        'base_uri'  =>  env('EXTERNAL_SERVICE_BASE_URL'),
        'secret'  =>  env('EXTERNAL_SERVICE_SECRET'),
    ],
    'promethee'   =>  [
        'base_uri'  =>  env('PROMETHEE_SERVICE_BASE_URL'),
        'secret'  =>  env('PROMETHEE_SERVICE_SECRET'),
    ],
    'smart'   =>  [
        'base_uri'  =>  env('SMART_SERVICE_BASE_URL'),
        'secret'  =>  env('SMART_SERVICE_SECRET'),
    ],
];