<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class SmartController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function smart(Request $request)
    {

        if ($request['kelompok']['calon_mitra3'] != NULL)
        {
            $mitra1     = DB::table('mitra')
                        ->where('id', $request['kelompok']['calon_mitra1'])
                        ->first();

            $bio1       = DB::table('biodata')
                        ->where('id_user', $mitra1->id_user)
                        ->first();

            $tarif1     = DB::table('tarif')
                        ->where('id', $request['kelompok']['id_tarif_line1'])
                        ->first();
            // return response()->json($tarif1->tarif_normal);

            $mitra2     = DB::table('mitra')
                        ->where('id', $request['kelompok']['calon_mitra2'])
                        ->first();

            $bio2       = DB::table('biodata')
                        ->where('id_user', $mitra2->id_user)
                        ->first();

            $tarif2     = DB::table('tarif')
                        ->where('id', $request['kelompok']['id_tarif_line2'])
                        ->first();

            $mitra3     = DB::table('mitra')
                        ->where('id', $request['kelompok']['calon_mitra3'])
                        ->first();

            $bio3       = DB::table('biodata')
                        ->where('id_user', $mitra3->id_user)
                        ->first();

            $tarif3     = DB::table('tarif')
                        ->where('id', $request['kelompok']['id_tarif_line3'])
                        ->first();

            $durasi1 = [
                'mitra'     => $mitra1->id,
                'durasi'    => $tarif1->durasi,
                'nama'      => $bio1->nama_lengkap
            ];

            $durasi2 = [
                'mitra'     => $mitra2->id,
                'durasi'    => $tarif2->durasi,
                'nama'      => $bio2->nama_lengkap
            ];

            $durasi3 = [
                'mitra'     => $mitra3->id,
                'durasi'    => $tarif3->durasi,
                'nama'      => $bio3->nama_lengkap
            ];

            $tarif1 = [
                'mitra'     => $mitra1->id,
                'tarif'     => $tarif1->tarif_normal,
                'nama'      => $bio1->nama_lengkap
            ];

            $tarif2 = [
                'mitra'     => $mitra2->id,
                'tarif'     => $tarif2->tarif_normal,
                'nama'      => $bio2->nama_lengkap
            ];

            $tarif3 = [
                'mitra'     => $mitra3->id,
                'tarif'     => $tarif3->tarif_normal,
                'nama'      => $bio3->nama_lengkap
            ];


            //DURASI 1
            if ($durasi1['durasi'] < $durasi2['durasi'] && $durasi1['durasi'] < $durasi3['durasi'])
            {
                //kecil kecil
                $d1 = 'cepat';
            }else if ($durasi1['durasi'] > $durasi2['durasi'] && $durasi1['durasi'] < $durasi3['durasi']) 
            {
                //besar kecil
                $d1 = 'sedang';
            }else if ($durasi1['durasi'] < $durasi2['durasi'] && $durasi1['durasi'] > $durasi3['durasi']) 
            {
                //kecil besar
                $d1 = 'sedang';
            }else if ($durasi1['durasi'] > $durasi2['durasi'] && $durasi1['durasi'] > $durasi3['durasi']) 
            {
                //besar besar
                $d1 = 'lambat';
            }else if ($durasi1['durasi'] == $durasi2['durasi'] && $durasi1['durasi'] > $durasi3['durasi']) 
            {
                //sama besar
                $d1 = 'sedang';
            } else if ($durasi1['durasi'] == $durasi2['durasi'] && $durasi1['durasi'] < $durasi3['durasi'])
            {
                //sama kecil
                $d1 = 'cepat';
            } else if ($durasi1['durasi'] > $durasi2['durasi'] && $durasi1['durasi'] == $durasi3['durasi'])
            {
                //besar sama
                $d1 = 'sedang';
            } else if ($durasi1['durasi'] < $durasi2['durasi'] && $durasi1['durasi'] == $durasi3['durasi'])
            {
                //besar kecil
                $d1 = 'cepat';
            } else if ($durasi1['durasi'] == $durasi2['durasi'] && $durasi1['durasi'] == $durasi3['durasi'])
            {
                //sama sama
                $d1 = 'cepat';
            } else {
                $d1 = 'sedang';
            }

            //DURASI 2
            if ($durasi2['durasi'] < $durasi1['durasi'] && $durasi2['durasi'] < $durasi3['durasi'])
            {
                //kecil kecil
                $d2 = 'cepat';
            } else if ($durasi2['durasi'] > $durasi1['durasi'] && $durasi2['durasi'] < $durasi3['durasi']) 
            {
                //besar kecil
                $d2 = 'sedang';
            } else if ($durasi2['durasi'] < $durasi1['durasi'] && $durasi2['durasi'] > $durasi3['durasi']) 
            {
                //kecil besar
                $d2 = 'sedang';
            } else if ($durasi2['durasi'] > $durasi1['durasi'] && $durasi2['durasi'] > $durasi3['durasi']) 
            {
                //besar besar
                $d2 = 'lambat';
            } else if ($durasi2['durasi'] == $durasi1['durasi'] && $durasi2['durasi'] > $durasi3['durasi']) 
            {
                //sama besar
                $d2 = 'sedang';
            } else if ($durasi2['durasi'] == $durasi1['durasi'] && $durasi2['durasi'] < $durasi3['durasi'])
            {
                // sama kecil
                $d2 = 'cepat';
            } else if ($durasi2['durasi'] > $durasi1['durasi'] && $durasi2['durasi'] == $durasi3['durasi'])
            {
                //besar sama
                $d2 = 'sedang';
            } else if ($durasi2['durasi'] < $durasi1['durasi'] && $durasi2['durasi'] == $durasi3['durasi'])
            {
                //besar kecil
                $d2 = 'cepat';
            } else if ($durasi2['durasi'] == $durasi1['durasi'] && $durasi2['durasi'] == $durasi3['durasi'])
            {
                //sama sama
                $d2 = 'cepat';
            } else {
                $d2 = 'sedang';
            }
            
            //DURASI 3
            if ($durasi3['durasi'] < $durasi1['durasi'] && $durasi3['durasi'] < $durasi2['durasi'])
            {
                //kecil kecil
                $d3 = 'cepat';
            } else if ($durasi3['durasi'] > $durasi1['durasi'] && $durasi3['durasi'] < $durasi2['durasi']) 
            {
                //besar kecil
                $d3 = 'sedang1';
            } else if ($durasi3['durasi'] < $durasi1['durasi'] && $durasi3['durasi'] > $durasi2['durasi']) 
            {
                //kecil besar
                $d3 = 'sedang2';
            } else if ($durasi3['durasi'] > $durasi1['durasi'] && $durasi3['durasi'] > $durasi2['durasi']) 
            {
                //besar besar
                $d3 = 'lambat';
            } else if ($durasi3['durasi'] == $durasi1['durasi'] && $durasi3['durasi'] > $durasi2['durasi']) 
            {
                // sama besar
                $d3 = 'sedang';
            } else if ($durasi3['durasi'] == $durasi1['durasi'] && $durasi3['durasi'] < $durasi2['durasi'])
            {
                // sama kecil
                $d3 = 'cepat';
            } else if ($durasi3['durasi'] > $durasi1['durasi'] && $durasi3['durasi'] == $durasi2['durasi'])
            {
                //besar sama
                $d3 = 'sedang';
            } else if ($durasi3['durasi'] < $durasi1['durasi'] && $durasi3['durasi'] == $durasi2['durasi'])
            {
                //besar kecil
                $d3 = 'cepat';
            } else if ($durasi3['durasi'] == $durasi1['durasi'] && $durasi3['durasi'] == $durasi2['durasi'])
            {
                //sama sama
                $d3 = 'cepat';
            } else {
                $d3 = 'sedang';
            }

            //TARIF 1
            if ($tarif1['tarif'] < $tarif2['tarif'] && $tarif1['tarif'] < $tarif3['tarif'])
            {
                //kecil kecil
                $t1 = 'murah';
            } else if ($tarif1['tarif'] > $tarif2['tarif'] && $tarif1['tarif'] < $tarif3['tarif']) 
            {
                //besar kecil
                $t1 = 'sedang';
            } else if ($tarif1['tarif'] < $tarif2['tarif'] && $tarif1['tarif'] > $tarif3['tarif']) 
            {
                //kecil besar
                $t1 = 'sedang';
            } else if ($tarif1['tarif'] > $tarif2['tarif'] && $tarif1['tarif'] > $tarif3['tarif']) 
            {
                //besar besar
                $t1 = 'mahal';
            } else if ($tarif1['tarif'] == $tarif2['tarif'] && $tarif1['tarif'] > $tarif3['tarif']) 
            {
                //sama besar
                $t1 = 'sedang';
            } else if ($tarif1['tarif'] == $tarif2['tarif'] && $tarif1['tarif'] < $tarif3['tarif'])
            {
                //sama kecil
                $t1 = 'murah';
            } else if ($tarif1['tarif'] > $tarif2['tarif'] && $tarif1['tarif'] == $tarif3['tarif'])
            {
                //besar sama
                $t1 = 'sedang';
            } else if ($tarif1['tarif'] < $tarif2['tarif'] && $tarif1['tarif'] == $tarif3['tarif'])
            {
                //kecil sama
                $t1 = 'murah';
            } else if ($tarif1['tarif'] == $tarif2['tarif'] && $tarif1['tarif'] == $tarif3['tarif'])
            {
                //sama sama
                $t1 = 'murah';
            } else {
                $t1 = 'sedang';
            }

            //TARIF 2
            if ($tarif2['tarif'] < $tarif1['tarif'] && $tarif2['tarif'] < $tarif3['tarif'])
            {
                //kecil kecil
                $t2 = 'murah';
            } else if ($tarif2['tarif'] > $tarif1['tarif'] && $tarif2['tarif'] < $tarif3['tarif']) 
            {
                //besar kecil
                $t2 = 'sedang';
            } else if ($tarif2['tarif'] < $tarif1['tarif'] && $tarif2['tarif'] > $tarif3['tarif']) 
            {
                //kecil besar
                $t2 = 'sedang';
            } else if ($tarif2['tarif'] > $tarif1['tarif'] && $tarif2['tarif'] > $tarif3['tarif']) 
            {
                //besar besar
                $t2 = 'mahal';
            } else if ($tarif2['tarif'] == $tarif1['tarif'] && $tarif2['tarif'] > $tarif3['tarif']) 
            {
                $t2 = 'sedang';
            } else if ($tarif2['tarif'] == $tarif1['tarif'] && $tarif2['tarif'] < $tarif3['tarif'])
            {
                $t2 = 'murah';
            } else if ($tarif2['tarif'] > $tarif1['tarif'] && $tarif2['tarif'] == $tarif3['tarif'])
            {
                //besar sama
                $t2 = 'sedang';
            } else if ($tarif2['tarif'] < $tarif1['tarif'] && $tarif2['tarif'] == $tarif3['tarif'])
            {
                //kecil sama
                $t2 = 'murah';
            } else if ($tarif2['tarif'] == $tarif1['tarif'] && $tarif2['tarif'] == $tarif3['tarif'])
            {
                //sama sama
                $t2 = 'murah';
            } else {
                $t2 = 'sedang';
            }
            
            //TARIF 3
            if ($tarif3['tarif'] < $tarif1['tarif'] && $tarif3['tarif'] < $tarif2['tarif'])
            {
                //kecil kecil
                $t3 = 'murah';
            } else if ($tarif3['tarif'] > $tarif1['tarif'] && $tarif3['tarif'] < $tarif2['tarif']) 
            {
                //besar kecil
                $t3 = 'sedang';
            } else if ($tarif3['tarif'] < $tarif1['tarif'] && $tarif3['tarif'] > $tarif2['tarif']) 
            {
                //kecil besar
                $t3 = 'sedang';
            } else if ($tarif3['tarif'] > $tarif1['tarif'] && $tarif3['tarif'] > $tarif2['tarif']) 
            {
                //besar besar
                $t3 = 'mahal';
            } else if ($tarif3['tarif'] == $tarif1['tarif'] && $tarif3['tarif'] > $tarif2['tarif']) 
            {
                //sama besar
                $t3 = 'sedang';
            } else if ($tarif3['tarif'] == $tarif1['tarif'] && $tarif3['tarif'] < $tarif2['tarif'])
            {
                //sama kecil
                $t3 = 'murah';
            } else if ($tarif3['tarif'] > $tarif1['tarif'] && $tarif3['tarif'] == $tarif2['tarif'])
            {
                //besar sama
                $t3 = 'sedang';
            } else if ($tarif3['tarif'] < $tarif1['tarif'] && $tarif3['tarif'] == $tarif2['tarif'])
            {
                //kecil sama
                $t3 = 'murah';
            } else if ($tarif3['tarif'] == $tarif1['tarif'] && $tarif3['tarif'] == $tarif2['tarif'])
            {
                //sama sama
                $t3 = 'murah';
            } else {
                $t3 = 'sedang';
            }

            //CATATAN HITAM
            $ch1    =   DB::table('report_mitra')
                        ->where('id_mitra', $request['kelompok']['calon_mitra1'])
                        ->count();

            $ch2    =   DB::table('report_mitra')
                        ->where('id_mitra', $request['kelompok']['calon_mitra2'])
                        ->count();

            $ch3    =   DB::table('report_mitra')
                        ->where('id_mitra', $request['kelompok']['calon_mitra3'])
                        ->count();
            
            $arrayDurasiHari            = [$durasi1['durasi'], $durasi2['durasi'], $durasi3['durasi']];
            $arrayTarifHarga            = [$tarif1['tarif'], $tarif2['tarif'], $tarif3['tarif']];
            $arrayCatatanHitamJumlah    = [$ch1, $ch2, $ch3];
            

            //MENENTUKAN NILAI
            if ($d1 == 'cepat')
            {
                $nilaiDurasi1   = 100;
            } 
            else if ($d1 == 'sedang')
            {
                $nilaiDurasi1   = 60;

            }
            else
            {
                $nilaiDurasi1   = 20;

            }

            if ($d2 == 'cepat')
            {
                $nilaiDurasi2   = 100;
            } 
            else if ($d2 == 'sedang')
            {
                $nilaiDurasi2   = 60;
            }
            else
            {
                $nilaiDurasi2   = 20;
            }

            if ($d3 == 'cepat')
            {
                $nilaiDurasi3   = 100;
            } 
            else if ($d3 == 'sedang')
            {
                $nilaiDurasi3   = 60;
            }
            else
            {
                $nilaiDurasi3   = 20;
            }

            if ($t1 == 'murah')
            {
                $nilaiTarif1   = 100;
            } 
            else if ($t1 == 'sedang')
            {
                $nilaiTarif1   = 60;
            }
            else
            {
                $nilaiTarif1   = 20;
            }

            if ($t2 == 'murah')
            {
                $nilaiTarif2   = 100;
            } 
            else if ($t2 == 'sedang')
            {
                $nilaiTarif2   = 60;
            }
            else
            {
                $nilaiTarif2   = 20;
            }

            if ($t3 == 'murah')
            {
                $nilaiTarif3   = 100;
            } 
            else if ($t3 == 'sedang')
            {
                $nilaiTarif3   = 60;
            }
            else
            {
                $nilaiTarif3   = 20;
            }

            if ($ch1 == 0)
            {
                $nilaiCatatanHitam1   = 100;
            } 
            else if ($ch1 <= 3)
            {
                $nilaiCatatanHitam1   = 60;
            }
            else
            {
                $nilaiCatatanHitam1   = 20;
            }

            if ($ch2 == 0)
            {
                $nilaiCatatanHitam2   = 100;
            } 
            else if ($ch2 <= 3)
            {
                $nilaiCatatanHitam2   = 60;
            }
            else
            {
                $nilaiCatatanHitam2   = 20;
            }

            if ($ch3 == 0)
            {
                $nilaiCatatanHitam3   = 100;
            } 
            else if ($ch3 <= 3)
            {
                $nilaiCatatanHitam3   = 60;
            }
            else
            {
                $nilaiCatatanHitam3   = 20;
            }

            $arrayNilaiDurasi           = [$nilaiDurasi1, $nilaiDurasi2, $nilaiDurasi3];
            $arrayNilaiTarif            = [$nilaiTarif1, $nilaiTarif2, $nilaiTarif3];
            $arrayNilaiCatatanHitam     = [$nilaiCatatanHitam1, $nilaiCatatanHitam2, $nilaiCatatanHitam3];

            $minDurasi      = min($arrayNilaiDurasi);
            $minTarif       = min($arrayNilaiTarif);
            $minCatatanHitam= min($arrayNilaiCatatanHitam);

            $maxDurasi      = max($arrayNilaiDurasi);
            $maxTarif       = max($arrayNilaiTarif);
            $maxCatatanHitam= max($arrayNilaiCatatanHitam);

            $FieldAtasUtilityDurasi1        = $nilaiDurasi1 - $minDurasi;
            $FieldBawahUtilityDurasi1       = $maxDurasi - $minDurasi;
            $divUtilityDurasi1              = $FieldAtasUtilityDurasi1 / $FieldBawahUtilityDurasi1;
            $utilityDurasi1                 = 100 * $divUtilityDurasi1;

            $FieldAtasUtilityDurasi2        = $nilaiDurasi2 - $minDurasi;
            $FieldBawahUtilityDurasi2       = $maxDurasi - $minDurasi;
            $divUtilityDurasi2              = $FieldAtasUtilityDurasi2 / $FieldBawahUtilityDurasi2;
            $utilityDurasi2                 = 100 * $divUtilityDurasi2;

            $FieldAtasUtilityDurasi3        = $nilaiDurasi3 - $minDurasi;
            $FieldBawahUtilityDurasi3       = $maxDurasi - $minDurasi;
            $divUtilityDurasi3              = $FieldAtasUtilityDurasi3 / $FieldBawahUtilityDurasi3;
            $utilityDurasi3                 = 100 * $divUtilityDurasi3;

            $FieldAtasUtilityTarif1        = $nilaiTarif1 - $minTarif;
            $FieldBawahUtilityTarif1       = $maxTarif - $minTarif;
            $divUtilityTarif1              = $FieldAtasUtilityTarif1 / $FieldBawahUtilityTarif1;
            $utilityTarif1                 = 100 * $divUtilityTarif1;

            $FieldAtasUtilityTarif2        = $nilaiTarif2 - $minTarif;
            $FieldBawahUtilityTarif2       = $maxTarif - $minTarif;
            $divUtilityTarif2              = $FieldAtasUtilityTarif2 / $FieldBawahUtilityTarif2;
            $utilityTarif2                 = 100 * $divUtilityTarif2;

            $FieldAtasUtilityTarif3        = $nilaiTarif3 - $minTarif;
            $FieldBawahUtilityTarif3       = $maxTarif - $minTarif;
            $divUtilityTarif3              = $FieldAtasUtilityTarif3 / $FieldBawahUtilityTarif3;
            $utilityTarif3                 = 100 * $divUtilityTarif3;

            $FieldAtasUtilityCatatanHitam1        = $nilaiCatatanHitam1 - $minCatatanHitam;
            $FieldBawahUtilityCatatanHitam1       = $maxCatatanHitam - $minCatatanHitam;
            $divUtilityCatatanHitam1              = $FieldAtasUtilityCatatanHitam1 / $FieldBawahUtilityCatatanHitam1;
            $utilityCatatanHitam1                 = 100 * $divUtilityCatatanHitam1;

            $FieldAtasUtilityCatatanHitam2        = $nilaiCatatanHitam2 - $minCatatanHitam;
            $FieldBawahUtilityCatatanHitam2       = $maxCatatanHitam - $minCatatanHitam;
            $divUtilityCatatanHitam2              = $FieldAtasUtilityCatatanHitam2 / $FieldBawahUtilityCatatanHitam2;
            $utilityCatatanHitam2                 = 100 * $divUtilityCatatanHitam2;

            $FieldAtasUtilityCatatanHitam3        = $nilaiCatatanHitam3 - $minCatatanHitam;
            $FieldBawahUtilityCatatanHitam3       = $maxCatatanHitam - $minCatatanHitam;
            $divUtilityCatatanHitam3              = $FieldAtasUtilityCatatanHitam3 / $FieldBawahUtilityCatatanHitam3;
            $utilityCatatanHitam3                 = 100 * $divUtilityCatatanHitam3;

            $bobotDurasi                = $request['durasi'] / 100;
            $bobotTarif                 = $request['tarif'] / 100;
            $bobotCatatanHitam          = $request['catatan_hitam'] / 100;

            $utilityXnormalisasiDurasi1 = $utilityDurasi1 * $bobotDurasi;
            $utilityXnormalisasiDurasi2 = $utilityDurasi2 * $bobotDurasi;
            $utilityXnormalisasiDurasi3 = $utilityDurasi3 * $bobotDurasi;
            
            $utilityXnormalisasiTarif1 = $utilityTarif1 * $bobotTarif;
            $utilityXnormalisasiTarif2 = $utilityTarif2 * $bobotTarif;
            $utilityXnormalisasiTarif3 = $utilityTarif3 * $bobotTarif;
            
            $utilityXnormalisasiCatatanHitam1 = $utilityCatatanHitam1 * $bobotCatatanHitam;
            $utilityXnormalisasiCatatanHitam2 = $utilityCatatanHitam2 * $bobotCatatanHitam;
            $utilityXnormalisasiCatatanHitam3 = $utilityCatatanHitam3 * $bobotCatatanHitam;

            $totalNilai1    = $utilityXnormalisasiDurasi1 + $utilityXnormalisasiTarif1 + $utilityXnormalisasiCatatanHitam1;
            $totalNilai2    = $utilityXnormalisasiDurasi2 + $utilityXnormalisasiTarif2 + $utilityXnormalisasiCatatanHitam2;
            $totalNilai3    = $utilityXnormalisasiDurasi3 + $utilityXnormalisasiTarif3 + $utilityXnormalisasiCatatanHitam3;

            $allArrayNilaiKriteria  =   [
                'durasi'        => $arrayNilaiDurasi,
                'tarif'         => $arrayNilaiTarif,
                'catatan_hitam' => $arrayNilaiCatatanHitam
            ];

            $arrayUtilityDurasi         = [$utilityDurasi1, $utilityDurasi2, $utilityDurasi3];
            $arrayUtilityTarif          = [$utilityTarif1, $utilityTarif2, $utilityTarif3];
            $arrayUtilityCatatanHitam   = [$utilityCatatanHitam1, $utilityCatatanHitam2, $utilityCatatanHitam3];

            $arrayBobot                 = [$bobotDurasi, $bobotTarif, $bobotCatatanHitam];

            $arrayUtilityXNormalisasiDurasi         = [$utilityXnormalisasiDurasi1, $utilityXnormalisasiDurasi2, $utilityXnormalisasiDurasi3];
            $arrayUtilityXNormalisasiTarif          = [$utilityXnormalisasiTarif1, $utilityXnormalisasiTarif2, $utilityXnormalisasiTarif3];
            $arrayUtilityXNormalisasiCatatanHitam   = [$utilityXnormalisasiCatatanHitam1, $utilityXnormalisasiCatatanHitam2, $utilityXnormalisasiCatatanHitam3];

            $arrayTotalNilai  = [
                [
                'id_mitra'  => $bio1->nama_lengkap,
                'id_tarif'  => $tarif1['tarif'],
                'total'     => $totalNilai1
                ],
                [
                'id_mitra'  => $bio2->nama_lengkap,
                'id_tarif'  => $tarif2['tarif'],
                'total'     => $totalNilai2
                ],
                [
                'id_mitra'  => $bio3->nama_lengkap,
                'id_tarif'  => $tarif3['tarif'],
                'total'     => $totalNilai3
                ],
            ];
            usort($arrayTotalNilai, function($a, $b) {
                return $a['total'] <= $b['total'];
            });
            
            $finalResponse  =   [
                // 'tableMitra'                            => [$request['kelompok']['calon_mitra1'], $request['kelompok']['calon_mitra2'], $request['kelompok']['calon_mitra3']],
                'tableMitra'                            => [$bio1->nama_lengkap, $bio2->nama_lengkap, $bio3->nama_lengkap],
                'tableBobot'                            => $arrayBobot,
                'tableNilaiDurasi'                      => $arrayNilaiDurasi,
                'tableDurasi'                           => $arrayDurasiHari,
                'tableNilaiTarif'                       => $arrayNilaiTarif,
                'tableTarif'                            => $arrayTarifHarga,
                'tableNilaiCatatanHitam'                => $arrayNilaiCatatanHitam,
                'tableCatatanHitam'                     => $arrayCatatanHitamJumlah,
                'tableUtilityDurasi'                    => $arrayUtilityDurasi,
                'tableUtilityTarif'                     => $arrayUtilityTarif,
                'tableUtilityCatatanHitam'              => $arrayUtilityCatatanHitam,
                'tableUtilityXNormalisasiDurasi'        => $arrayUtilityXNormalisasiDurasi,
                'tableUtilityXNormalisasiTarif'         => $arrayUtilityXNormalisasiTarif,
                'tableUtilityXNormalisasiCatatanHitam'  => $arrayUtilityXNormalisasiCatatanHitam,
                'tableTotalNilai'                       => $arrayTotalNilai
            ];

            return response()->json($finalResponse, 200);

        }
        
    }

}
