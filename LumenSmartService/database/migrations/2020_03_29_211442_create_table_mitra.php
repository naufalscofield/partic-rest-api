<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('jenis_kendaraan');
            $table->string('nama_kendaraan');
            $table->string('plat_kendaraan');
            $table->integer('tahun_kendaraan');
            $table->string('domisili');
            $table->string('foto_stnk');
            $table->string('foto_sim');
            $table->string('jenis_mitra');
            $table->integer('maksimal_berat');
            $table->integer('verifikasi')->default(2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitra');
    }
}
