<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrometheeController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function promethee(Request $request)
    {
        $dataPengiriman  = $request->dataPengiriman;
        $dataMitra      = $request->dataMitra;
        $dataReport    = $request->dataReport;
        $alias = 'A';

        //TABEL PENILAIAN//
        $tabelPenilaian = [];
        foreach ($dataMitra as $mitra)
        {
            //Kriteria 1 Kesusaian Jenis Pengiriman
            if ($mitra['tarif']['jenis_pengiriman'] == $dataPengiriman['jenis_pengiriman']) 
            {
                $k1 = 2;
            } else {
                $k1 = 1;
            }
            
            //Kriteria 2 Kesesuaian Jenis Pengangkutan
            if ($mitra['tarif']['jenis_pengangkutan'] == $dataPengiriman['jenis_pengangkutan']) 
            {
                $k2 = 2;
            } else {
                $k2 = 1;
            }

            //Kriteria 3 Ketersediaan Mitra
            if ($mitra['mitra']['status_ketersediaan'] == 'Available') 
            {
                $k3 = 1;
            } else {
                $k3 = 0;
            }

            //Kriteria 4 Memenuhi Minimal dan Maksimal Daya Angkut Mitra
                //Mengecek Apakah Mitra Sudah Memuat Barang / Belum Sama Sekali

            if ($mitra['lobi'] == 0) 
            {
                if($dataPengiriman['jenis_pengangkutan'] == 'kilo')
                {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'kilo')
                    {
                        if( ($dataPengiriman['berat_total'] >= $mitra['tarif']['minimal_berat']) && ($dataPengiriman['berat_total'] <= $mitra['tarif']['maksimal_berat']) )
                        {
                            $k4 = 2;   
                        } else {
                            $k4 = 0;   
                        }
                    } else {
                        $k4 = 0;
                    }

                } else if($dataPengiriman['jenis_pengangkutan'] == 'koli')
                {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'koli')
                    {
                        if($dataPengiriman['nama_barang'] == $mitra['tarif']['nama_barang'])
                        {
                            if( ($dataPengiriman['unit_total'] >= $mitra['tarif']['minimal_jumlah']) && ($dataPengiriman['unit_total'] <= $mitra['tarif']['maksimal_jumlah']) )
                            {
                                $k4 = 2;
                            } else {
                                $k4 = 0;
                            }
                        }
                        else {
                            $k4 = 0;
                        }
                    } else {
                        $k4 = 0;
                    }
                } else {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'dimensional')
                    {
                        if( ($dataPengiriman['dimensi_total'] >= $mitra['tarif']['minimal_dimensi']) && ($dataPengiriman['dimensi_total'] <= $mitra['tarif']['maksimal_dimensi']) )
                        {
                            $k4 = 2;
                        } else {
                            $k4 = 0;
                        }

                    } else {
                        $k4 = 0;
                    }
                }
            } else {
                if ($dataPengiriman['jenis_pengangkutan'] == 'kilo')
                {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'kilo')
                    {
                        $beratSisa = $mitra['tarif']['maksimal_berat'] - $mitra['berat_total_lobi'];
                
                        if ($dataPengiriman['berat_total'] <= $beratSisa)
                        {
                            $k4 = 3;
                        } else {
                            $k4 = 0;
                        }

                    } else {
                        $k4 = 0;
                    }
                } else if ($dataPengiriman['jenis_pengangkutan'] == 'koli')
                {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'koli')
                    {
                        if($dataPengiriman['nama_barang'] == $mitra['tarif']['nama_barang'])
                        {
                            $unitSisa = $mitra['tarif']['maksimal_jumlah'] - $mitra['unit_total_lobi'];
    
                            if($dataPengiriman['unit_total'] <= $unitSisa)
                            {
                                $k4 = 3;
                            } else {
                                $k4 = 0;
                            }
                        }
                        else {
                            $k4 = 0;
                        }
                    } else {
                        $k4 = 0;
                    }
                } else 
                {
                    if($mitra['tarif']['jenis_pengangkutan'] == 'dimensional')
                    {
                        $dimensiSisa = $mitra['tarif']['maksimal_dimensi'] - $mitra['dimensi_total_lobi'];
                        if($dataPengiriman['dimensi_total'] <= $dimensiSisa)
                        {
                            $k4 = 3;
                        } else {
                            $k4 = 0;
                        }
                    } else {
                        $k4 = 0;
                    }
                }
            }

            //Kriteria 5 Pengecekan Catatan Masalah
            $totalReport = 0;
            if ($dataReport['total'] != 0)
            {
                foreach ($dataReport['report'] as $report) {
                    if ($report['id_mitra'] == $mitra['mitra']['id']) {
                        $totalReport+=1;
                    }
                }
            }

            if ($totalReport == 0){
                $k5 = 3;
            } else if($totalReport <= 3) {
                $k5 = 2;
            } else {
                $k5 = 1;
            }

            $dataPenilaian = [
                'id_mitra'  => $mitra['mitra']['id'],
                'id_tarif'  => $mitra['tarif']['id'],
                'alias'     => $mitra['biodata']['nama_lengkap'],
                'K1'        => $k1,
                'K2'        => $k2,
                'K3'        => $k3,
                'K4'        => $k4,
                'Jumlah'    => $k1+$k2+$k3+$k4
            ];
            
            $alias++;
            
            array_push($tabelPenilaian, $dataPenilaian);
    }

        //MENGHITUNG NILAI PREFERENSI & INDEKS PREFERENSI MULTIKRITERIA//
        $tabelPreferensi = [];

        //foreach Index ke 1
        foreach($tabelPenilaian as $index1)
        {
            foreach ($tabelPenilaian as $index2) 
            {
                if ($index2['alias'] != $index1['alias'])
                {
                    for ($i = 1; $i <= 4; $i++)
                    {
                        if ( ($index1['K'.$i] == $index2['K'.$i]) || ($index1['K'.$i] < $index2['K'.$i]) )
                        {
                            $pref[$i] = 0 ;
                        } else {
                            $pref[$i] = 1;
                        }

                    }

                    $jumlahNilaiPreferensi = $pref[1] + $pref[2] + $pref[3] + $pref[4];

                    $totalNilaiPreferensi = round($jumlahNilaiPreferensi/5, 5);
                    $dataPreferensi = [
                        'index1'            => $index1['alias'],
                        'id_mitra1'         => $index1['id_mitra'],
                        'id_tarif1'         => $index1['id_tarif'],
                        'index2'            => $index2['alias'],
                        'id_mitra2'         => $index2['id_mitra'],
                        'id_tarif2'         => $index2['id_tarif'],
                        'nilai_pref_1'      => $pref[1],
                        'nilai_pref_2'      => $pref[2],
                        'nilai_pref_3'      => $pref[3],
                        'nilai_pref_4'      => $pref[4],
                        'Jumlah'            => $jumlahNilaiPreferensi,
                        'Total'             => $totalNilaiPreferensi
                    ];
                    array_push($tabelPreferensi, $dataPreferensi);
                }
            }
        }

        //MENGHITUNG JUMLAH NILAI PREFERENSI TIAP ALTERNATIF

        $tabelIndexPreferensiAlternatif = [];

        foreach ($tabelPreferensi as $idxPref)
        {
            $totalNilaiPrefAlternatifH = 0;
            $totalNilaiPrefAlternatifV = 0;

            foreach ($tabelPreferensi as $prefH)
            {
                if ($prefH['index1'] == $idxPref['index1'])
                {
                    $totalNilaiPrefAlternatifH += $prefH['Total'];
                }
            }

            foreach ($tabelPreferensi as $prefV)
            {
                if ($prefV['index2'] == $idxPref['index1'])
                {
                    $totalNilaiPrefAlternatifV += $prefV['Total'];
                }
            }

            $dataNilaiPreferensiAlternatif = [
                'alternatif'    => $idxPref['index1'],
                'id_mitra'      => $idxPref['id_mitra1'],
                'id_tarif'      => $idxPref['id_tarif1'],
                'total_nilai_h' => round($totalNilaiPrefAlternatifH, 5),
                'total_nilai_v' => round($totalNilaiPrefAlternatifV, 5)
            ];

            array_push($tabelIndexPreferensiAlternatif, $dataNilaiPreferensiAlternatif );
        }

        $tabelFinalIndexPreferensiAlternatif = array_unique($tabelIndexPreferensiAlternatif, SORT_REGULAR);
        $jumlahAlternatif = count($tabelFinalIndexPreferensiAlternatif) - 1;

        // MENGHITUNG LEAVING FLOW, ENTERING FLOW & NET FLOW TIAP ALTERNATIF
        $tabelFlows = [];

        foreach ($tabelFinalIndexPreferensiAlternatif as $idx)
        {
            $leavingFlow    = 1/$jumlahAlternatif*$idx['total_nilai_h'];
            $enteringFlow   = 1/$jumlahAlternatif*$idx['total_nilai_v'];
            $netFlow        = round($leavingFlow - $enteringFlow, 5);

            $dataFlows = [
                'alternatif'    => $idx['alternatif'],
                'id_mitra'      => $idx['id_mitra'],
                'id_tarif'      => $idx['id_tarif'],
                'leaving_flow'  => $leavingFlow,
                'entering_flow' => $enteringFlow,
                'net_flow'      => $netFlow,
            ];

            array_push($tabelFlows, $dataFlows);
        }

        usort($tabelFlows, function($a, $b) {
            return $a['net_flow'] <= $b['net_flow'];
        });

        $finalResponse = [
            'tabel_penilaian'       => $tabelPenilaian,
            'tabel_preferensi'      => $tabelPreferensi,
            'tabel_index_preferensi'=> $tabelFinalIndexPreferensiAlternatif,
            'tabel_flows'           => $tabelFlows
        ];
        return response()->json($finalResponse, 200);
    }
}
