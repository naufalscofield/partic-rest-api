<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\KelompokPengiriman;
use App\Models\LobiMitra;
use App\Models\Pengiriman;
use DB;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class KelompokPengirimanController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
    * Return full list of authors
    * @return Response
    */

    public function get($idRegional)
    {
        $kelompok = DB::table('kelompok_pengiriman as a')
        ->leftJoin('tarif as b', 'b.id', 'a.id_tarif_line')
        ->leftJoin('mitra as c', 'c.id', 'a.id_mitra')
        ->leftJoin('biodata as d', 'd.id_user', 'c.id_user')
        ->select('a.*', 'a.id as id_kelompok_pengiriman','a.tujuan as tujuan_pengiriman', 'a.asal as asal_tujuan', 'a.jenis_pengangkutan as jenis_angkut', 'a.jenis_pengiriman as jenis_kirim', 'b.*', 'b.tujuan as tujuan_tarif', 'c.*', 'd.*')
        ->get();

        return response()->json($kelompok, 200);
    }

    public function show($idKelompokPengiriman)
    {
        $kelompok   = KelompokPengiriman::find($idKelompokPengiriman);
        
        // $kelompok   = DB::table('kelompok_pengiriman')
        //                 ->find($idKelompokPengiriman);
        
        // $kelompok   = DB::table('kelompok_pengiriman')
        //                 ->where('id', $idKelompokPengiriman)
        //                 ->first();
        
        // $kelompok   = DB::table('kelompok_pengiriman')
        //                 ->where('id', $idKelompokPengiriman)
        //                 ->get();
            
        // $kelompok   = KelompokPengiriman::where('id', $idKelompokPengiriman)->get();

        return response($kelompok, 200);
    }

    public function getFieldKelompokPengiriman($kota, $id_perusahaan)
    {
        $kotaAsal = str_replace('-', ' ', $kota);

        $kelompok = DB::table('pengiriman')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->where('id_kelompok_pengiriman', 0)
                        ->distinct()
                        ->select('kota_tujuan')
                        ->get();

        return response()->json($kelompok, 200);
    }

    public function getField2KelompokPengiriman($tujuan, $kota, $id_perusahaan)
    {
        $kotaAsal = str_replace('-', ' ', $kota);
        $kotaTujuan = str_replace('-', ' ', $tujuan);

        $kelompok = DB::table('pengiriman')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->where('kota_tujuan', $kotaTujuan)
                        ->where('id_kelompok_pengiriman', 0)
                        ->distinct()
                        ->select('jenis_pengangkutan')
                        ->get();

        return response()->json($kelompok, 200);
    }

    public function getField3KelompokPengiriman($tujuan, $jenis_pengangkutan, $kota, $id_perusahaan)
    {
        $kotaAsal = str_replace('-', ' ', $kota);
        $kotaTujuan = str_replace('-', ' ', $tujuan);

        $kelompok = DB::table('pengiriman')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->where('kota_tujuan', $kotaTujuan)
                        ->where('jenis_pengangkutan', $jenis_pengangkutan)
                        ->where('id_kelompok_pengiriman', 0)
                        ->distinct()
                        ->select('jenis_pengiriman')
                        ->get();

        return response()->json($kelompok, 200);
    }

    public function getField4KelompokPengiriman($tujuan, $jenis_pengangkutan, $jenis_pengiriman, $kota, $id_perusahaan)
    {
        $kotaAsal = str_replace('-', ' ', $kota);
        $kotaTujuan = str_replace('-', ' ', $tujuan);

        $kelompok   = DB::table('pengiriman')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->where('kota_tujuan', $kotaTujuan)
                        ->where('jenis_pengangkutan', $jenis_pengangkutan)
                        ->where('jenis_pengiriman', $jenis_pengiriman)
                        ->where('id_kelompok_pengiriman', 0)
                        ->get();

        $total      = DB::table('pengiriman')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->where('kota_tujuan', $kotaTujuan)
                        ->where('jenis_pengangkutan', $jenis_pengangkutan)
                        ->where('jenis_pengiriman', $jenis_pengiriman)
                        ->where('id_kelompok_pengiriman', 0)
                        ->count();

        if ($jenis_pengangkutan == 'kilo')
        {
            $berat_total = 0;
            $dimensi_total = 0;
            $unit_total = 0;
            $satuan = 'Kilo';
            foreach($kelompok as $kel)
            {
                $berat_total += $kel->berat;
            }
        }
        if ($jenis_pengangkutan == 'koli')
        {
            $berat_total = 0;
            $dimensi_total = 0;
            $unit_total = 0;
            $satuan = 'Unit';
            foreach($kelompok as $kel)
            {
                $unit_total += $kel->total_unit;
            }
        }
        if ($jenis_pengangkutan == 'dimensional')
        {
            $berat_total = 0;
            $dimensi_total = 0;
            $unit_total = 0;
            $satuan = 'Meter Persegi';
            foreach($kelompok as $kel)
            {
                $dimensi_total += $kel->dimensi;
            }
        }

        $response = [
            'total_pengiriman'  => $total,
            'berat_total'       => $berat_total,
            'dimensi_total'     => $dimensi_total,
            'unit_total'        => $unit_total,
            'satuan'            => $satuan
        ];

        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        // return response()->json($request->all(),200);
        $kelompok = new KelompokPengiriman;
        
        $kelompok->id_perusahaan        = $request->id_perusahaan;
        $kelompok->id_regional          = $request->id_regional;
        $kelompok->asal                 = $request->asal;
        $kelompok->tujuan               = $request->tujuan;
        $kelompok->jenis_pengangkutan   = $request->jenis_pengangkutan;
        $kelompok->jenis_pengiriman     = $request->jenis_pengiriman;
        $kelompok->berat_total          = $request->berat_total;
        $kelompok->dimensi_total        = $request->dimensi_total;
        $kelompok->unit_total           = $request->unit_total;
        
        $kelompok->save();

        $kelompok_id    = $kelompok->id;
        
        $pengiriman     = DB::table('pengiriman')
                                ->where('id_perusahaan', $kelompok->id_perusahaan)
                                ->where('kota_asal', $kelompok->asal)
                                ->where('kota_tujuan', $kelompok->tujuan)
                                ->where('jenis_pengangkutan', $kelompok->jenis_pengangkutan)
                                ->where('jenis_pengiriman', $kelompok->jenis_pengiriman)
                                ->update(['id_kelompok_pengiriman' => $kelompok_id]);


        return response()->json($kelompok,200);
    }

    public function update(Request $request)
    {
        $kelompok                   = KelompokPengiriman::find($request->id_kelompok_pengiriman);

        $kelompok->id_mitra         = $request->calon_mitra1;
        $kelompok->calon_mitra1     = $request->calon_mitra1;
        $kelompok->calon_mitra2     = $request->calon_mitra2;
        $kelompok->calon_mitra3     = $request->calon_mitra3;
        $kelompok->id_tarif_line    = $request->id_tarif_line1;
        $kelompok->id_tarif_line1   = $request->id_tarif_line1;
        $kelompok->id_tarif_line2   = $request->id_tarif_line2;
        $kelompok->id_tarif_line3   = $request->id_tarif_line3;

        if ($kelompok->save())
        {
            $resp = [
                'status'    => 200,
                'data'      => $kelompok
            ];
        } else {
            $resp = [
                'status'    => 201,
                'data'      => $kelompok
            ];
        }

        return response()->json($resp);
    }

    public function updateFinal(Request $request)
    {
        $KelompokPengiriman         = KelompokPengiriman::find($request->id_kelompok_pengiriman);
        $berat_total                = $KelompokPengiriman->berat_total;
        $dimensi_total              = $KelompokPengiriman->dimensi_total;
        $unit_total                 = $KelompokPengiriman->unit_total;

        $lobi                       = new LobiMitra;
        $lobi->id_perusahaan        = $request->id_perusahaan;
        $lobi->id_kelompok_pengiriman  = $request->id_kelompok_pengiriman;
        $lobi->id_mitra             = $request->id_mitra;
        $lobi->id_tarif             = $request->id_tarif_line;
        $lobi->berat_total          = $berat_total;
        $lobi->dimensi_total        = $dimensi_total;
        $lobi->unit_total           = $unit_total;
        $lobi->save();

        $kelompok                   = KelompokPengiriman::find($request->id_kelompok_pengiriman);
        $kelompok->id_mitra         = $request->id_mitra;
        $kelompok->calon_mitra1     = $request->calon_mitra1;
        $kelompok->calon_mitra2     = $request->calon_mitra2;
        $kelompok->calon_mitra3     = $request->calon_mitra3;
        $kelompok->id_tarif_line    = $request->id_tarif_line;
        $kelompok->id_tarif_line1   = $request->id_tarif_line1;
        $kelompok->id_tarif_line2   = $request->id_tarif_line2;
        $kelompok->id_tarif_line3   = $request->id_tarif_line3;

        if ($kelompok->save())
        {
            $resp = [
                'status'    => 200,
                'data'      => $kelompok
            ];
        } else {
            $resp = [
                'status'    => 201,
                'data'      => $kelompok
            ];
        }

        return response()->json($resp);
    }
}

