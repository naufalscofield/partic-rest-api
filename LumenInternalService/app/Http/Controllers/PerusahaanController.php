<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Exceptions\Handler;
use App\Models\Perusahaan;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
Use Symfony\Component\HttpFoundation\File\UploadedFile;


class PerusahaanController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function uploadLogo(Request $request)
    {
        return response('aw');
        $logo          = $request->file('file_logo');
        $logo_name     = $request->logo;
        $path_logo     = storage_path('/app/images/logo_perusahaan');
        $logo->move($path_logo, $logo_name);
    }

    public function get()
    {
        $perusahaan = Perusahaan::All();

        return response($perusahaan, 200);
    }

    public function store(Request $request)
    {        
        $perusahaan = new Perusahaan;

        $perusahaan->nama_perusahaan = $request->nama_perusahaan;
        $perusahaan->alamat = $request->alamat;
        $perusahaan->email = $request->email;
        $perusahaan->no_telp = $request->no_telp;
        $perusahaan->logo = $request->logo;

        $perusahaan->save();

        return response()->json($perusahaan,200);
    }

    public function show($idPerusahaan)
    {
        $perusahaan = Perusahaan::find($idPerusahaan);
        
        return response()->json($perusahaan,200);
    }

    public function update(Request $request, $id)
    {
        $update = Perusahaan::find($id);
        $update->nama_perusahaan   = $request->nama_perusahaan;
        $update->alamat            = $request->alamat;
        $update->kota              = $request->kota;
        $update->logo              = $request->logo;
        $update->save();
        
        return response()->json($update,200);
    }

}
