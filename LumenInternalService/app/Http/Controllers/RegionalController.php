<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Regional;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RegionalController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
    * Return full list of authors
    * @return Response
    */
    public function get($idperusahaan)
    {
        $getRegional = Regional::where('id_perusahaan', $idperusahaan)->get();
        return response()->json($getRegional);
    }


    /**
     * Create one new authors
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {        
        $regional = new Regional;

        $regional->id_perusahaan = $request->id_perusahaan;
        $regional->provinsi = $request->provinsi;
        $regional->kota = $request->kota;
        $regional->alamat = $request->alamat;
        $regional->no_telp = $request->no_telp;
        $regional->email = $request->email;

        $regional->save();

        return response()->json($regional,200);
    }


    /**
     * Show a specific author
     * @param Author $author
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $showRegional = Regional::find($id);
        return response()->json($showRegional,200);
    }


    /**
     * Update author information
     * @param Request $request
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        $update = Regional::find($id);
        $update->alamat = $request->alamat;
        $update->no_telp = $request->no_telp;
        $update->email = $request->email;
        $update->save();
        return response()->json($update,200);
    }


    /**
     * Delete author information
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = Regional::find($id);
        $delete->delete();
        return $this->successResponse($delete,200);
    }

}
