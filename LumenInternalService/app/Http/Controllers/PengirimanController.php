<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Tarif;
use App\Models\Pengiriman;
use App\Models\StatusPengiriman;
use DB;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PengirimanController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
    * Return full list of authors
    * @return Response
    */

    public function getById($id)
    {
        $pengiriman = DB::table('pengiriman AS a')
                        ->join('cabang AS d','d.id','a.id_cabang')
                        ->where('a.id', $id)
                        ->first();
        
        return response()->json($pengiriman);
        if ($pengiriman->id_tarif_line !== NULL)
        {
            $tarif = DB::table('tarif')
                    ->first();
            
            $mitra = DB::table('mitra AS a')
                    ->join('biodata AS b', 'b.id_user', 'a.id_user')
                    ->select('a.*', 'a.id as id_mitra', 'b.*', 'b.id as id_biodata')
                    ->first();

            
            $idTarifLine    = $tarif->id;
            $tarifLine      = $tarif->tarif_normal;
            $idKurir        = $mitra->id_mitra;
            $namaKurir      = $mitra->nama_lengkap;
            $noTelpKurir    = $mitra->no_telp;

        } else {
            $idTarifLine    = NULL;
            $tarifLine      = NULL;
            $idKurir        = NULL;
            $namaKurir      = NULL;
            $noTelpKurir    = NULL;
        }


        $status = DB::table('status_pengiriman')
                    ->where('id_pengiriman', $pengiriman->id)
                    ->get();

        if ($pengiriman->jenis_pengangkutan == 'kilo')
        {
            $ukuran = $pengiriman->berat.' Kilo';
        } else if ($pengiriman->jenis_pengangkutan == 'koli')
        {
            $ukuran = $pengiriman->total_unit.' Unit';
        } else {
            $ukuran = $pengiriman->dimensi.' Meter Persegi';
        }

        $resp = [
            'no_resi'                   => $pengiriman->no_resi,
            'jenis_pengangkutan'        => $pengiriman->jenis_pengangkutan,
            'deskripsi_barang'          => $pengiriman->jenis_barang.','.$pengiriman->nama_barang,
            'ukuran'                    => $ukuran,
            'jenis_pengiriman'          => $pengiriman->jenis_pengiriman,
            'asal'                      => $pengiriman->kota_asal,
            'tujuan'                    => $pengiriman->provinsi_tujuan.','.$pengiriman->kota_tujuan,
            'alamat_tujuan'             => $pengiriman->alamat_tujuan.' '.$pengiriman->kode_pos_tujuan.','.$pengiriman->kecamatan_tujuan.','.$pengiriman->kelurahan_tujuan,
            'tarif'                     => $pengiriman->tarif,
            'id_kelompok_pengiriman'    => $pengiriman->id_kelompok_pengiriman,
            'cabang'                    => $pengiriman->nama_cabang,
            'nama_pengirim'             => $pengiriman->nama_pengirim,
            'no_telp_pengirim'          => $pengiriman->no_telp_pengirim,
            'id_member'                 => $pengiriman->id_member,
            'nama_penerima'             => $pengiriman->nama_penerima,
            'no_telp_penerima'          => $pengiriman->no_telp_penerima,
            'id_mitra'                  => $idKurir,
            'nama_mitra'                => $namaKurir,
            'no_telp_mitra'             => $noTelpKurir,
            'id_tarif_line'             => $idTarifLine,
            'tarif_line'                => $tarifLine,
            'tanggal_masuk'             => $pengiriman->created_at,
            'tanggal_pengiriman'        => $pengiriman->tanggal_pengiriman,
            'perkiraan_tanggal_sampai'  => $pengiriman->perkiraan_tanggal_sampai,
            'tanggal_sampai'            => $pengiriman->tanggal_sampai,
            'jumlah_hari_telat'         => $pengiriman->jumlah_hari_telat,
            'keterangan'                => $pengiriman->keterangan,
            'status_pengiriman'         => $status
        ];

        return response()->json($resp, 200);
    }

    public function getByKota($idPerusahaan, $kota)
    {
        $kotaAsal = str_replace('-', ' ', $kota);
        // return response()->json($kota, 200);

        $pengiriman = DB::table('pengiriman')
                        ->where('id_perusahaan', $idPerusahaan)
                        ->where('kota_asal', $kotaAsal)
                        ->get();

        return response()->json($pengiriman, 200);
    }
  
    public function getByCabang($idCabang)
    {
        $pengiriman = DB::table('pengiriman AS a')
                        ->join('cabang AS b', 'b.id', 'a.id_cabang')
                        ->where('a.id_cabang', $idCabang)
                        ->get();

        return response()->json($pengiriman, 200);
    }

    public function getByPerusahaan($idPerusahaan)
    {
        $pengiriman = DB::table('pengiriman')
                        ->where('pengiriman.id_perusahaan', $idPerusahaan)
                        ->join('cabang','cabang.id','pengiriman.id_cabang')
                        ->get();

        return response()->json($pengiriman, 200);
    }
    

    public function verifikasi(Request $request, $id)
    {
        $pengiriman = Pengiriman::find($id);
        $pengiriman->verifikasi = $request->verifikasi;
        $pengiriman->keterangan = $request->keterangan;
        $pengiriman->save();

        $resp = [
            'status' => 200,
            'data' => $pengiriman,
        ];

        return response()->json($resp, 200);
    }

    public function inputPengirimanByMitra(Request $request)
    {
        $pengiriman = new Pengiriman;
        $pengiriman->no_resi = $request->no_resi;
        $pengiriman->jenis_pengangkutan = $request->jenis_pengangkutan;
        $pengiriman->jenis_barang = $request->jenis_barang;
        $pengiriman->nama_barang = $request->nama_barang;
        $pengiriman->berat = intval($request->berat);
        $pengiriman->dimensi = intval($request->dimensi);
        $pengiriman->total_unit = intval($request->total_unit);
        $pengiriman->jenis_pengiriman = $request->jenis_pengiriman;
        $pengiriman->kota_asal = $request->kota_asal;
        $pengiriman->provinsi_tujuan = $request->provinsi_tujuan;
        $pengiriman->kota_tujuan = $request->kota_tujuan;
        $pengiriman->kecamatan_tujuan = $request->kecamatan_tujuan;
        $pengiriman->kelurahan_tujuan = $request->kelurahan_tujuan;
        $pengiriman->kode_pos_tujuan = intval($request->kode_pos_tujuan);
        $pengiriman->alamat_tujuan = $request->alamat_tujuan;
        $pengiriman->nama_pengirim = $request->nama_pengirim;
        $pengiriman->nama_penerima = $request->nama_penerima;
        $pengiriman->no_telp_pengirim = $request->no_telp_pengirim;
        $pengiriman->no_telp_penerima = $request->no_telp_penerima;
        $pengiriman->id_kelompok_pengiriman = intval($request->id_kelompok_pengiriman);
        $pengiriman->latitude = $request->latitude;
        $pengiriman->longitude = $request->longitude;
        $pengiriman->save();
        return response()->json($pengiriman, 200);


    }

    public function store(Request $request)
    {
        if (isset($request->id_member))
        {
            $id_member = $request->id_member;
        } else {
            $id_member = NULL;
        }
        
        if (isset($request->berat))
        {
            $berat = $request->berat;
        } else {
            $berat = NULL;
        }

        if (isset($request->dimensi))
        {
            $dimensi = $request->dimensi;
        } else {
            $dimensi = NULL;
        }

        if (isset($request->total_unit))
        {
            $total_unit = $request->total_unit;
        } else {
            $total_unit = NULL;
        }

        // return response()->json($berat);

        $pengiriman = new Pengiriman;
        $pengiriman->no_resi = $request->no_resi;
        $pengiriman->pin = $request->pin;
        $pengiriman->jenis_pengangkutan = $request->jenis_pengangkutan;
        $pengiriman->jenis_barang = $request->jenis_barang;
        $pengiriman->nama_barang = $request->nama_barang;
        $pengiriman->berat = $berat;
        $pengiriman->dimensi = $dimensi;
        $pengiriman->total_unit = $total_unit;
        $pengiriman->jenis_pengiriman = $request->jenis_pengiriman;
        $pengiriman->kota_asal = $request->kota_asal;
        $pengiriman->provinsi_tujuan = $request->provinsi_tujuan;
        $pengiriman->kota_tujuan = $request->kota_tujuan;
        $pengiriman->kecamatan_tujuan = $request->kecamatan_tujuan;
        $pengiriman->kelurahan_tujuan = $request->kelurahan_tujuan;
        $pengiriman->kode_pos_tujuan = $request->kode_pos_tujuan;
        $pengiriman->alamat_tujuan = $request->alamat_tujuan;
        $pengiriman->id_member = $id_member;
        $pengiriman->nama_pengirim = $request->nama_pengirim;
        $pengiriman->nama_penerima = $request->nama_penerima;
        $pengiriman->no_telp_pengirim = $request->no_telp_pengirim;
        $pengiriman->no_telp_penerima = $request->no_telp_penerima;
        $pengiriman->tarif = $request->tarif;
        $pengiriman->verifikasi = $request->verifikasi;
        $pengiriman->keterangan = $request->keterangan;
        $pengiriman->id_perusahaan = $request->id_perusahaan;
        $pengiriman->id_cabang = $requecst->id_cabang;
        $pengiriman->perkiraan_tanggal_sampai = $request->perkiraan_tanggal_sampai;
        $pengiriman->latitude = $request->latitude;
        $pengiriman->longitude = $request->longitude;

        $pengiriman->save();

        $resp = [
            'status' => 200,
            'data' => $pengiriman
        ];

        return response()->json($pengiriman, 200);
    }

    public function update(Request $request, $id)
    {
        $pengiriman = Pengiriman::find($id);

        $currentStatus = $pengiriman->status_pengiriman;
        $newStatus = $currentStatus.'-'.$request->status_pengiriman;

        $pengiriman->status_pengiriman = $newStatus;

        $pengiriman->save();

        $resp = [
            'status' => 200,
            'data' => $pengiriman
        ];

        return response()->json($pengiriman, 200);
    }

    public function pickup(Request $request, $id)
    {
        // return response()->json($request['nama_mitra'], 200);
        $pengiriman = Pengiriman::find($id);

        $status = $pengiriman->status_pengiriman;
        $status_new = $status.'Sudah dipickup oleh kurir '.$request['nama_mitra'].' menggunakan kendaraan '.$request['mitra']['nama_kendaraan'].' dengan id mitra '.$request['mitra']['id'].' // ';

        $pengiriman->status_pengiriman = $status_new;

        $pengiriman->save();

        return response($pengiriman, 200);
    }

    public function inputStatusPengiriman(Request $request)
    {
        // return response()->json($reques->all());
        $statusPengiriman = new StatusPengiriman;

        $statusPengiriman->id_pengiriman = $request->id;
        $statusPengiriman->tanggal = date('Y-m-d h:i:s');
        $statusPengiriman->status = $request->status;

        $statusPengiriman->save();

        return response()->json($statusPengiriman, 200);
    }

    public function updateTanggalSampai($id)
    {
        // return response()->json($id);
        $pengiriman = Pengiriman::find($id);

        $pengiriman->tanggal_sampai = date('Y-m-d');

        $pengiriman->save();

        return response()->json($pengiriman, 200);
    }

    public function updateJumlahHariTelat($id,$hari)
    {
        // return response()->json($id);
        $pengiriman = Pengiriman::find($id);

        $pengiriman->jumlah_hari_telat = $hari;

        $pengiriman->save();

        return response()->json($pengiriman, 200);
    }

    public function cekResiStatus($no_resi)
    {
        // return response()->json($no_resi);
        $status_pengiriman = DB::table('status_pengiriman')
                    ->join('pengiriman', 'pengiriman.id', 'status_pengiriman.id_pengiriman')
                    ->where('pengiriman.no_resi', $no_resi)
                    ->get();

        return response()->json($status_pengiriman, 200);    
    }

    public function cekResiKeterangan($no_resi)
    {
        // return response()->json($no_resi);
        $keterangan = DB::table('pengiriman')
                    ->join('kelompok_pengiriman', 'kelompok_pengiriman.id', 'pengiriman.id_kelompok_pengiriman')
                    ->where('no_resi', $no_resi)
                    ->get();

        return response()->json($keterangan, 200);    
    }

    public function getRiwayatPengirimanByIdUsers($id)
    {
        $getMember = DB::table('member')
                    ->where('member.id_user', $id)
                    // ->join('perusahaan', 'perusahaan.id', 'member.id_perusahaan')
                    ->get();

        $resp = [];
        foreach ($getMember as $key) {
            $getPengirimanMember = DB::table('pengiriman')
                    ->where('id_member', $key->id)
                    ->where('tanggal_sampai', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->join('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->get();
                    
                    foreach ($getPengirimanMember as $key2) {
                        array_push($resp, $key2);
                    }
        }
        return response()->json($resp,200);
    }

    public function loginMobile(Request $request)
    {
        $check  = Pengiriman::where('no_resi', $request->no_resi)
                            ->where('pin', $request->pin);
                            
        if ($check->count() != 0) {
            $check = $check->first();

            $resp = [
                'status'    => 200,
                'data'      => $check,
                'message'   => 'Login Sukses'
            ];
            return response()->json($resp, 200);
        } else {
            $resp = [
                'status'    => 401,
                'message'   => 'Login Gagal, Kombinasi No Resi dan PIN Tidak Sesuai'
            ];
            return response()->json($resp, 401);
        }

    }

    public function updatePengirimanCoor(Request $request)
    {
        $data   = Pengiriman::find($request->id);

        $data->latitude     = $request->latitude;
        $data->longitude    = $request->longitude;
        $data->save();

        $resp = [
            'status' => 200,
            'data'  => $data
        ];
        return response()->json($resp, 200);
    }


}

