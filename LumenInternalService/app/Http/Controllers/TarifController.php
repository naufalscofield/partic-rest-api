<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Tarif;
use App\Models\TarifPelanggan;
use App\Models\PengajuanTarif;
use App\Models\DetailPengajuanTarif;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TarifController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
    * Return full list of authors
    * @return Response
    */
    public function getTarifByIdPerusahaan($idPerusahaan)
    {
        $getTarif = Tarif::where('id_perusahaan', $idPerusahaan)->get();
        // $getTarif = Tarif::all();
        
        return response()->json($getTarif);
    }

    public function getTarifByKota($idPerusahaan, $kota)
    {
        $kotaAsal = str_replace('-', ' ', $kota);

        $tarif = DB::table('tarif')
                    // ->where('id_perusahaan', $idPerusahaan)
                    ->where('status_verifikasi', 1)
                    ->where('asal', $kotaAsal)
                    ->select('tarif.*', 'tarif.id as id_tarif', 'mitra.*')
                    ->join('mitra', 'mitra.id', 'tarif.id_mitra')
                    // ->join('users', 'users.id', 'mitra.id_user')
                    // ->join('biodata', 'biodata.id_user', 'users.id')
                    ->get();
                    
        $data = [];
        foreach($tarif as $el)
        {
            $user   = DB::table('users')
                        ->where('id', $el->id_user)
                        ->first();
            $biodata= DB::table('biodata')
                        ->where('id_user', $el->id_user)
                        ->first();

            $value  = [
                'tarif' => $el,
                'user' => $user,
                'biodata' => $biodata,
            ];

            array_push($data, $value);
        }
        
        return response()->json($data, 200);
    }

    public function getPengajuanTarifByKota($idPerusahaan, $kota)
    {
        $kotaAsal = str_replace('-', ' ', $kota);

        $tarif = DB::table('tarif')
                    ->where('id_perusahaan', $idPerusahaan)
                    ->where('status_verifikasi', 2)
                    ->where('asal', $kotaAsal)
                    ->select('tarif.*', 'tarif.id as id_tarif', 'mitra.*')
                    ->join('mitra', 'mitra.id', 'tarif.id_mitra')
                    // ->join('users', 'users.id', 'mitra.id_user')
                    // ->join('biodata', 'biodata.id_user', 'users.id')
                    ->get();
                    
        $data = [];
        foreach($tarif as $el)
        {
            $user   = DB::table('users')
                        ->where('id', $el->id_user)
                        ->first();
            $biodata= DB::table('biodata')
                        ->where('id_user', $el->id_user)
                        ->first();

            $value  = [
                'tarif' => $el,
                'user' => $user,
                'biodata' => $biodata,
            ];

            array_push($data, $value);
        }
        
        return response()->json($data, 200);
    }

    public function updatePengajuanTarif(Request $request)
    {
        $pengajuan_tarif = Tarif::find($request->id_tarif);

        $pengajuan_tarif->status_verifikasi = $request->status_verifikasi;
        $pengajuan_tarif->keterangan_penolakan = $request->keterangan_penolakan;

        $pengajuan_tarif->save();

        return response()->json($pengajuan_tarif, 200);
    }

    public function updatePengajuanTarifByMitra(Request $request, $id)
    {
        // return $id;
        $tarif = Tarif::find($id);
        // return response()->json($tarif);
        $tarif->id_mitra           = $request->id_mitra;
        $tarif->asal               = $request->asal;
        $tarif->tujuan             = $request->tujuan;
        $tarif->jenis_pengiriman   = $request->jenis_pengiriman;
        $tarif->moda               = $request->moda;
        $tarif->jenis_pengangkutan = $request->jenis_pengangkutan;
        $tarif->nama_barang        = $request->nama_barang;
        $tarif->tarif_normal       = $request->tarif_normal;
        $tarif->durasi             = $request->durasi;
        $tarif->minimal_berat      = $request->minimal_berat;
        $tarif->maksimal_berat     = $request->maksimal_berat;
        $tarif->minimal_dimensi    = $request->minimal_dimensi;
        $tarif->maksimal_dimensi   = $request->maksimal_dimensi;
        $tarif->minimal_jumlah     = $request->minimal_jumlah;
        $tarif->maksimal_jumlah    = $request->maksimal_jumlah;

        $tarif->save();

        return response()->json($tarif, 200);
    }

    public function storePengajuanTarif(Request $request)
    {    
        // print_r($request->all()); die;
        $pengajuan_tarif = new Tarif;

        $pengajuan_tarif->id_mitra           = $request->id_mitra;
        $pengajuan_tarif->asal               = $request->asal;
        $pengajuan_tarif->tujuan             = $request->tujuan;
        $pengajuan_tarif->jenis_pengiriman   = $request->jenis_pengiriman;
        $pengajuan_tarif->moda               = $request->moda;
        $pengajuan_tarif->jenis_pengangkutan = $request->jenis_pengangkutan;
        $pengajuan_tarif->nama_barang        = $request->nama_barang;
        $pengajuan_tarif->tarif_normal              = $request->tarif_normal;
        $pengajuan_tarif->durasi             = $request->durasi;
        $pengajuan_tarif->minimal_berat      = $request->minimal_berat;
        $pengajuan_tarif->maksimal_berat     = $request->maksimal_berat;
        $pengajuan_tarif->minimal_dimensi    = $request->minimal_dimensi;
        $pengajuan_tarif->maksimal_dimensi   = $request->maksimal_dimensi;
        $pengajuan_tarif->minimal_jumlah     = $request->minimal_jumlah;
        $pengajuan_tarif->maksimal_jumlah    = $request->maksimal_jumlah;

        $pengajuan_tarif->save();

        return response()->json($pengajuan_tarif, 200);
    }

    // public function updatePengajuanTarif(Request $request, $id)
    // {    
    //     $pengajuan_tarif = PengajuanTarif::find($id);
        
    //     $pengajuan_ke = $pengajuan_tarif->pengajuan_ke;
    //     $pengajuan_ke += 1;

    //     $pengajuan_tarif->pengajuan_ke = $pengajuan_ke;

    //     $pengajuan_tarif->save();

    //     return response()->json($pengajuan_tarif, 200);
    // }

    public function storeDetailPengajuanTarif(Request $request)
    {
        $detail = new DetailPengajuanTarif;

        $detail->id_pengajuan_tarif = $request->id_pengajuan_tarif;
        $detail->id_mitra           = $request->id_mitra;
        $detail->asal               = $request->asal;
        $detail->tujuan             = $request->tujuan;
        $detail->jenis_pengiriman   = $request->jenis_pengiriman;
        $detail->moda               = $request->moda;
        $detail->jenis_pengangkutan = $request->jenis_pengangkutan;
        $detail->nama_barang        = $request->nama_barang;
        $detail->tarif              = $request->tarif;
        $detail->durasi             = $request->durasi;
        $detail->minimal_berat      = $request->minimal_berat;
        $detail->maksimal_berat     = $request->maksimal_berat;
        $detail->minimal_dimensi    = $request->minimal_dimensi;
        $detail->maksimal_dimensi   = $request->maksimal_dimensi;
        $detail->minimal_jumlah     = $request->minimal_jumlah;
        $detail->maksimal_jumlah    = $request->maksimal_jumlah;
        $detail->status             = 'pending';
        $detail->pengajuan_ke       = 1;
        $detail->keterangan         = 'Menunggu Verifikasi';

        $detail->save();

        return response()->json($detail, 200);
    }

    public function updateDetailPengajuanTarif(Request $request, $id)
    {
        
        $detail = DetailPengajuanTarif::find($id);
        
        $pengajuan_ke = $detail->pengajuan_ke;
        $pengajuan_ke += 1;
        // return response()->json($request->all(), 200);

        $detail->id_pengajuan_tarif = $request->id_pengajuan_tarif;
        $detail->id_mitra           = $request->id_mitra;
        $detail->asal               = $request->asal;
        $detail->tujuan             = $request->tujuan;
        $detail->jenis_pengiriman   = $request->jenis_pengiriman;
        $detail->moda               = $request->moda;
        $detail->jenis_pengangkutan = $request->jenis_pengangkutan;
        $detail->nama_barang        = $request->nama_barang;
        $detail->tarif              = $request->tarif;
        $detail->durasi             = $request->durasi;
        $detail->minimal_berat      = $request->minimal_berat;
        $detail->maksimal_berat     = $request->maksimal_berat;
        $detail->minimal_dimensi    = $request->minimal_dimensi;
        $detail->maksimal_dimensi   = $request->maksimal_dimensi;
        $detail->minimal_jumlah     = $request->minimal_jumlah;
        $detail->maksimal_jumlah    = $request->maksimal_jumlah;
        $detail->status             = 'pending';
        $detail->pengajuan_ke       = $pengajuan_ke;
        $detail->keterangan         = 'Menunggu Verifikasi';

        $detail->save();

        return response()->json($detail, 200);
    }

    public function deletePengajuanTarif($id)
    {
        $detail = DetailPengajuanTarif::where('id_pengajuan_tarif',$id);
        $detail->delete();

        $pengajuan_tarif = PengajuanTarif::find($id);
        $pengajuan_tarif->delete();
        

        $resp = [
            'Pengajuan Tarif' => $pengajuan_tarif,
            'Detail Pengajuan Tarif' => $detail,
        ];
        return response()->json($resp, 200);
    }

    public function deletePengajuanTarif2($id)
    {
        $tarif = Tarif::where('id',$id);
        $tarif->delete();

        return response()->json($tarif, 200);
    }

    public function deleteDetailPengajuanTarif($id)
    {
        $detail = DetailPengajuanTarif::find($id);
        $detail->delete();

        $resp = [
            'Detail Pengajuan Tarif' => $detail,
        ];

        return response()->json($resp, 200);
    }

    public function getTarifPelanggan($idPerusahaan)
    {
        $tarif = TarifPelanggan::where('id_perusahaan', $idPerusahaan)->get();

        return response()->json($tarif, 200);
    }

    public function showTarifPelanggan($idTarif)
    {
        $tarif = TarifPelanggan::find($idTarif);

        return response()->json($tarif, 200);
    }

    public function showTarifPelangganByParams($id_perusahaan, $jenis_pengangkutan, $jenis_pengiriman, $nama_barang, $kota_asal, $kota_tujuan)
    {
        $asal  = str_replace('-', ' ', $kota_asal);
        $tujuan  = str_replace('-', ' ', $kota_tujuan);

        if ($jenis_pengangkutan == 'koli') 
        {
            $tarif = DB::table('tarif_pelanggan')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('jenis_pengangkutan', $jenis_pengangkutan)
                        ->where('jenis_pengiriman', $jenis_pengiriman)
                        ->where('nama_barang', $nama_barang)
                        ->where('asal', $asal)
                        ->where('tujuan', $tujuan)
                        ->first();
            } else {
            $tarif = DB::table('tarif_pelanggan')
                        ->where('id_perusahaan', $id_perusahaan)
                        ->where('jenis_pengangkutan', $jenis_pengangkutan)
                        ->where('jenis_pengiriman', $jenis_pengiriman)
                        ->where('asal', $asal)
                        ->where('tujuan', $tujuan)
                        ->first();
        }

        return response()->json($tarif, 200);
    }

    public function storeTarifPelanggan(Request $request, $idPerusahaan)
    {
        $tarif = new TarifPelanggan;

        $tarif->id_perusahaan = $idPerusahaan;
        $tarif->asal = $request->asal;
        $tarif->tujuan = $request->tujuan;
        $tarif->tarif = $request->tarif;

        $tarif->save();

        return response()->json($tarif, 200);
    }

    public function updateTarifPelanggan(Request $request, $idTarif)
    {
        $tarif = TarifPelanggan::find($idTarif);

        $tarif->asal = $request->asal;
        $tarif->tujuan = $request->tujuan;
        $tarif->tarif = $request->tarif;

        $tarif->save();

        return response()->json($tarif, 200);
    }

    public function deleteTarifPelanggan($idTarif)
    {
        $tarif = TarifPelanggan::find($idTarif)->delete();

        return response()->json($tarif, 200);
    }

    public function getTarifByIdMitra($idMitra)
    {
        // return $idMitra;

        $tarif = DB::table('tarif')
                    ->where('id_mitra', $idMitra)
                    ->where('deleted_at', null)
                    // ->select('tarif.*', 'perusahaan.nama_perusahaan')
                    // ->join('perusahaan', 'perusahaan.id', 'tarif.id_perusahaan')
                    // ->join('users', 'users.id', 'mitra.id_user')
                    // ->join('biodata', 'biodata.id_user', 'users.id')
                    ->get();
        
        return response()->json($tarif, 200);
    }

}
