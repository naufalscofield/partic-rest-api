<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cabang;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class CabangController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getByIdRegional($id_regional)
    {
        $get = Cabang::where('id_regional', $id_regional)->get();
        
        return response()->json($get);
    }

    public function showByIdPerusahaan(Request $request, $idPerusahaan)
    {
        $showCabang = Cabang::where('id_perusahaan', $idPerusahaan)->get();

        $showCabang =  DB::table('cabang')
                        ->where('cabang.id_perusahaan', $idPerusahaan)
                        ->where('cabang.deleted_at', NULL)
                        ->join('regional', 'regional.id', 'cabang.id_regional')
                        ->select('cabang.*', 'cabang.id as id_cabang', 'regional.*', 'cabang.alamat as alamat_cabang', 'cabang.id as id_cabang')
                        ->get();

        return response()->json($showCabang,200);
    }
    
    public function store(Request $request)
    {
        $store = new Cabang;

        $store->id_perusahaan   = $request->id_perusahaan;
        $store->id_regional     = $request->id_regional;
        $store->alamat          = $request->alamat;
        $store->nama_cabang     = $request->nama_cabang;
        $store->no_telp         = $request->no_telp;
        $store->latitude        = $request->latitude;
        $store->longitude       = $request->longitude;

        $store->save();

        return response()->json($store, 201);
    }

    public function update(Request $request, $id)
    {
        $update = Cabang::find($id);
        
        $update->alamat = $request->alamat;
        $update->no_telp = $request->no_telp;
        $update->nama_cabang = $request->nama_cabang;
        $update->longitude = $request->longitude;
        $update->latitude = $request->latitude;
        $update->save();
        return $this->successResponse($update,200);
    }

    public function show($id)
    {
        $cabang = Cabang::find($id);

        return response()->json($cabang, 200);
    }

    public function delete($id)
    {
        $cabang = Cabang::find($id);
        $cabang->delete();

        return response()->json($cabang, 200);
    }

    public function getCabangByIdPerusahaan($id)
    {
        $showCabang =  DB::table('cabang')
                        ->where('cabang.id_perusahaan', $id)
                        ->where('cabang.deleted_at', NULL)
                        ->join('regional', 'regional.id', 'cabang.id_regional')
                        ->select('cabang.*', 'cabang.id as id_cabang', 'regional.*', 'cabang.alamat as alamat_cabang', 'cabang.id as id_cabang', 'cabang.no_telp as no_telp_cabang')
                        ->get();

        return response()->json($showCabang,200);
    }
}
