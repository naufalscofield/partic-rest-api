<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Tarif;
use App\Models\PengajuanTarif;
use App\Models\LobiPengiriman;
use App\Models\DetailPengajuanTarif;
use App\Models\KelompokPengiriman;
use App\Models\LobiMitra;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LobiMitraController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

   public function delete($id)
   {
       return response()->json('$lobi', 200);
    //    $lobi = LobiPengiriman::where('id_pengiriman', $id)->first();
    //    $lobi->delete();

    //    return response()->json($lobi, 200);
   }

   public function getLobiMitraByIdMitra($id)
    {
        // return $idMitra;

        $lobi_mitra = DB::table('lobi_mitra')
        // return response()->json('qw');
                    ->where('lobi_mitra.id_mitra', $id)
                    ->where('kelompok_pengiriman.tanggal_pickup', null)
                    ->where('kelompok_pengiriman.tanggal_pengiriman', null)
                    // ->select('lobi_mitra.*')
                    ->join('perusahaan', 'perusahaan.id', 'lobi_mitra.id_perusahaan')
                    // ->join('cabang', 'cabang.id_perusahaan', 'lobi_mitra.id_perusahaan')
                    ->join('kelompok_pengiriman', 'kelompok_pengiriman.id', 'lobi_mitra.id_kelompok_pengiriman')
                    // ->join('biodata', 'biodata.id_user', 'users.id')
                    ->get();
        return response()->json($lobi_mitra, 200);
        
    }

    public function getPengirimanByIdKelompokPengiriman($id)
    {
        $pengiriman = DB::table('pengiriman')
                    ->where('pengiriman.id_kelompok_pengiriman', $id)
                    // ->join('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->leftJoin('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->get();
        return response()->json($pengiriman, 200);
        
    }

    public function getPengirimanPickUpByIdKelompokPengiriman($id)
    {
        $lobi_mitra = DB::table('lobi_mitra')
                    ->where('lobi_mitra.id_mitra', $id)
                    ->where('kelompok_pengiriman.tanggal_pickup', '!=', null)
                    ->where('kelompok_pengiriman.tanggal_pengiriman', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'lobi_mitra.id_perusahaan')
                    ->join('kelompok_pengiriman', 'kelompok_pengiriman.id', 'lobi_mitra.id_kelompok_pengiriman')
                    ->get();

        $resp = [];    
        foreach ($lobi_mitra as $key) {
            $pengiriman = DB::table('pengiriman')
                    ->where('pengiriman.id_kelompok_pengiriman', $key->id_kelompok_pengiriman)
                    ->where('pengiriman.tanggal_sampai', '=', null)
                    // ->rightJoin('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->leftJoin('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->select('cabang.*', 'pengiriman.*', 'pengiriman.id as id_pengiriman')
                    ->get();

                    foreach ($pengiriman as $key2) {
                        array_push($resp, $key2);
                    }
        }

        return response()->json($resp, 200);    
    }

    public function getRiwayatPengirimanPickUpByIdKelompokPengiriman($id)
    {
        $lobi_mitra = DB::table('lobi_mitra')
                    ->where('lobi_mitra.id_mitra', $id)
                    ->where('kelompok_pengiriman.tanggal_pickup', '!=', null)
                    ->where('kelompok_pengiriman.tanggal_pengiriman', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'lobi_mitra.id_perusahaan')
                    ->join('tarif', 'tarif.id', 'lobi_mitra.id_tarif')
                    ->join('kelompok_pengiriman', 'kelompok_pengiriman.id', 'lobi_mitra.id_kelompok_pengiriman')
                    ->get();
        $resp = [];    

        foreach ($lobi_mitra as $key) {
            $pengiriman = DB::table('pengiriman')
                    ->where('pengiriman.id_kelompok_pengiriman', $key->id_kelompok_pengiriman)
                    ->where('pengiriman.tanggal_sampai', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->join('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->join('kelompok_pengiriman', 'pengiriman.id_kelompok_pengiriman', 'kelompok_pengiriman.id')
                    ->select('perusahaan.*', 'cabang.*', 'pengiriman.*', 'kelompok_pengiriman.*', 'pengiriman.id as id_pengiriman')
                    ->get();

                    foreach ($pengiriman as $key2) {
                        array_push($resp, $key2);
                    }
        }

        return response()->json($resp, 200);       
    }

    public function getPendapatanMitra($id)
    {
        $lobi_mitra = DB::table('lobi_mitra')
                    ->where('lobi_mitra.id_mitra', $id)
                    ->where('kelompok_pengiriman.tanggal_pickup', '!=', null)
                    ->where('kelompok_pengiriman.tanggal_pengiriman', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'lobi_mitra.id_perusahaan')
                    ->join('tarif', 'tarif.id', 'lobi_mitra.id_tarif')
                    ->join('kelompok_pengiriman', 'kelompok_pengiriman.id', 'lobi_mitra.id_kelompok_pengiriman')
                    ->get();
                    // return response()->json($lobi_mitra);
        $resp = [];    

        foreach ($lobi_mitra as $key) {
            $pengiriman = DB::table('pengiriman')
                    ->where('pengiriman.id_kelompok_pengiriman', $key->id_kelompok_pengiriman)
                    ->where('pengiriman.tanggal_sampai', '!=', null)
                    ->join('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->join('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->join('kelompok_pengiriman', 'pengiriman.id_kelompok_pengiriman', 'kelompok_pengiriman.id')
                    ->select('perusahaan.*', 'cabang.*', 'pengiriman.*', 'kelompok_pengiriman.*', 'pengiriman.id as id_pengiriman')
                    ->get();

                    foreach ($pengiriman as $key2) {
                        $kelompok_pengiriman = DB::table('kelompok_pengiriman')
                        ->where('id', $key2->id_kelompok_pengiriman)
                        ->first();

                        $tarif = DB::table('tarif')
                        ->where('id', $kelompok_pengiriman->id_tarif_line)
                        ->first();

                        $pengirimanFinal = ['pengiriman' => $key2,
                                            'tarif' => $tarif->tarif_normal
                                            ];
                        array_push($resp, $pengirimanFinal);
                    }
        }

        return response()->json($resp, 200);       
    }

    public function updateStatusPickUp($id)
    {
        $statusPickup = KelompokPengiriman::find($id);
        $currentDate = date('Y-m-d h:i:s');
        // $currentDateFinal = getDate($currentDate);


        $statusPickup->tanggal_pickup = $currentDate;
        $statusPickup->tanggal_pengiriman = $currentDate;

        $statusPickup->save();
        return response()->json($statusPickup, 200);

        
    }

}
