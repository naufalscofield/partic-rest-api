<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->post('/loginMobile', 'PengirimanController@loginMobile');
$router->post('/perusahaan', 'PerusahaanController@store');
$router->get('/perusahaan', 'PerusahaanController@get');
$router->get('/perusahaan/showPerusahaan/{idPerusahaan}', 'PerusahaanController@show');
$router->put('/perusahaan/updatePerusahaan/{id}', 'PerusahaanController@update');
$router->put('/updatePengirimanCoor', 'PengirimanController@updatePengirimanCoor');

$router->post('/regional', 'RegionalController@store');
$router->get('/regional/{idperusahaan}', 'RegionalController@get');
$router->get('/regional/show/{id}', 'RegionalController@show');
$router->put('/regional/update/{id}', 'RegionalController@update');
$router->delete('/regional/delete/{id}', 'RegionalController@delete');

$router->get('/regional/cabang/regional/{id_regional}', 'CabangController@getByIdRegional');

$router->get('/cabang/perusahaan/{idPerusahaan}', 'CabangController@showByIdPerusahaan');
$router->get('/cabang/{id}', 'CabangController@show');
$router->post('/cabang', 'CabangController@store');
$router->put('/cabang/{id}', 'CabangController@update');
$router->delete('/cabang/{id}', 'CabangController@delete');

$router->put('/pengiriman/verifikasi/{id}', 'PengirimanController@verifikasi');
$router->post('/pengiriman', 'PengirimanController@store');
$router->get('/pengiriman/perusahaan/kota/{idPerusahaan}/{kota}', 'PengirimanController@getByKota');
$router->get('/pengiriman/perusahaan/{idPerusahaan}', 'PengirimanController@getByPerusahaan');
$router->put('/pengiriman/{id}', 'PengirimanController@update');
$router->get('/pengiriman/{id}', 'PengirimanController@getById');
$router->get('/pengiriman/cabang/{idCabang}', 'PengirimanController@getByCabang');

$router->get('/kelompok_pengiriman/{idRegional}', 'KelompokPengirimanController@get');
$router->get('/kelompok_pengiriman/show/{idKelompokPengiriman}', 'KelompokPengirimanController@show');
$router->post('/kelompok_pengiriman', 'KelompokPengirimanController@store');
$router->put('/kelompok_pengiriman', 'KelompokPengirimanController@update');
$router->put('/kelompok_pengiriman/smart', 'KelompokPengirimanController@updateFinal');
$router->get('/field_kelompok_pengiriman/{kota}/{id_perusahaan}', 'KelompokPengirimanController@getFieldKelompokPengiriman');
$router->get('/field2_kelompok_pengiriman/{tujuan}/{kota}/{id_perusahaan}', 'KelompokPengirimanController@getField2KelompokPengiriman');
$router->get('/field3_kelompok_pengiriman/{tujuan}/{jenis_pengangkutan}/{kota}/{id_perusahaan}', 'KelompokPengirimanController@getField3KelompokPengiriman');
$router->get('/field4_kelompok_pengiriman/{tujuan}/{jenis_pengangkutan}/{jenis_pengiriman}/{kota}/{id_perusahaan}', 'KelompokPengirimanController@getField4KelompokPengiriman');

$router->get('/perusahaan/getTarif/{idPerusahaan}', 'TarifController@getTarifByIdPerusahaan');
$router->get('/perusahaan/tarifPelanggan/{idPerusahaan}', 'TarifController@getTarifPelanggan');
$router->get('/perusahaan/tarifPelanggan/show/{idTarif}', 'TarifController@showTarifPelanggan');
$router->get('/perusahaan/tarifPelanggan/showByParams/{idPerusahaan}/{jenisPengangkutan}/{jenisPengiriman}/{namaBarang}/{kotaAsal}/{kotaTujuan}', 'TarifController@showTarifPelangganByParams');
$router->post('/perusahaan/tarifPelanggan/{idPerusahaan}', 'TarifController@storeTarifPelanggan');
$router->put('/perusahaan/tarifPelanggan/{idTarif}', 'TarifController@updateTarifPelanggan');
$router->delete('/perusahaan/tarifPelanggan/{idTarif}', 'TarifController@deleteTarifPelanggan');

$router->get('/perusahaan/getTarifByKota/{idPerusahaan}/{kota}', 'TarifController@getTarifByKota');
$router->get('/perusahaan/getPengajuanTarifByKota/{idPerusahaan}/{kota}', 'TarifController@getPengajuanTarifByKota');
$router->put('/perusahaan/updatePengajuanTarif', 'TarifController@updatePengajuanTarif');

$router->put('/pengajuanTarif/{id}', 'TarifController@updatePengajuanTarif');
$router->put('/detailPengajuanTarif/{id}', 'TarifController@updateDetailPengajuanTarif');

$router->delete('/pengajuanTarif/{id}', 'TarifController@deletePengajuanTarif');
$router->delete('/detailPengajuanTarif/{id}', 'TarifController@deleteDetailPengajuanTarif');

$router->post('/pengajuanTarif', 'TarifController@storePengajuanTarif');
$router->post('/detailPengajuanTarif', 'TarifController@storeDetailPengajuanTarif');

$router->put('/pickup/{id}', 'PengirimanController@pickup');

$router->delete('/removeLobiPengiriman/{id}', 'LobiPengirimanController@delete');

$router->get('/getNamaPerusahaan', 'PerusahaanController@get');

$router->get('/getTarifByIdMitra/{idMitra}', 'TarifController@getTarifByIdMitra');
$router->delete('/deletePengajuanTarif/{id}', 'TarifController@deletePengajuanTarif2');

$router->get('/getLobiMitraByIdMitra/{id}', 'LobiMitraController@getLobiMitraByIdMitra');
$router->get('/getPengirimanByIdKelompokPengiriman/{id}', 'LobiMitraController@getPengirimanByIdKelompokPengiriman');
$router->get('/getPengirimanPickUpByIdKelompokPengiriman/{id}', 'LobiMitraController@getPengirimanPickUpByIdKelompokPengiriman');
$router->put('/updateStatusPickUp/{id}', 'LobiMitraController@updateStatusPickUp');
$router->post('/inputStatusPengiriman', 'PengirimanController@inputStatusPengiriman');
$router->put('/updateTanggalSampai/{id}', 'PengirimanController@updateTanggalSampai');
$router->get('/getRiwayatPengirimanPickUpByIdKelompokPengiriman/{id}', 'LobiMitraController@getRiwayatPengirimanPickUpByIdKelompokPengiriman');
$router->put('/updatePengajuanTarifByMitra/{id}', 'TarifController@updatePengajuanTarifByMitra');
$router->get('/cekResiStatus/{no_resi}', 'PengirimanController@cekResiStatus');
$router->get('/cekResiKeterangan/{no_resi}', 'PengirimanController@cekResiKeterangan');

$router->get('/getCabangByIdPerusahaan/{id}', 'CabangController@getCabangByIdPerusahaan');
$router->get('/getRiwayatPengirimanByIdUsers/{id}', 'PengirimanController@getRiwayatPengirimanByIdUsers');
$router->get('/getPendapatanMitra/{id}', 'LobiMitraController@getPendapatanMitra');
$router->post('/inputPengiriman', 'PengirimanController@inputPengirimanByMitra');
$router->put('/updateJumlahHariTelat/{id}/{hari}', 'PengirimanController@updateJumlahHariTelat');
