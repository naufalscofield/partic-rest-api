<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKelompokPengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelompok_pengiriman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_perusahaan');
            $table->integer('id_regional');
            $table->integer('id_mitra');
            $table->integer('id_tarif_line')->nullable();
            $table->string('asal');
            $table->string('tujuan');
            $table->string('jenis_pengangkutan');
            $table->string('jenis_pengiriman');
            $table->integer('berat_total')->nullable();
            $table->integer('dimensi_total')->nullable();
            $table->integer('unit_total')->nullable();
            $table->date('tanggal_pickup')->nullable();
            $table->date('tanggal_pengiriman')->nullable();
            $table->integer('calon_mitra1')->nullable();
            $table->integer('calon_mitra2')->nullable();
            $table->integer('calon_mitra3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_kelompok_pengiriman');
    }
}
