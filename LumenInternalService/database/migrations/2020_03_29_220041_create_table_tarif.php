<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTarif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_perusahaan');
            $table->integer('id_mitra');
            $table->string('asal');
            $table->string('tujuan');
            $table->string('jenis_pengiriman');
            $table->string('moda');
            $table->string('jenis_pengangkutan');
            $table->string('nama_barang')->nullable();
            $table->integer('tarif_normal');
            $table->integer('durasi');
            $table->integer('minimal_berat')->nullable();
            $table->integer('maksimal_berat')->nullable();
            $table->integer('minimal_jumlah')->nullable();
            $table->integer('maksimal_jumlah')->nullable();
            $table->integer('minimal_dimensi')->nullable();
            $table->integer('maksimal_dimensi')->nullable();
            $table->integer('status_verifikasi')->default(2);
            $table->integer('keterangan_penolakan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarif');
    }
}
