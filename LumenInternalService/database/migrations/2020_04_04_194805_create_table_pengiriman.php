<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_resi');
            $table->enum('jenis_pengangkutan', ['kilo', 'koli', 'dimensional']);
            $table->string('jenis_barang');
            $table->string('nama_barang');
            $table->integer('berat');
            $table->integer('dimensi')->nullable();
            $table->integer('total_unit');
            $table->enum('jenis_pengiriman', ['regular', 'express']);
            $table->string('kota_asal');
            $table->string('provinsi_tujuan');
            $table->string('kota_tujuan');
            $table->string('kecamatan_tujuan');
            $table->string('kelurahan_tujuan');
            $table->integer('kode_pos_tujuan');
            $table->longText('alamat_tujuan');
            $table->integer('id_member')->nullable();
            $table->string('nama_pengirim');
            $table->string('nama_penerima');
            $table->string('no_telp_pengirim');
            $table->string('no_telp_penerima');
            $table->integer('id_kelompok_pengiriman')->nullable();
            $table->integer('tarif');
            $table->integer('jumlah_hari_telat');
            $table->enum('verifikasi', ['pending', 'decline', 'accepted']);
            $table->longText('keterangan')->nullable();
            $table->integer('id_perusahaan');
            $table->integer('id_cabang');
            $table->date('perkiraan_tanggal_sampai')->nullable();
            $table->date('tanggal_sampai')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_pengiriman');
    }
}
