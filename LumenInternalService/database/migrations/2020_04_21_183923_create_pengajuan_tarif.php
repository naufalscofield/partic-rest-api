<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanTarif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_tarif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_perusahaan');
            $table->integer('id_mitra');
            $table->enum('status_verifikasi',['1','2']);
            $table->integer('pengajuan_ke')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_tarif');
    }
}
