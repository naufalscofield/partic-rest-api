<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPengajuanTarif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pengajuan_tarif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengajuan_tarif');
            $table->integer('id_mitra');
            $table->string('asal');
            $table->string('tujuan');
            $table->enum('jenis_pengiriman',['Regular','Express']);
            $table->enum('moda',['Darat','Udara','Laut']);
            $table->enum('jenis_pengangkutan',['Kilo','Koli','Dimensional']);
            $table->string('nama_barang')->nullable();
            $table->integer('tarif');
            $table->integer('durasi');
            $table->integer('minimal_berat')->nullable();
            $table->integer('maksimal_berat')->nullable();
            $table->integer('minimal_dimensi')->nullable();
            $table->integer('maksimal_dimensi')->nullable();
            $table->integer('minimal_jumlah')->nullable();
            $table->integer('maksimal_jumlah')->nullable();
            $table->enum('status',['pending','ditolak','diterima']);
            $table->integer('pengajuan_ke');
            $table->longText('keterangan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pengajuan_tarif');
    }
}
