-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2020 at 07:44 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `partic`
--

-- --------------------------------------------------------

--
-- Table structure for table `biodata`
--

CREATE TABLE `biodata` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_npwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_diri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biodata`
--

INSERT INTO `biodata` (`id`, `id_user`, `nama_lengkap`, `tanggal_lahir`, `tempat_lahir`, `alamat`, `no_telp`, `foto_ktp`, `foto_npwp`, `nik`, `foto_diri`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26, 26, 'Naufal', '2020-01-18', 'Bandung', 'sukarasa', '08965004585', 'lara.png', 'brba.png', '2345623456345', 'naufalramadhan.JPG', '2020-05-19 15:05:04', '2020-06-06 15:41:07', NULL),
(31, 37, 'Anggy', '1999-01-01', 'bandung', 'dago', '123', 'defaultktp.png', 'defaultnpwp.png', '321', 'defaultuser.jpg', '2020-06-07 21:31:04', '2020-06-07 22:35:13', NULL),
(33, 44, 'Ramdhan Eka Saputra', '1999-01-18', 'Ponorogo', 'asd', '009876523455', 'defaultktp.png', 'defaultnpwp.png', '1234', 'defaultuser.jpg', '2020-06-12 20:58:33', '2020-06-12 20:58:33', NULL),
(34, 45, 'admin', '1997-01-18', 'bandung', 'asdf', '1232', 'defaultktp.png', 'defaultnpwp.png', '123', 'defaultuser.jpg', '2020-06-12 21:05:39', '2020-06-12 21:52:11', '2020-06-12 21:52:11'),
(36, 47, 'Kurir 1', '2020-06-15', 'bandung', 'aweu', '081234567890', 'defaultktp.png', 'defaultnpwp.png', '12345', 'defaultuser.jpg', NULL, NULL, NULL),
(37, 48, 'Kurir 2', '2020-06-15', 'jakarta', 'cikidaw', '081234567890', 'defaultktp.png', 'defaultnpwp.png', '12345', 'defaultuser.jpg', NULL, NULL, NULL),
(39, 50, 'Farhan Maulana', '2020-07-09', 'Bandung', 'Pasir Impun, Kec. Mandalajati, Kota Bandung, Jawa Barat', '987654321', 'hipwee-dfadsfaf-700x42234.jpg', 'cara-membuat-npwp-online-klikpajakcom34.jpg', '123456789', 'Pas_Foto_3X434.jpg', '2020-07-09 22:47:06', '2020-07-09 22:47:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_regional` int(11) NOT NULL,
  `nama_cabang` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id`, `id_perusahaan`, `id_regional`, `nama_cabang`, `alamat`, `no_telp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 30, 19, 'Katamso', 'sukarasaaaaaaaaaaweu', '0812963', NULL, NULL, NULL),
(2, 30, 19, 'Antapani', 'aa', '123', '2020-06-09 22:11:34', '2020-06-09 22:39:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengajuan_tarif`
--

CREATE TABLE `detail_pengajuan_tarif` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pengajuan_tarif` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `asal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengiriman` enum('Regular','Express') COLLATE utf8mb4_unicode_ci NOT NULL,
  `moda` enum('Darat','Udara','Laut') COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengangkutan` enum('Kilo','Koli','Dimensional') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarif` int(11) NOT NULL,
  `durasi` int(11) NOT NULL,
  `minimal_berat` int(11) DEFAULT NULL,
  `maksimal_berat` int(11) DEFAULT NULL,
  `minimal_dimensi` int(11) DEFAULT NULL,
  `maksimal_dimensi` int(11) DEFAULT NULL,
  `minimal_jumlah` int(11) DEFAULT NULL,
  `maksimal_jumlah` int(11) DEFAULT NULL,
  `status` enum('pending','ditolak','diterima') COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengajuan_ke` int(11) NOT NULL,
  `keterangan` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kelompok_pengiriman`
--

CREATE TABLE `kelompok_pengiriman` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_regional` int(11) NOT NULL,
  `id_mitra` int(11) DEFAULT NULL,
  `calon_mitra1` int(11) DEFAULT NULL,
  `calon_mitra2` int(11) DEFAULT NULL,
  `calon_mitra3` int(11) DEFAULT NULL,
  `id_tarif_line` int(11) DEFAULT NULL,
  `id_tarif_line1` int(11) DEFAULT NULL,
  `id_tarif_line2` int(11) DEFAULT NULL,
  `id_tarif_line3` int(11) DEFAULT NULL,
  `asal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengangkutan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengiriman` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat_total` int(11) DEFAULT NULL,
  `dimensi_total` int(11) DEFAULT NULL,
  `unit_total` int(11) DEFAULT NULL,
  `nama_barang` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_pickup` date DEFAULT NULL,
  `tanggal_pengiriman` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelompok_pengiriman`
--

INSERT INTO `kelompok_pengiriman` (`id`, `id_perusahaan`, `id_regional`, `id_mitra`, `calon_mitra1`, `calon_mitra2`, `calon_mitra3`, `id_tarif_line`, `id_tarif_line1`, `id_tarif_line2`, `id_tarif_line3`, `asal`, `tujuan`, `jenis_pengangkutan`, `jenis_pengiriman`, `berat_total`, `dimensi_total`, `unit_total`, `nama_barang`, `tanggal_pickup`, `tanggal_pengiriman`, `created_at`, `updated_at`) VALUES
(12, 30, 19, 4, 2, 1, 1, 2, 2, 1, 4, 'Bandung', 'Jakarta', 'kilo', 'regular', 8, 0, 0, NULL, '2020-07-16', '2020-07-16', '2020-07-05 15:43:44', '2020-07-16 02:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `lobi_mitra`
--

CREATE TABLE `lobi_mitra` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `id_tarif` int(11) NOT NULL,
  `berat_total` int(11) NOT NULL,
  `dimensi_total` int(11) NOT NULL,
  `id_kelompok_pengiriman` int(11) DEFAULT NULL,
  `unit_total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lobi_mitra`
--

INSERT INTO `lobi_mitra` (`id`, `id_perusahaan`, `id_mitra`, `id_tarif`, `berat_total`, `dimensi_total`, `id_kelompok_pengiriman`, `unit_total`, `created_at`, `updated_at`) VALUES
(1, 30, 4, 7, 15, 0, 12, 0, NULL, NULL),
(2, 29, 4, 1, 15, 0, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lobi_pengiriman`
--

CREATE TABLE `lobi_pengiriman` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_pengiriman` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `verifikasi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `id_user`, `id_perusahaan`, `verifikasi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 44, 29, 1, NULL, '2020-06-17 21:47:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(9, '2016_06_01_000004_create_oauth_clients_table', 1),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(11, '2020_03_25_141045_create_users_table', 1),
(12, '2020_03_25_141341_create_biodata_table', 1),
(13, '2020_04_26_195301_create_choir', 1),
(14, '2020_03_25_144904_create_table_perusahaan', 2),
(15, '2020_03_25_213457_create_table_regional', 2),
(16, '2020_03_27_172356_create_table_cabang', 2),
(17, '2020_03_29_220041_create_table_tarif', 2),
(18, '2020_04_04_194805_create_table_pengiriman', 2),
(19, '2020_04_21_182931_create_lobi_pengiriman', 2),
(20, '2020_04_21_183923_create_pengajuan_tarif', 2),
(21, '2020_04_21_184104_create_detail_pengajuan_tarif', 2),
(22, '2020_03_29_211442_create_table_mitra', 3),
(23, '2020_04_04_210812_create_member', 3),
(24, '2020_04_13_200041_create_lobi_mitra', 3),
(25, '2020_04_14_204600_create_report_mitra', 3),
(26, '2020_06_09_200954_create_tarif_pelanggan_table', 4),
(27, '2020_06_27_104952_create_table_kelompok_pengiriman', 5),
(28, '2020_06_27_105732_create_status_pengiriman_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mitra`
--

CREATE TABLE `mitra` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis_kendaraan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kendaraan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plat_kendaraan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_kendaraan` int(11) NOT NULL,
  `domisili` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_stnk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_sim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verifikasi` int(11) NOT NULL DEFAULT 2,
  `status_ketersediaan` enum('Available','On Trip') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mitra`
--

INSERT INTO `mitra` (`id`, `id_user`, `jenis_kendaraan`, `nama_kendaraan`, `plat_kendaraan`, `tahun_kendaraan`, `domisili`, `foto_stnk`, `foto_sim`, `verifikasi`, `status_ketersediaan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, 'mobil', 'avanza', 'd3234', 2019, 'bandung', 'defaultstnk.png', 'defaultsim.png', 1, 'Available', NULL, NULL, NULL),
(2, 48, 'truk', 'fuso', 'd3234', 2019, 'bandung', 'defaultstnk.png', 'defaultsim.png', 1, 'Available', NULL, NULL, NULL),
(4, 50, 'mobil', 'Grand Max', 'D2134AB', 2014, 'Bandung', 'STNK-kendaraan-132.jpg', 'sim-palsu-yang-diamankan-polisi32.jpg', 1, 'Available', '2020-07-09 22:47:06', '2020-07-09 22:47:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0545e8847ce1106ab5416ce4ed1ad51a951db9840f0ffbf455b7e64471e9c21640ba9513c15ec1e8', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:55:04', '2020-06-09 15:55:04', '2021-06-09 15:55:04'),
('061d936c6df84295e13a57b44e7c1286a5a41ae4aaa09c112da05e938074020541b9f211409d75a7', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:58:48', '2020-07-10 00:58:48', '2021-07-10 00:58:48'),
('07dfccbe2b6350c947f123b40eefe9e6737ae8c1c846b37471e7d60b51fde98a689e7911c92f3bb2', 37, 2, NULL, '[\"*\"]', 1, '2020-06-17 20:57:42', '2020-06-17 20:57:42', '2021-06-17 20:57:42'),
('0a4bc8fa4e07873c3f2eebcb5a17f3fb05eeeb48242c0fa0c35f3b1b741dde7c7120421670bdcbf0', 26, 2, NULL, '[\"*\"]', 1, '2020-06-08 18:45:31', '2020-06-08 18:45:31', '2021-06-08 18:45:31'),
('0b5f63b775a92b23b2f62e6f022db8bcacfcc536e0ffc17a8f33e706c04da26db47ba801e67157b3', 26, 2, NULL, '[\"*\"]', 1, '2020-06-09 19:46:54', '2020-06-09 19:46:54', '2021-06-09 19:46:54'),
('0c1c7afeb3abc62d0123dff7a2a9c0d082329e3c7ffe235678f7eb00b9f738daa55c4f46e3fdd4da', 50, 2, NULL, '[\"*\"]', 1, '2020-07-11 08:59:08', '2020-07-11 08:59:08', '2021-07-11 08:59:08'),
('0cb0f818c45f9877bbc766c2908d0518e11150c5dc4925220acab5a40b981453f5237317b9724c4e', 26, 2, NULL, '[\"*\"]', 1, '2020-05-20 15:19:38', '2020-05-20 15:19:38', '2021-05-20 15:19:38'),
('11e0927f275da6c66d5d0127eefaa2ee3243a236d9b60be960659436a99048d2699a07362b288412', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 21:16:31', '2020-06-09 21:16:31', '2021-06-09 21:16:31'),
('157748c2abd56a5bb69c2447af38fd41becf3d33e3569e226e07ab4b2a93577327949f97dff16492', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:50:07', '2020-06-09 15:50:07', '2021-06-09 15:50:07'),
('18cd0bea86790c755396a192c313d4061bf99b6d2c327ea97f01430a4c53f76bc05835edfa83502d', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:30:50', '2020-07-10 00:30:50', '2021-07-10 00:30:50'),
('195d9d71dd3e3d84adcbfa293ab5a67c3a1fe1301fa7db15383e99900206400ecce0d2fffb0a0afe', 37, 2, NULL, '[\"*\"]', 1, '2020-06-19 18:32:20', '2020-06-19 18:32:20', '2021-06-19 18:32:20'),
('19a85846039b78eba0edbb0eb31ae5b67120db57a64697675fd787b2d7ae0fe2da2b4b599ad3a17f', 37, 2, NULL, '[\"*\"]', 1, '2020-06-15 11:20:12', '2020-06-15 11:20:12', '2021-06-15 11:20:12'),
('1cdfdbc2709e82a5e52f33301f731b48a6afd1d6ba875ea5dbf36f5883f8207423678e1462ccd68b', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 20:47:31', '2020-06-07 20:47:31', '2021-06-07 20:47:31'),
('20e935135030505d6ce9c3ac1ccec4fb1cd5a2c686f571b84161e259332633d2de7d549a92f3244b', 26, 2, NULL, '[\"*\"]', 1, '2020-06-09 14:28:47', '2020-06-09 14:28:47', '2021-06-09 14:28:47'),
('26297f6e803c2f14d7d2791b467486132ece957be0c7d5e5dddeef3ae140a73e1d20d09aba21232f', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 11:40:15', '2020-05-29 11:40:15', '2021-05-29 11:40:15'),
('26e099d864eb0c85ee964a88eded88bb104b23fec6133929f2486920353f311cc627518544b06561', 26, 2, NULL, '[\"*\"]', 0, '2020-06-19 19:11:56', '2020-06-19 19:11:56', '2021-06-19 19:11:56'),
('289c66be3d98c661d5c9c349f15a2b8ef3d6ea179543baf41ac00a5b66a15d150a79c3f1d592905b', 37, 2, NULL, '[\"*\"]', 1, '2020-06-23 01:08:33', '2020-06-23 01:08:33', '2021-06-23 01:08:33'),
('28fb4f35d787959c670a7cbf3842b382dc2e0ddda236631e2543ae619355a047e2f0ab79e5a4692c', 50, 2, NULL, '[\"*\"]', 0, '2020-07-16 10:55:13', '2020-07-16 10:55:13', '2021-07-16 10:55:13'),
('2b05794ace9bf5854eeee0fc6321a82f2387662842c08e27eeb672bfd865bc0ab2f015f0954557fb', 26, 2, NULL, '[\"*\"]', 1, '2020-05-20 16:03:43', '2020-05-20 16:03:43', '2021-05-20 16:03:43'),
('2ccfbafbc6aa93df553e921c98307965f14c1e35e0b10d7fa2ead04ad99513fdfe83687799d5909b', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:31:24', '2020-07-10 00:31:24', '2021-07-10 00:31:24'),
('2d5cc449b664134f23afa855db389c90a2b3a945b3cfa2cd1d2b7424ad93a6dc1c13dde08c0eac9b', 37, 2, NULL, '[\"*\"]', 1, '2020-06-20 21:58:44', '2020-06-20 21:58:44', '2021-06-20 21:58:44'),
('30e164c766735cd7e2114856249534591258e5052961530e521cd2fdc656c4b5ed6d0e2e96a9d9f3', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 10:38:18', '2020-06-05 10:38:18', '2021-06-05 10:38:18'),
('35fa141331df3aa81c1982826361d616c6f36416c8d90c6955bc13a53e7aed4a6a2cca33a6181eb7', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:58:33', '2020-07-10 00:58:33', '2021-07-10 00:58:33'),
('36ab8bea414e775b15b1643c84deaf20c7100238748f45b472a518c301816976a376205354268207', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:55:40', '2020-07-10 00:55:40', '2021-07-10 00:55:40'),
('381a24ff8c6034384fa03c2a51fb92612b37f2336034178bd22f80c040722267d259cc9d2c0e19bf', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:56:01', '2020-07-10 00:56:01', '2021-07-10 00:56:01'),
('390ff372599d7581396677ab90f9f49f07aafdeebb8c1d242944cacb309dc2ff71d91d4511a7ebbc', 37, 2, NULL, '[\"*\"]', 0, '2020-07-07 21:46:21', '2020-07-07 21:46:21', '2021-07-07 21:46:21'),
('3ba5c1df464d7cdba16c6c002a34fd5492793a84806865f21879d44c390902a5902ab176faf2f29d', 37, 2, NULL, '[\"*\"]', 1, '2020-06-23 09:18:25', '2020-06-23 09:18:25', '2021-06-23 09:18:25'),
('3c0b04449fee053356d77b1a1c8808803455b936db0295755af402e2a5e9f35f1d717c3b097ab451', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:55:39', '2020-06-09 15:55:39', '2021-06-09 15:55:39'),
('3c82a90333edc6470c6b3fe09e3a71659d25bbfb7beb69696069e88f085f54d5d25d7924d766fe06', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:25:31', '2020-05-19 15:25:31', '2021-05-19 15:25:31'),
('3c970ad968a5299c238f89ed6c05c24c18f393b5ef853778169cdec804f60283efc236c7cd551fd9', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 14:46:06', '2020-06-06 14:46:06', '2021-06-06 14:46:06'),
('3e0c0e466cb93b52fc5aea8c9b085390ef79b16081fbdc887b08f2d69d37a0de95d194c03c6a736e', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:47:23', '2020-07-10 00:47:23', '2021-07-10 00:47:23'),
('3e4ffbb2b43ab2d492681754ba16b6fb557f7db73243ec58212761fc96e815260be3623efdd27e88', 37, 2, NULL, '[\"*\"]', 1, '2020-06-19 19:12:34', '2020-06-19 19:12:34', '2021-06-19 19:12:34'),
('425b1d1e87d08778d6a560bf057a634a00450475bf52ad57264cc206df7944ec2059670f54be14cd', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:18:26', '2020-05-19 15:18:26', '2021-05-19 15:18:26'),
('4387a01418cb253783e3cc0abb1eec2401fac1582b258b824fee126e68e05dc00078e1446463ba9a', 26, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:53:07', '2020-06-09 15:53:07', '2021-06-09 15:53:07'),
('439d493fddfb04d2bae0566fa75e4d73a534f93382faf9e51c975a241845a39390fed3fa250b2b61', 50, 2, NULL, '[\"*\"]', 1, '2020-07-09 22:58:26', '2020-07-09 22:58:26', '2021-07-09 22:58:26'),
('440a09013e34baac8c86cbc5affefebdfec2f6003365673edfa50e8a7bd9668cfc95de4061d64a34', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 22:44:34', '2020-07-10 22:44:34', '2021-07-10 22:44:34'),
('45e13ef1dd90805e148d0e7b5552707e4bb3452c227ac5eca37c0e61131d54f50d1b4abc0f4d6680', 37, 2, NULL, '[\"*\"]', 1, '2020-07-07 21:27:53', '2020-07-07 21:27:53', '2021-07-07 21:27:53'),
('49c9d6d487cd6fce957b37fe089130e5523ee75c547d6719766daebed56bd826ce8abbdd53cbfe93', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:27:49', '2020-05-19 16:27:49', '2021-05-19 16:27:49'),
('4a61ecc88daed7d7d27551ab788f10ee035fa63976f9f1e40775cbdbc9a2aca2d5c91b40fe4c2d12', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:55:58', '2020-06-09 15:55:58', '2021-06-09 15:55:58'),
('4bd15f7b64c05e0edf9445e0ff748c0ed1d532ec68eb740f76b24edc75f548651c059ac1407bd126', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 14:42:34', '2020-06-06 14:42:34', '2021-06-06 14:42:34'),
('4e82738bee6b5baefdc433348670423224280f0f520e847b13cabf420dc2d057c99ec1f1b7065433', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 17:57:03', '2020-07-04 17:57:03', '2021-07-04 17:57:03'),
('4eb96e7b2662a8a0e61addd076fa714d31293979e507b308b685f7133e5d3621bb0f6f7b3df331aa', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:41:54', '2020-06-05 11:41:54', '2021-06-05 11:41:54'),
('4f02c31c3dbcbac8b19f98d3fd0b2334d8f27fe809582265e54eb7d41001360ec4f8f01e15608d40', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 10:49:16', '2020-07-10 10:49:16', '2021-07-10 10:49:16'),
('4fece2eb5046ac8188cee175dcc6793b910e5d698bee283c57032ca56b2a5884a0c7276c0c200181', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:19:21', '2020-06-05 11:19:21', '2021-06-05 11:19:21'),
('520cbbda9e98ee9be1414d26ca941e8cf7ecea6ba52653c85a9a305c1e076fc02f416d8bd0e84643', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 14:23:33', '2020-06-06 14:23:33', '2021-06-06 14:23:33'),
('5706cc7d5811ecefe9501c5745a1c9f0080f1d6370d8c30dee72eede90b00447ad56ad1e3871cd5a', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 11:35:40', '2020-05-29 11:35:40', '2021-05-29 11:35:40'),
('57397f67d4f035489f02f82d899d3b642115f9a878f3be16c4088739bb7cea8c43814865ab5ccb8d', 37, 2, NULL, '[\"*\"]', 1, '2020-06-30 18:50:33', '2020-06-30 18:50:33', '2021-06-30 18:50:33'),
('5b62141e3724b1d19eb49f096a2216454cc862dc2bf05db7ca91793b808204be7975f4dfe7cb10ba', 37, 2, NULL, '[\"*\"]', 1, '2020-07-07 20:03:51', '2020-07-07 20:03:51', '2021-07-07 20:03:51'),
('5e79a8b438097a1e5bff2ee9c10f20d3f194afb1f055865bc12147f52f6b25dd1e5be5befafbfd4c', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:51:48', '2020-06-09 15:51:48', '2021-06-09 15:51:48'),
('5f445f05ffe4fd1bc0b87de33bff7dca1911b6af1948df76c421f8b81dca649f273d9bc29f377ab7', 37, 2, NULL, '[\"*\"]', 1, '2020-06-19 19:23:19', '2020-06-19 19:23:19', '2021-06-19 19:23:19'),
('60df063b228568f0554e592c62d49da8e10cea5717a991ae8d8bd5be3f177d7ceedf23a55faa6c01', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 12:39:22', '2020-06-06 12:39:22', '2021-06-06 12:39:22'),
('6421b0929b717280d5595ee0c5954859081262506b87eed0de5ff24cbae94f926fea37b26bddde86', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:24:24', '2020-05-19 15:24:24', '2021-05-19 15:24:24'),
('6544567641df4c73622325485ade99ecb2fb7ca4b99d57297dd3be13d4af9e13e71517750c636a1a', 37, 2, NULL, '[\"*\"]', 1, '2020-06-13 23:55:16', '2020-06-13 23:55:16', '2021-06-13 23:55:16'),
('68de8e2ba2075cb1f9f8fa3ec46d68e03b67ade8a0814454bbe84fa3aeccabab697e836db6ce6b02', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:46:01', '2020-05-19 16:46:01', '2021-05-19 16:46:01'),
('6901014f9281bd92b5ac9b0c1fc1617cbc2419abe091000d6c7a85882a15fa0a844ca72cf0e920a4', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 10:39:54', '2020-06-05 10:39:54', '2021-06-05 10:39:54'),
('6b97855b6a6dbbcdb0964677504f9815b99611a955647ca69a8c983ad5b1451ab1188005b1a0499a', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 19:46:49', '2020-06-07 19:46:49', '2021-06-07 19:46:49'),
('6cae2b819f1496cf1fd010223fc9c3e442fe35d5a8ea0743d296247dcb7d2110e48105e05519225e', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 18:04:14', '2020-07-04 18:04:14', '2021-07-04 18:04:14'),
('70563525e02e27e6975bd8cd62b8e5445e74c9ce70e1e91eaac60665810d251a3b604976f99dee67', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 18:38:44', '2020-06-05 18:38:44', '2021-06-05 18:38:44'),
('7205a44f1c1b4fb6aa737f24f2c7878497d4d3bc10f53dba941df1e12bad91935bf0b03621af307e', 37, 2, NULL, '[\"*\"]', 1, '2020-06-16 22:33:04', '2020-06-16 22:33:04', '2021-06-16 22:33:04'),
('7382d1a7436b3035a1918f6ac5dc22dba4270f9ac3a1ee660fd496da87631ce7d7ee6253ea1d9f96', 37, 2, NULL, '[\"*\"]', 1, '2020-06-12 19:03:24', '2020-06-12 19:03:24', '2021-06-12 19:03:24'),
('75ac67f7d7cea26265f19d62c98b4f86f297eedbeef6a6d7666b0a3611a1dc1f10164bf974145425', 26, 2, NULL, '[\"*\"]', 1, '2020-06-08 18:53:52', '2020-06-08 18:53:52', '2021-06-08 18:53:52'),
('77decd0c3a7a48eb5d1aeefa512f4f59130bc6fe779d9ffaecf595b647c21d0c9a8f513384ac6e86', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 10:41:29', '2020-06-05 10:41:29', '2021-06-05 10:41:29'),
('7846098b815af8d7848e10e2810a21c8f958ff51b16a6e060dc9c8babfdb71c860e66cfa35d773fc', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 10:48:37', '2020-07-10 10:48:37', '2021-07-10 10:48:37'),
('787f5303175927f8ff9938c5bb74718bdb2a3687346302f0d8bf40259176dc28eb80b8730645323f', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:25:18', '2020-05-19 16:25:18', '2021-05-19 16:25:18'),
('79862d4af2e3be942008bab51017fc5856e2578f2efbf9263020bdff932e3e25d582a70499e90668', 26, 2, NULL, '[\"*\"]', 1, '2020-06-09 16:19:21', '2020-06-09 16:19:21', '2021-06-09 16:19:21'),
('7aa549ab99fb3d80900a91a8fdd6e302e4cfb8ac987a754c3d0b7f4deba16b54bf66d507ed0fc413', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:14:44', '2020-06-05 11:14:44', '2021-06-05 11:14:44'),
('7b5a183eb7b4ebce8d34183116e08ca48d5cee75ddeb215b3cebcf2a31501df54ecf4dfc13449eaf', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 12:46:44', '2020-06-07 12:46:44', '2021-06-07 12:46:44'),
('7c47800403865f5b1c2b05f22afbe80f980f8fed7af8e0327bde6a88e12731ddaecb5a1845948e3b', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 17:04:27', '2020-06-06 17:04:27', '2021-06-06 17:04:27'),
('7dc38241d24a59254801bc2920f72ff1ea01990b60b6bf68130b2f7255c545692c49a1e73e9ff5f5', 37, 2, NULL, '[\"*\"]', 1, '2020-06-14 11:30:11', '2020-06-14 11:30:11', '2021-06-14 11:30:11'),
('7e4d516d87e6db48cc8f36c582e4f81a72748afff74c152a9652f738d780e9fd42c3661ed0230fc3', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 21:08:03', '2020-07-04 21:08:03', '2021-07-04 21:08:03'),
('82fd9eb1fb1d9df8e02170321210b2a500b3efc1a68fd7ed067eff518c1110a0b693b7fc380465e7', 50, 2, NULL, '[\"*\"]', 1, '2020-07-09 22:55:39', '2020-07-09 22:55:39', '2021-07-09 22:55:39'),
('84312127f545fa1cc4e56a43e292e447a30a124d23d57bb31cad5f6ae8f6a7a9436bd9e9f8d0938e', 50, 2, NULL, '[\"*\"]', 1, '2020-07-09 23:00:40', '2020-07-09 23:00:40', '2021-07-09 23:00:40'),
('869328b4694637f39e39cb79a9961ede6a9d7a1b40c533b5f70849c07ad270c05d58986aaac5a41b', 50, 2, NULL, '[\"*\"]', 1, '2020-07-16 02:16:07', '2020-07-16 02:16:07', '2021-07-16 02:16:07'),
('88ed8b005037840ff22299226a19b9f58afc8d0979b6d7a7a85aa23dee0914c271781d26de9ea2fa', 37, 2, NULL, '[\"*\"]', 1, '2020-06-10 18:59:14', '2020-06-10 18:59:14', '2021-06-10 18:59:14'),
('8a6051f84b0ba9812719361ba3c5c4c013bd7ae94f5559f5503a32d461f5182d9f8a762e43f0119b', 37, 2, NULL, '[\"*\"]', 1, '2020-06-27 10:34:36', '2020-06-27 10:34:36', '2021-06-27 10:34:36'),
('8ac4aa5f47145433dbd959882f1c47c847c7dcc283f3380f8083e7b153f4360956eb13f8dd6348f8', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:19:23', '2020-05-19 15:19:23', '2021-05-19 15:19:23'),
('8d42125183eb65e1b6fd65dd7fe76be348f5e027410cdd0f233b2b2ee49c9c2b942cff8c9db442f1', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:33:02', '2020-07-10 00:33:02', '2021-07-10 00:33:02'),
('91fdf96b2d735a9ec2337db627c9c55dfda8ef63b0a292040febeb0cd7125d95872798bff837076f', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 11:36:40', '2020-05-29 11:36:40', '2021-05-29 11:36:40'),
('9206a00063f112aebf96c152b902790d171d4b55f424eeb42c2bc69172143d61ea3f77e629e84a41', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:27:08', '2020-05-19 15:27:08', '2021-05-19 15:27:08'),
('93dc7d700737866e248302ffe37dcede590dcaf06b7cd47908199e49235625b907517c0ec9efef52', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 10:20:31', '2020-05-29 10:20:31', '2021-05-29 10:20:31'),
('944673b861fde32401ed44a9e7ffb5386577ac8974ea43350b2ad1258936510ae7989de6afaceb52', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 19:36:23', '2020-06-06 19:36:23', '2021-06-06 19:36:23'),
('94688628c8186cc1ab50a25c41ec7b13cbc8f0949a2db6c0e245b135965c96315b6d927398310441', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 10:49:54', '2020-07-10 10:49:54', '2021-07-10 10:49:54'),
('95398d7f8c99794803938ff65badf2131f0bf83f7e19deffb51c4f9a11a828c498d14912436be271', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:24:28', '2020-05-19 16:24:28', '2021-05-19 16:24:28'),
('95eed2df0f1bbc1fb790280c4ac5bacf549a2733ec51d4012908f0e1b39d372959204b91224f93c7', 50, 2, NULL, '[\"*\"]', 1, '2020-07-11 01:15:42', '2020-07-11 01:15:42', '2021-07-11 01:15:42'),
('96ee0c5ae007e21158ed0759b9a533947b199296b045e0d5fb67252727627338509e0818fc958e30', 26, 2, NULL, '[\"*\"]', 1, '2020-06-08 19:10:16', '2020-06-08 19:10:16', '2021-06-08 19:10:16'),
('979bbcdb98b9e9f5478a177a78acd9df5fabdb9fe1a124298350090c05fb52dc756b2f6571ab8c82', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 11:50:43', '2020-07-04 11:50:43', '2021-07-04 11:50:43'),
('9f63a17ff6b2ff8424126567a76636af47c5edcbbe0784365979a076f38f0faa551aa2e326c5e250', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 16:19:43', '2020-06-09 16:19:43', '2021-06-09 16:19:43'),
('a110fe62d9818f321bf345e43fca9f3c0a9c53ccb2b531818ee88664b4c27c5e4390e560552161f4', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:45:24', '2020-05-19 16:45:24', '2021-05-19 16:45:24'),
('a1bfa4fe3cb38c9eb7f49b7f620f3d8167e3fcbac47638cd646e34da315b0908acf274277124fdc5', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 10:12:24', '2020-07-10 10:12:24', '2021-07-10 10:12:24'),
('a342b5ff76d407ea0b103583d2c9ade7de1064aaf9036ebadceaa48cf229f40efc36fe3f62f625a4', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:19:19', '2020-05-19 15:19:19', '2021-05-19 15:19:19'),
('a404dfd6fb679cc191947ff584d33a666b78d31178e18784a3908142d5eb1e0d131ba38450f3bbef', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 11:38:38', '2020-05-29 11:38:38', '2021-05-29 11:38:38'),
('a6c2479deca45b71ca2bf32b29e83175292a1df40187ba9b62f15d96b07b56e298ff773bdf8e715b', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:35:19', '2020-07-10 00:35:19', '2021-07-10 00:35:19'),
('a9e8a5a79dbcf3a706bbfd3a46f49fdeff107e9e1c0d8b4660e878732f031d49461e68617ef1e095', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:23:55', '2020-05-19 16:23:55', '2021-05-19 16:23:55'),
('ab11b257b503ad018d5fc1907c6359251a414d4053d45431053233929630801dfbc7046d785f0c36', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:30:43', '2020-05-19 15:30:43', '2021-05-19 15:30:43'),
('ac02b5d5b1376328dcea817bcfb377c614967784956f6035ddc7d0ee916bc4cdad9f87b7f9be597f', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:56:56', '2020-07-10 00:56:56', '2021-07-10 00:56:56'),
('b08ca3493a74599dc89c64d400b085ed241ba7c9173b68b643f09cf93a95cfd5f7292e1b25037e07', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:41:23', '2020-06-05 11:41:23', '2021-06-05 11:41:23'),
('b1053bac7237b4ad6a106db508871856aa045ef1a38c4e87fc62a1040e6e04109ba413b2fde33be2', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 18:38:19', '2020-06-07 18:38:19', '2021-06-07 18:38:19'),
('b5285e7c9a3644bddee057d2f71b5d3e3ccd2f9a8dd828e0937562f7e01eed90a67b175a0d2ae5b4', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:20:11', '2020-05-19 15:20:11', '2021-05-19 15:20:11'),
('b6d1a2b155c24a03285e9a58cd8b93731ab3c6f7eb8f1bddbd103b8741988ea2b8985741803b6900', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 10:39:07', '2020-06-05 10:39:07', '2021-06-05 10:39:07'),
('ba3c97bab47ca127de761dc0f9e55909ceefc9c0591bbff5dc7c8f881249e9f8ade43ecaaa495e73', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:18:11', '2020-05-19 15:18:11', '2021-05-19 15:18:11'),
('bb0200e00ef67226827f652a5deab16de7408c56df00cf94892dbd93735945d903578deddbb457fa', 50, 2, NULL, '[\"*\"]', 1, '2020-07-09 22:47:15', '2020-07-09 22:47:15', '2021-07-09 22:47:15'),
('bd9496afaeec88654c154e5f152166cba8f39f9a20738879bbcf250e0c5fbe8ed0a212b1d40225c3', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 18:20:24', '2020-07-04 18:20:24', '2021-07-04 18:20:24'),
('bdfd0c6eb0d25ff7f7c930d5f14b0296b264558b09c74f04ac34a51451e89778f1a609ce8d53f9b2', 50, 2, NULL, '[\"*\"]', 1, '2020-07-15 21:18:36', '2020-07-15 21:18:36', '2021-07-15 21:18:36'),
('beab95cba14418170f7b2a339701de3af59ecd438a2967949987e6a55923f1b921c2c9efd2eee832', 26, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:54:35', '2020-06-09 15:54:35', '2021-06-09 15:54:35'),
('bfda6b01b7fc27d2285a8e47ee7806793bf33e2fa824b9714451b5b3340a3095233532ae675806a5', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:51:22', '2020-07-10 00:51:22', '2021-07-10 00:51:22'),
('c2ad9e9510c29928a559a97f3061252dc0685f001af0592a339e25563857a1b6dd3ba8647e3fe3f5', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:31:41', '2020-07-10 00:31:41', '2021-07-10 00:31:41'),
('c311af1a92df73a34726c347331edbfed1c1471884b7b0e8de90135ab69c82265217d0e37acf7b62', 37, 2, NULL, '[\"*\"]', 1, '2020-06-13 11:18:19', '2020-06-13 11:18:19', '2021-06-13 11:18:19'),
('c35323ccb84e5f140648a79680e236d3aff008d95fe8b94ebf58c36d65ab2134306efca4b8d0872f', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 14:28:26', '2020-06-06 14:28:26', '2021-06-06 14:28:26'),
('c46f5d9f0c8198e12a545e8be8799a3a9b832e3ed2587112a1ff3a7749ecdce330d83be0466ccef5', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:19:32', '2020-05-19 15:19:32', '2021-05-19 15:19:32'),
('c585f10fe30cc4d58851d503aa5dc4d4a3aade19e512b1640141f3aed560abf755bad9f7ec51ecfe', 26, 2, NULL, '[\"*\"]', 1, '2020-06-06 14:49:54', '2020-06-06 14:49:54', '2021-06-06 14:49:54'),
('c708bf4195ee7104917e93dfeb44f16d32d2c08292c5ed681e04b185e2b8c2dfb65840da70c63caf', 37, 2, NULL, '[\"*\"]', 1, '2020-07-03 15:34:07', '2020-07-03 15:34:07', '2021-07-03 15:34:07'),
('c88f6f997dd5e5aa10599c77d8e0a7d1b06b8bac34cdd8cb1d635a56b026266b2723e6dea886255f', 50, 2, NULL, '[\"*\"]', 1, '2020-07-15 21:17:05', '2020-07-15 21:17:05', '2021-07-15 21:17:05'),
('cb14ea3e09dea7fcddfd7134395fd2eda86c0820892e282887df14e810955293c06132276275539c', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 01:00:24', '2020-07-10 01:00:24', '2021-07-10 01:00:24'),
('cba330a328622fe8dfc555a81805d7ae304e6743f1de632892ce3140597a3703ebd080e05a381fb5', 37, 2, NULL, '[\"*\"]', 1, '2020-07-03 22:04:54', '2020-07-03 22:04:54', '2021-07-03 22:04:54'),
('cd3a8f20730c9a4886ab328fd4a0f8a27ca82195d8cf934ac34d7009a3fa4dd24a2264e1e67ccdeb', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 10:25:02', '2020-06-07 10:25:02', '2021-06-07 10:25:02'),
('d1d289866e180c6635330926a1305ab60b38bda0b7f61319df637146bff5f0d4eb77242ad43baff6', 37, 2, NULL, '[\"*\"]', 1, '2020-06-13 23:53:35', '2020-06-13 23:53:35', '2021-06-13 23:53:35'),
('d320470cec75cf59a18fbe6b423e91b321947d32ff3e109dba282bb5e6cf40c3210e7f5df83fa71d', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:40:36', '2020-06-05 11:40:36', '2021-06-05 11:40:36'),
('d6383e2152acfc8190cf7b7995b725a53b6a520522f2f982353c9001e4d960dcde087edb43c820a6', 37, 2, NULL, '[\"*\"]', 1, '2020-06-09 15:50:44', '2020-06-09 15:50:44', '2021-06-09 15:50:44'),
('da520c4cd0e04fb25b140f1430c51ff592ad977bf4a169adcd3c228e979aa6323a066f70264496a4', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:59:11', '2020-07-10 00:59:11', '2021-07-10 00:59:11'),
('da600d8c58b6061d4516a19dcdd6beba48d031624dd5e116e3cf59948bb37ab8db4dd6ee0404221e', 26, 2, NULL, '[\"*\"]', 1, '2020-06-05 11:39:48', '2020-06-05 11:39:48', '2021-06-05 11:39:48'),
('dbe972977667c7633c6e69c3c2c4aa9a416780370229bba054b437c08d121e05cf7a17a3dee4f7ff', 50, 2, NULL, '[\"*\"]', 1, '2020-07-09 22:47:36', '2020-07-09 22:47:36', '2021-07-09 22:47:36'),
('dd581686cc987fd11a490c23bd108aeb29308bfd9cd170bd0dcc98adf9a66b68d503575e76fb1c65', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:49:42', '2020-07-10 00:49:42', '2021-07-10 00:49:42'),
('de11535e0d2a1574bcd3adf81715fc81f0f19ee97ec059a02f6140a3621a85957633610df75bb062', 37, 2, NULL, '[\"*\"]', 1, '2020-07-04 11:50:01', '2020-07-04 11:50:01', '2021-07-04 11:50:01'),
('de3ff8390434faa638cca16e1fddd322fdeb2cb06ba9ffbdefa5ed6d4a6ad880bd0feaee26014035', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:31:10', '2020-05-19 16:31:10', '2021-05-19 16:31:10'),
('dffccd88e85921fe5fd97ba75d970713519ebf5425d9af079e2556f311d0fff40641a3feb32be5a4', 37, 2, NULL, '[\"*\"]', 1, '2020-06-23 01:39:22', '2020-06-23 01:39:22', '2021-06-23 01:39:22'),
('e1fc6ee1c700257d8a88fcf6ebee1be5c732d74d2408782f3f695e18cf4fe4d2a66fa0a40aa4c8e6', 37, 2, NULL, '[\"*\"]', 1, '2020-06-22 23:17:25', '2020-06-22 23:17:25', '2021-06-22 23:17:25'),
('e34a78d9df920043f15e1790126795fab66e6c810d1acde15d3a1577c7498a5692ee61e570455ac8', 37, 2, NULL, '[\"*\"]', 1, '2020-07-05 15:25:35', '2020-07-05 15:25:35', '2021-07-05 15:25:35'),
('e66447f313b62593f9b0f9f2b1cc665bb9f60b2b9f9d5562af3a259cd28357c4165c3852ceb78f86', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:32:41', '2020-07-10 00:32:41', '2021-07-10 00:32:41'),
('e8e7731ece97b4652df1a2efc84b11ee8bcc477cc20e287341195ad20bf2c672cb96727999a9ea7b', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:18:58', '2020-05-19 15:18:58', '2021-05-19 15:18:58'),
('ea697d647a220e1422acb3bd74bb3d84d05e2c5d91f19d0e43522775af05a535351a6daefc48c53a', 26, 2, NULL, '[\"*\"]', 1, '2020-05-29 10:27:55', '2020-05-29 10:27:55', '2021-05-29 10:27:55'),
('ecf05f1f40f9474fe86ee44e1172f394c21a81efc7c10360411034b717cb6ea08dc2bb21628567d9', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 00:58:06', '2020-07-10 00:58:06', '2021-07-10 00:58:06'),
('f1db242a023204c83a6ea4242df383c8d538dfd2d5009d74d6fddae3f3d045ec07af4431fb7fefaa', 37, 2, NULL, '[\"*\"]', 1, '2020-06-26 18:44:26', '2020-06-26 18:44:26', '2021-06-26 18:44:26'),
('f2485cb50f9cdde206bcc9f8456f2db27157fc7d7ef65f09b798bf01677f254b90bf1bdd455c01c8', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:32:49', '2020-05-19 15:32:49', '2021-05-19 15:32:49'),
('f35593226d4e5157a93826391d3b586322e4a5ec19015f2102618659509dc556a808cb17bf829673', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 20:57:34', '2020-06-07 20:57:34', '2021-06-07 20:57:34'),
('faa548dbd491080a5dfac81de046582343ee979f58468423250c19d7c0bc11cb1f62a60dd3df1be9', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 16:48:53', '2020-05-19 16:48:53', '2021-05-19 16:48:53'),
('fc33839fd8a7b736c5ac68ec7629f11e6bc92d4553dc681aa45def46ebc1c1d83f075b2d3144b6c9', 26, 2, NULL, '[\"*\"]', 1, '2020-06-07 18:50:52', '2020-06-07 18:50:52', '2021-06-07 18:50:52'),
('fca1d626be754036352f28acbde34c59d774fa81d0726049fd21aa24ecc904ce9925ab648d4f9173', 50, 2, NULL, '[\"*\"]', 1, '2020-07-10 10:02:58', '2020-07-10 10:02:58', '2021-07-10 10:02:58'),
('fd7b513c302e7719575acf4a791fb5f6e1d674a2ffc8903131287af9be0d08538f2fe44e80a0ed53', 26, 2, NULL, '[\"*\"]', 1, '2020-05-19 15:19:04', '2020-05-19 15:19:04', '2021-05-19 15:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, ' Personal Access Client', 'UkXqhWVeLv0hBUXJ8Kc8d19jNCHXFkLX3vCD2ffV', 'http://localhost', 1, 0, 0, '2020-05-19 15:17:25', '2020-05-19 15:17:25'),
(2, NULL, ' Password Grant Client', 'z2wCSR6tClX9HJYFropSsLQqrZhvT5ULHds64njL', 'http://localhost', 0, 1, 0, '2020-05-19 15:17:25', '2020-05-19 15:17:25'),
(3, NULL, ' Personal Access Client', 'fpgCf0oCNyhVj9o10JVRCTfWqYlhbMexCiDLQiAL', 'http://localhost', 1, 0, 0, '2020-07-09 22:34:20', '2020-07-09 22:34:20'),
(4, NULL, ' Password Grant Client', 'vEU2xGKkgeZmuQj48b3WH2ws9EWcPVnBdYeCqzPS', 'http://localhost', 0, 1, 0, '2020-07-09 22:34:20', '2020-07-09 22:34:20'),
(5, NULL, ' Personal Access Client', '0P0EV2z2RIzMZgFo9ZPtELSM43UdFWtT6LMqdE8G', 'http://localhost', 1, 0, 0, '2020-07-09 22:36:17', '2020-07-09 22:36:17'),
(6, NULL, ' Password Grant Client', 'szyMaHGwdRp8WgbOCh8KRMFGfBBlAo4x46EfkTOa', 'http://localhost', 0, 1, 0, '2020-07-09 22:36:17', '2020-07-09 22:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-19 15:17:25', '2020-05-19 15:17:25'),
(2, 3, '2020-07-09 22:34:20', '2020-07-09 22:34:20'),
(3, 5, '2020-07-09 22:36:17', '2020-07-09 22:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('025c8a4f5b13cd2de56ee071e19024c5cf807b9f479de86387791ebb320523a54bee1c4dd7effa18', '26e099d864eb0c85ee964a88eded88bb104b23fec6133929f2486920353f311cc627518544b06561', 0, '2021-06-19 19:11:56'),
('07a6b57e5429ab79f0a5c191ee0b0129d7a9229155d020bd48f6d3da1776a9739b6fb56d253294f8', '5e79a8b438097a1e5bff2ee9c10f20d3f194afb1f055865bc12147f52f6b25dd1e5be5befafbfd4c', 0, '2021-06-09 15:51:48'),
('08d43a20512e4f7f1161f7c960337484d936aa003cf7364629a8403265f313dae231199fa4051afe', 'fd7b513c302e7719575acf4a791fb5f6e1d674a2ffc8903131287af9be0d08538f2fe44e80a0ed53', 0, '2021-05-19 15:19:04'),
('0b20ab91504cb43b77a6b73d1132dc9f1cfde1127473f2c069ec49d2b5423b164573328167b41dfe', '787f5303175927f8ff9938c5bb74718bdb2a3687346302f0d8bf40259176dc28eb80b8730645323f', 0, '2021-05-19 16:25:18'),
('0c51d26551d67bc5df6d66959cbde0785533b20c5c96449edbc6f1e4bed58628210d94655f619b4c', '9f63a17ff6b2ff8424126567a76636af47c5edcbbe0784365979a076f38f0faa551aa2e326c5e250', 0, '2021-06-09 16:19:43'),
('0cf33caac1b298b6c515619bb4e2659bd256219307a7f458c394e961446be0334f1c0bebca2c40b6', '439d493fddfb04d2bae0566fa75e4d73a534f93382faf9e51c975a241845a39390fed3fa250b2b61', 0, '2021-07-09 22:58:26'),
('0d247e2428e12b363ab69f328305cc0e71a8fd1f6f0c721d364b4e04023babd35e3f33a87cdfe14e', '35fa141331df3aa81c1982826361d616c6f36416c8d90c6955bc13a53e7aed4a6a2cca33a6181eb7', 0, '2021-07-10 00:58:33'),
('10ff000bbada8b7af672c3a5851dfff61f4ce6cc26874ea014323e5f6c1674d04c78e970931122fa', '60df063b228568f0554e592c62d49da8e10cea5717a991ae8d8bd5be3f177d7ceedf23a55faa6c01', 0, '2021-06-06 12:39:22'),
('1140eb02322fa7fe0f67e1508e65de9afda3a19fdba04518557c541e2adbbf39f2c51ecc66e53325', 'e34a78d9df920043f15e1790126795fab66e6c810d1acde15d3a1577c7498a5692ee61e570455ac8', 0, '2021-07-05 15:25:35'),
('1319c790cf5cefc439cce117ca339397a854ece937611b1a4454ce56cc9d7eb3f8f0ec62befceb74', '96ee0c5ae007e21158ed0759b9a533947b199296b045e0d5fb67252727627338509e0818fc958e30', 0, '2021-06-08 19:10:16'),
('136980943897934cf19e0ee059ad76c459d65e2ebe2a38afe19d0ad120e1a73d451abb2d0e999ee4', '84312127f545fa1cc4e56a43e292e447a30a124d23d57bb31cad5f6ae8f6a7a9436bd9e9f8d0938e', 0, '2021-07-09 23:00:40'),
('1546f1cdcf850837ffb152240dd66d02cfb9e71e07db495c300474e171a5a880c07663d06e08e90b', 'e8e7731ece97b4652df1a2efc84b11ee8bcc477cc20e287341195ad20bf2c672cb96727999a9ea7b', 0, '2021-05-19 15:18:58'),
('177f544fa094b86de9e60b70ad77ae5a051e341eb0c9d954016624aecd4d577f87f4cec85ffee753', '157748c2abd56a5bb69c2447af38fd41becf3d33e3569e226e07ab4b2a93577327949f97dff16492', 0, '2021-06-09 15:50:07'),
('1a45fcc6ecb7d156ea8dd3b1ffdaca0729cf9d833b1b5355dcb81bb56f6123376a24faed2ce6b03a', '4a61ecc88daed7d7d27551ab788f10ee035fa63976f9f1e40775cbdbc9a2aca2d5c91b40fe4c2d12', 0, '2021-06-09 15:55:58'),
('1b5be29e89c3ec913a6b29bc0c5649b1a660d1103288d66e3a7b1f4372d7b669e797cc6a2de08ac5', '4e82738bee6b5baefdc433348670423224280f0f520e847b13cabf420dc2d057c99ec1f1b7065433', 0, '2021-07-04 17:57:03'),
('1bf35041771ab1ff6b87ae4e89df06b543bef2ae71ec92478f9ff82e8f13998263b44ff5a47df246', '0c1c7afeb3abc62d0123dff7a2a9c0d082329e3c7ffe235678f7eb00b9f738daa55c4f46e3fdd4da', 0, '2021-07-11 08:59:08'),
('1bf7ded1dc1ce882dc2100eb5025b309115670d422e3a1649c773eed260fbcbac8822b0ed29063a1', '28fb4f35d787959c670a7cbf3842b382dc2e0ddda236631e2543ae619355a047e2f0ab79e5a4692c', 0, '2021-07-16 10:55:13'),
('1cba025bc5a5f1767f9e502009c096833092255dcd4b3bcac196c1c5f8dae487c650f62dae50cdb8', 'cd3a8f20730c9a4886ab328fd4a0f8a27ca82195d8cf934ac34d7009a3fa4dd24a2264e1e67ccdeb', 0, '2021-06-07 10:25:02'),
('1f3a7c7f3d6c17d8c8ece9028dde6070ef843922f277a68ac65fe535cbc62eb1d63a183844b66d2f', '869328b4694637f39e39cb79a9961ede6a9d7a1b40c533b5f70849c07ad270c05d58986aaac5a41b', 0, '2021-07-16 02:16:07'),
('212a3e38f4360e0240dce77296d7b64c11f96f5b8dee48e5bd82d303f20b3a16ad9d1543f4a64f9c', 'ab11b257b503ad018d5fc1907c6359251a414d4053d45431053233929630801dfbc7046d785f0c36', 0, '2021-05-19 15:30:43'),
('2155354145058f288f321d6696229cfded5d95bbcc27e27a80a255be0b678cd3abbd127e96c9bf78', 'da520c4cd0e04fb25b140f1430c51ff592ad977bf4a169adcd3c228e979aa6323a066f70264496a4', 0, '2021-07-10 00:59:11'),
('2624cbaa9429c6251c3ceba2ce4d604c135b0c8fa096a99ffde3b854fa1e7cdfa4535d0c17182b07', '2ccfbafbc6aa93df553e921c98307965f14c1e35e0b10d7fa2ead04ad99513fdfe83687799d5909b', 0, '2021-07-10 00:31:24'),
('26bbfdf85d942d6bde05af137c9ac89e3ac9aa05b22c5744a05cea8af0a66c6792cc268bd5646e93', '9206a00063f112aebf96c152b902790d171d4b55f424eeb42c2bc69172143d61ea3f77e629e84a41', 0, '2021-05-19 15:27:08'),
('29c2a0b72a1b8b9c9bf5bc1c088aa3fe102ca8f4d2e2668446c519031dbe93773f7112be69a1efb5', 'de3ff8390434faa638cca16e1fddd322fdeb2cb06ba9ffbdefa5ed6d4a6ad880bd0feaee26014035', 0, '2021-05-19 16:31:10'),
('2c319df51fc4944713942fe40e7ad76357f6ddf59c2d2e44c89db624fab9ccbdca73fe55f6ace482', '93dc7d700737866e248302ffe37dcede590dcaf06b7cd47908199e49235625b907517c0ec9efef52', 0, '2021-05-29 10:20:31'),
('2eba7dba90b9aff450280953b7dab0d08d9938150b7748d79103d87812db87bd4c98be1a5835f080', 'bdfd0c6eb0d25ff7f7c930d5f14b0296b264558b09c74f04ac34a51451e89778f1a609ce8d53f9b2', 0, '2021-07-15 21:18:36'),
('307f5ddc0979aa1c2b03ce374455e86ab1d2cd5541b5194b82d4852ed38b2f417185c47d4160cba5', '70563525e02e27e6975bd8cd62b8e5445e74c9ce70e1e91eaac60665810d251a3b604976f99dee67', 0, '2021-06-05 18:38:44'),
('386a3e6d9e48aaeb5847aeb264ea45a124dda8b6cfed828cafa63d45215e4e7f5d87b6445378e086', '7c47800403865f5b1c2b05f22afbe80f980f8fed7af8e0327bde6a88e12731ddaecb5a1845948e3b', 0, '2021-06-06 17:04:27'),
('38976234e1ddb701073ce2416a58a597ad7b0cfd0f55c0a2dd587e04ed5646c1aed9adbd26adfdf4', 'ba3c97bab47ca127de761dc0f9e55909ceefc9c0591bbff5dc7c8f881249e9f8ade43ecaaa495e73', 0, '2021-05-19 15:18:11'),
('3ac768769a040048cff8efd20aa2ebb13fb55f69be71e5ce1d2256fcf70675de00eb283cd7f5590a', '440a09013e34baac8c86cbc5affefebdfec2f6003365673edfa50e8a7bd9668cfc95de4061d64a34', 0, '2021-07-10 22:44:34'),
('3ce6891105a4081f367586c97b1ee438915ce064158f5d07ccb5e9bba0d96ed4f7eec545c4ec9322', '520cbbda9e98ee9be1414d26ca941e8cf7ecea6ba52653c85a9a305c1e076fc02f416d8bd0e84643', 0, '2021-06-06 14:23:33'),
('3d5252e5d55dec7a0b042b1a4747f5abd105c0fe7450ca8afeac23f91d0c37eb0ee41d22ff233d0e', 'a110fe62d9818f321bf345e43fca9f3c0a9c53ccb2b531818ee88664b4c27c5e4390e560552161f4', 0, '2021-05-19 16:45:24'),
('3feda6b0c00fe892f8fd32e3d90d838c83af4c80a4ee57bd5a1ff0289c9d1fabe86393ac55a2b6e6', 'f2485cb50f9cdde206bcc9f8456f2db27157fc7d7ef65f09b798bf01677f254b90bf1bdd455c01c8', 0, '2021-05-19 15:32:49'),
('4280cf87aa76fd86cd9ea2c571ef0fa9a8fc571df2227b1fcfcf223ce251dc0187a77e0f0e5d7f02', '8a6051f84b0ba9812719361ba3c5c4c013bd7ae94f5559f5503a32d461f5182d9f8a762e43f0119b', 0, '2021-06-27 10:34:36'),
('49e4c1fe3d0a171b4f116a3787b80cad74e4557c9af3c3006c074173e640753d6c615b7a3a6fc46f', 'da600d8c58b6061d4516a19dcdd6beba48d031624dd5e116e3cf59948bb37ab8db4dd6ee0404221e', 0, '2021-06-05 11:39:48'),
('4b2f3fe5b08045d99066a216a4b06fd45ea66fd8bd0396ecb86666a7e54d0a257109acd023d2e0c7', '20e935135030505d6ce9c3ac1ccec4fb1cd5a2c686f571b84161e259332633d2de7d549a92f3244b', 0, '2021-06-09 14:28:47'),
('4c96e0d4216c86e7a50a0b4a3be65b39d4385dda2e2728264ecfde289ded98950bc5e63241a0c647', '49c9d6d487cd6fce957b37fe089130e5523ee75c547d6719766daebed56bd826ce8abbdd53cbfe93', 0, '2021-05-19 16:27:49'),
('4f36a1b35fded7e4b40d241b81e39b4a41c7d2299224c18037e66993e545eabcf80b8f5667264c9f', '45e13ef1dd90805e148d0e7b5552707e4bb3452c227ac5eca37c0e61131d54f50d1b4abc0f4d6680', 0, '2021-07-07 21:27:53'),
('4f7facf48e6b5e4ae632054549f387961f33413e324960eaa8d643433d6e10be09fd039c8e718084', 'ea697d647a220e1422acb3bd74bb3d84d05e2c5d91f19d0e43522775af05a535351a6daefc48c53a', 0, '2021-05-29 10:27:55'),
('5018dc1e681b38ca6497c61a354452d1bb247d9e0587da962b5a56d712926f0dd87fc6f18c7050f9', 'c46f5d9f0c8198e12a545e8be8799a3a9b832e3ed2587112a1ff3a7749ecdce330d83be0466ccef5', 0, '2021-05-19 15:19:32'),
('51d784159630a4b6b9417e9e4cb06b5fe7617a31c61327b955ae1122252bdb844a3f334958e01c8a', 'd320470cec75cf59a18fbe6b423e91b321947d32ff3e109dba282bb5e6cf40c3210e7f5df83fa71d', 0, '2021-06-05 11:40:36'),
('52b736a19789133b95a12ddf8058525e14054798295cf8ee44ebe8ff21eabef55d90c54f62956482', '91fdf96b2d735a9ec2337db627c9c55dfda8ef63b0a292040febeb0cd7125d95872798bff837076f', 0, '2021-05-29 11:36:40'),
('550ad39932f657aa41291efe5d3c32455fdcf4fb174d6cf269ab6ffb4a27cddf240b13731ce6b853', 'b08ca3493a74599dc89c64d400b085ed241ba7c9173b68b643f09cf93a95cfd5f7292e1b25037e07', 0, '2021-06-05 11:41:23'),
('56b55b225990fbfe58f0f48ea174a890b8e2c7dcc72de702019e4ba016dc36c28b28d4b161c8ce8c', 'bfda6b01b7fc27d2285a8e47ee7806793bf33e2fa824b9714451b5b3340a3095233532ae675806a5', 0, '2021-07-10 00:51:22'),
('5a409cebebbd2d7b7986cc4bcc5b6eb3e28337fd8606873ec4a7d1c990e1403285ed4c8a9eb0d66a', 'dbe972977667c7633c6e69c3c2c4aa9a416780370229bba054b437c08d121e05cf7a17a3dee4f7ff', 0, '2021-07-09 22:47:36'),
('5a9be7b24fcfd3299367cc58fa5b3099db21cf6ed8504356fc241b5a17536674557fc1927a841c0f', '3c970ad968a5299c238f89ed6c05c24c18f393b5ef853778169cdec804f60283efc236c7cd551fd9', 0, '2021-06-06 14:46:06'),
('5cb58f3529faa751005803ff14097e772c6d0719bbf4618df5a12a93be4b180126370fb60c2cdac2', '95398d7f8c99794803938ff65badf2131f0bf83f7e19deffb51c4f9a11a828c498d14912436be271', 0, '2021-05-19 16:24:28'),
('5d4dbb132fd3f992f8925e6e8bec3de69abc1f5636f4e9a7e7f49660c444874046f7940842435d32', '3e4ffbb2b43ab2d492681754ba16b6fb557f7db73243ec58212761fc96e815260be3623efdd27e88', 0, '2021-06-19 19:12:34'),
('5e6679f35783bc810ccccc7dac9088a8c28183355aa8b0c4f52dcb7749b85bbf799c21743cebc522', '0cb0f818c45f9877bbc766c2908d0518e11150c5dc4925220acab5a40b981453f5237317b9724c4e', 0, '2021-05-20 15:19:38'),
('6077e277848004287fd8823df0feec77bb185fc284cf18d61e5f7ad214d451ea03c93708f41f782a', 'e1fc6ee1c700257d8a88fcf6ebee1be5c732d74d2408782f3f695e18cf4fe4d2a66fa0a40aa4c8e6', 0, '2021-06-22 23:17:25'),
('62abcf1e3802c135f6c2812df4346f54d2e90e42c9521d46c2a1ac353e99f0ca1de456bacb1fdb97', '4f02c31c3dbcbac8b19f98d3fd0b2334d8f27fe809582265e54eb7d41001360ec4f8f01e15608d40', 0, '2021-07-10 10:49:16'),
('6405fab92adbe4f975622f1ddf0f1844063f6e8312fe4420d969c4a55b9a6a4b9a526928b2e02774', '1cdfdbc2709e82a5e52f33301f731b48a6afd1d6ba875ea5dbf36f5883f8207423678e1462ccd68b', 0, '2021-06-07 20:47:31'),
('64c96394043247c6fcb843b524812102ebe2cea2f2b61e4ccea1ee23ccb062cbf1a853af9e2703ac', 'faa548dbd491080a5dfac81de046582343ee979f58468423250c19d7c0bc11cb1f62a60dd3df1be9', 0, '2021-05-19 16:48:53'),
('657e7d32668ee2ef91f384ab296c1d7741ab42add476d6231401d8624a5649f48b7c416d0c7ad3e0', '3e0c0e466cb93b52fc5aea8c9b085390ef79b16081fbdc887b08f2d69d37a0de95d194c03c6a736e', 0, '2021-07-10 00:47:23'),
('677e521f95b753fed34efb1c7ae22d412f5ecd77166404f30cd928b161e74f9834a479a800f3ba32', '75ac67f7d7cea26265f19d62c98b4f86f297eedbeef6a6d7666b0a3611a1dc1f10164bf974145425', 0, '2021-06-08 18:53:52'),
('67b937840ae8921880340ed3c6fe29bcde1856ac128cecc9d9d9ff8929b9d4ceb2b51c2aee2223f0', 'beab95cba14418170f7b2a339701de3af59ecd438a2967949987e6a55923f1b921c2c9efd2eee832', 0, '2021-06-09 15:54:35'),
('6b77c738934ef198e7dad7a813020aa3e2c3a67a99001fed24874b9c5096736fda5c02491458e035', '2b05794ace9bf5854eeee0fc6321a82f2387662842c08e27eeb672bfd865bc0ab2f015f0954557fb', 0, '2021-05-20 16:03:43'),
('6bc47e7009297ab9e6c67e4dfcd84b0cf66d27d2d41b0ff753b3a9f4da54a0fbcf712e6c97a6ef89', '4387a01418cb253783e3cc0abb1eec2401fac1582b258b824fee126e68e05dc00078e1446463ba9a', 0, '2021-06-09 15:53:07'),
('6df58c4c50c6e05e534887942650c862104af152f8ab042af79dee29ae8f5e02a3f37fe9838553d8', '7dc38241d24a59254801bc2920f72ff1ea01990b60b6bf68130b2f7255c545692c49a1e73e9ff5f5', 0, '2021-06-14 11:30:11'),
('6f6a5bf20f255c8c373a8a97bc805be9ad041b855436cb246a04739707fe3f101a1623c3d14f4d14', '6421b0929b717280d5595ee0c5954859081262506b87eed0de5ff24cbae94f926fea37b26bddde86', 0, '2021-05-19 15:24:24'),
('709a08719930965a4da41b08f3e0a481b040473edc12a82acd0412252acfc9cf47075241a18c35ac', '19a85846039b78eba0edbb0eb31ae5b67120db57a64697675fd787b2d7ae0fe2da2b4b599ad3a17f', 0, '2021-06-15 11:20:12'),
('73b675bb6cfd1778945e67d73557f9c826faf06244858198c2d71aa55620ccbe253e247999205137', '8ac4aa5f47145433dbd959882f1c47c847c7dcc283f3380f8083e7b153f4360956eb13f8dd6348f8', 0, '2021-05-19 15:19:23'),
('7510cfa13d995d11ef303f7fd1e82ccbfcd9aa36a34e5ef31307cd73e857c775008085f6900f26b8', '5706cc7d5811ecefe9501c5745a1c9f0080f1d6370d8c30dee72eede90b00447ad56ad1e3871cd5a', 0, '2021-05-29 11:35:40'),
('7608d3515f9b886347a4cbec31c5e6332e71c0edbe894db79d8c807343abcf352321c0bb502237b9', '061d936c6df84295e13a57b44e7c1286a5a41ae4aaa09c112da05e938074020541b9f211409d75a7', 0, '2021-07-10 00:58:48'),
('7632da47d49b2eb690540f541d42a852e4c8fe710b898619ee07cc09c6c3bd63fc0d042e00f72967', '11e0927f275da6c66d5d0127eefaa2ee3243a236d9b60be960659436a99048d2699a07362b288412', 0, '2021-06-09 21:16:31'),
('793d41617d51c9cedaec7f04a0e0a08be0ebc670c091d6860bc57550ba34be50ba43c8913fe7a42f', 'dd581686cc987fd11a490c23bd108aeb29308bfd9cd170bd0dcc98adf9a66b68d503575e76fb1c65', 0, '2021-07-10 00:49:42'),
('7ae83bf8cb60198b8670c32d19bbd874ab9c601ab8538ce9beaee8a42f0c56146eb7523602f8dbbd', 'c88f6f997dd5e5aa10599c77d8e0a7d1b06b8bac34cdd8cb1d635a56b026266b2723e6dea886255f', 0, '2021-07-15 21:17:06'),
('7c92b8a5c5872088ccd11210f4784df0a40d716d875804b7c37ab5b39ff94f7f47521b410f125594', '195d9d71dd3e3d84adcbfa293ab5a67c3a1fe1301fa7db15383e99900206400ecce0d2fffb0a0afe', 0, '2021-06-19 18:32:20'),
('7edf4f8c865b0d690c7bf82d5c158215c76f06ec3401969974a9627e40a278bd54a031a59d697231', 'c2ad9e9510c29928a559a97f3061252dc0685f001af0592a339e25563857a1b6dd3ba8647e3fe3f5', 0, '2021-07-10 00:31:41'),
('80cff0b04cb1c63ae0dc3b60ea1dd34536579107ada86ae1ac71425e077e74eee8af714608530535', '0a4bc8fa4e07873c3f2eebcb5a17f3fb05eeeb48242c0fa0c35f3b1b741dde7c7120421670bdcbf0', 0, '2021-06-08 18:45:31'),
('80d8762d9f14f5e75c0f02ac05b5c71c8383f115d27305ced6003dea1b06f337d2f2048e321438c3', '7b5a183eb7b4ebce8d34183116e08ca48d5cee75ddeb215b3cebcf2a31501df54ecf4dfc13449eaf', 0, '2021-06-07 12:46:44'),
('834686631c278fa9e1d15d00a27fc4393ebaeadb21367e4607d1c2f6592513e81f724e0b0234f10e', '381a24ff8c6034384fa03c2a51fb92612b37f2336034178bd22f80c040722267d259cc9d2c0e19bf', 0, '2021-07-10 00:56:01'),
('857ecba9bd04b7cbcb493dc900d23914bd9808b0d15db3792aacc9f85b63868bba8cdd074af596e3', 'fca1d626be754036352f28acbde34c59d774fa81d0726049fd21aa24ecc904ce9925ab648d4f9173', 0, '2021-07-10 10:02:59'),
('872d23e5b3c72aec0a8987ac78e6e1d87efa6b051a0a14cc053ee49716166b4a8d4213614a882ca9', 'ecf05f1f40f9474fe86ee44e1172f394c21a81efc7c10360411034b717cb6ea08dc2bb21628567d9', 0, '2021-07-10 00:58:06'),
('8847095953e3b33f8f088a62f5022bd3dc1e9860b1bf150ebea4fa39cb7ecf7503ccabb70206e1ec', '7205a44f1c1b4fb6aa737f24f2c7878497d4d3bc10f53dba941df1e12bad91935bf0b03621af307e', 0, '2021-06-16 22:33:05'),
('8c0d41eba37664945f9878b8e415750855d5e360369a03f686a9f1fcde9520ba3e7320efb0dcab49', 'cb14ea3e09dea7fcddfd7134395fd2eda86c0820892e282887df14e810955293c06132276275539c', 0, '2021-07-10 01:00:24'),
('8c79f9186323ba0bf6cd0f2d3ba8161b3fed97d87e22bd181af5974f49e60833d501cb04ce6d57a2', 'c585f10fe30cc4d58851d503aa5dc4d4a3aade19e512b1640141f3aed560abf755bad9f7ec51ecfe', 0, '2021-06-06 14:49:54'),
('8e020feead688e3de5cdb9d7f85a68f9716c99949282033198d6c950a486f0a3214d78ce7eaf8d6e', 'd1d289866e180c6635330926a1305ab60b38bda0b7f61319df637146bff5f0d4eb77242ad43baff6', 0, '2021-06-13 23:53:35'),
('8f8205dd6ecbd23ec784309f5de12eac6863b4ff22f59ddb608b9287c3850a49c06339ce18154997', 'a1bfa4fe3cb38c9eb7f49b7f620f3d8167e3fcbac47638cd646e34da315b0908acf274277124fdc5', 0, '2021-07-10 10:12:24'),
('90a96c6d0f748df4095bc4d5fc8f0661ce148d8c61befd9e3d0d108b5023160c70f51cf22abcd9df', '4eb96e7b2662a8a0e61addd076fa714d31293979e507b308b685f7133e5d3621bb0f6f7b3df331aa', 0, '2021-06-05 11:41:54'),
('912c1e0b356726a9aa0963ef231aff42a8ed2b8a2bffeff093878d079c76bf515a7d917ec01ef8bd', '944673b861fde32401ed44a9e7ffb5386577ac8974ea43350b2ad1258936510ae7989de6afaceb52', 0, '2021-06-06 19:36:23'),
('92ac53e9b230491a0ec46283c4595b014aac3bb5c1d18a93467904081e9bc08eb18af28ef3a8943a', '3c82a90333edc6470c6b3fe09e3a71659d25bbfb7beb69696069e88f085f54d5d25d7924d766fe06', 0, '2021-05-19 15:25:31'),
('97a5a4204e63cc54a784d7d3075f14f18c34d95c75db81da74bf5a54394caae45c484396b812ec79', '5f445f05ffe4fd1bc0b87de33bff7dca1911b6af1948df76c421f8b81dca649f273d9bc29f377ab7', 0, '2021-06-19 19:23:19'),
('99bee4a977e3e0fd6645776eb3e6aa8821a3efa6508b3c295539a00e7576ea9015849b8f2b66c866', '95eed2df0f1bbc1fb790280c4ac5bacf549a2733ec51d4012908f0e1b39d372959204b91224f93c7', 0, '2021-07-11 01:15:42'),
('9b64d9411517ddc2c2048c9358eefae7009d968ecd2b3735ec2488af432628cb1cb57d0c1ad569d3', '5b62141e3724b1d19eb49f096a2216454cc862dc2bf05db7ca91793b808204be7975f4dfe7cb10ba', 0, '2021-07-07 20:03:52'),
('9cd8955cebd436eeeae5f817e9f671756f693dbf97b6db8e03f768946c3d19e39464079675777089', '7aa549ab99fb3d80900a91a8fdd6e302e4cfb8ac987a754c3d0b7f4deba16b54bf66d507ed0fc413', 0, '2021-06-05 11:14:44'),
('9fa6870235d1b10ce0f467f212502c4c68349d499344aff8bacbb0dab863407f29faedd6b7faa41f', 'e66447f313b62593f9b0f9f2b1cc665bb9f60b2b9f9d5562af3a259cd28357c4165c3852ceb78f86', 0, '2021-07-10 00:32:41'),
('a14f3cb549a1d5ffe57ae4dbafdfb645be1c7b0d9b3c6ea16ca1d66b993ea5f0297357264d140d09', '77decd0c3a7a48eb5d1aeefa512f4f59130bc6fe779d9ffaecf595b647c21d0c9a8f513384ac6e86', 0, '2021-06-05 10:41:29'),
('a388a1ca2ac3e92218fbd3a821643cb28f4dfe1b0e1d903a431ae3882a3f591ecb0efa77f451d72d', '6b97855b6a6dbbcdb0964677504f9815b99611a955647ca69a8c983ad5b1451ab1188005b1a0499a', 0, '2021-06-07 19:46:49'),
('a73db087992259fc5f1ac0c371c71abb51962af57d455b8ef877431c8aeb641c6001c27d6acb0447', '2d5cc449b664134f23afa855db389c90a2b3a945b3cfa2cd1d2b7424ad93a6dc1c13dde08c0eac9b', 0, '2021-06-20 21:58:45'),
('abd871d0ef415a5e8a9e28c7048bcd4825ddc4e7f41d1140bddd8edbb159c91d0e9a556865427516', '289c66be3d98c661d5c9c349f15a2b8ef3d6ea179543baf41ac00a5b66a15d150a79c3f1d592905b', 0, '2021-06-23 01:08:33'),
('ac775fb4f893a34073e46b8d74b084fb0287bef03c761949c63ab0e944c73792d67ba5e157844149', '68de8e2ba2075cb1f9f8fa3ec46d68e03b67ade8a0814454bbe84fa3aeccabab697e836db6ce6b02', 0, '2021-05-19 16:46:01'),
('ac8319b931ed1d64dccb43015d584021041952bba8dd71489aebf85fb5754f9ad651f1d79324021b', 'c35323ccb84e5f140648a79680e236d3aff008d95fe8b94ebf58c36d65ab2134306efca4b8d0872f', 0, '2021-06-06 14:28:26'),
('aeecd9437dc181cdaf17ac4b9a84f37b19def267c2d2df5f49387fe695dd155d9032ce3052ba68b5', '4fece2eb5046ac8188cee175dcc6793b910e5d698bee283c57032ca56b2a5884a0c7276c0c200181', 0, '2021-06-05 11:19:21'),
('afd7e2cd6a6949e71d74115cc32fe414b0283ecc59c0b87fa88de13b27a9f4f3d9bbeeb64a1edcfe', 'a6c2479deca45b71ca2bf32b29e83175292a1df40187ba9b62f15d96b07b56e298ff773bdf8e715b', 0, '2021-07-10 00:35:19'),
('afeb5243f5ae377abda1f1a09cf983867d61d4c2b5b344d655a6d8d320679aa2c817627a3e8d5627', '3c0b04449fee053356d77b1a1c8808803455b936db0295755af402e2a5e9f35f1d717c3b097ab451', 0, '2021-06-09 15:55:39'),
('b184537582152328bae106a2a5af62a668740b7712b99dd17618ae580108654237b993e4a0992087', 'f35593226d4e5157a93826391d3b586322e4a5ec19015f2102618659509dc556a808cb17bf829673', 0, '2021-06-07 20:57:34'),
('b1b924c2d780181b8c65f79adf3491491faa80840c5aa0624c4f262ea7a00fd2a5523c0e28297db8', '0545e8847ce1106ab5416ce4ed1ad51a951db9840f0ffbf455b7e64471e9c21640ba9513c15ec1e8', 0, '2021-06-09 15:55:04'),
('b5040c9362f3de448a766a1036c2c436f3d351835c0064f997354ef146fa7fd626e7900031535331', 'b5285e7c9a3644bddee057d2f71b5d3e3ccd2f9a8dd828e0937562f7e01eed90a67b175a0d2ae5b4', 0, '2021-05-19 15:20:11'),
('b50cbccb04548ed4dbb52cb7b5f6648e2b9b4f8ac473a03ca10826e7700e255ef3deb8930a1a57dc', '88ed8b005037840ff22299226a19b9f58afc8d0979b6d7a7a85aa23dee0914c271781d26de9ea2fa', 0, '2021-06-10 18:59:14'),
('b5762deab5c861f0382199dadc965eeca479fdb39ec5b145dcbdcda09940e128a8c71f56df215cfa', '6544567641df4c73622325485ade99ecb2fb7ca4b99d57297dd3be13d4af9e13e71517750c636a1a', 0, '2021-06-13 23:55:16'),
('b8cac56347e428ef24c66e2a3abc10e40430e1d4b300ef90345254088e5b7737cec86d64172c7055', 'f1db242a023204c83a6ea4242df383c8d538dfd2d5009d74d6fddae3f3d045ec07af4431fb7fefaa', 0, '2021-06-26 18:44:26'),
('ba153fe3b18da06f069aa1fb7fd9a0507a8fe7d004e5e13dc5eb9e331c331044c9ff1a7be4a2dcec', '57397f67d4f035489f02f82d899d3b642115f9a878f3be16c4088739bb7cea8c43814865ab5ccb8d', 0, '2021-06-30 18:50:33'),
('bb4d0863cb3cd4fded22fd96badea7b6ed3058ed00375b78b0c45e50a992801a15c397b323bab97a', '30e164c766735cd7e2114856249534591258e5052961530e521cd2fdc656c4b5ed6d0e2e96a9d9f3', 0, '2021-06-05 10:38:18'),
('bbc17a4bf6e379fd0f3ef1d4f8978aceaf27cf620590156973080e586d7ed91c6619a51c17dac29e', '94688628c8186cc1ab50a25c41ec7b13cbc8f0949a2db6c0e245b135965c96315b6d927398310441', 0, '2021-07-10 10:49:54'),
('bc440061ec729709b28f0d782785a6e78df2a11d4d93ec37d62f94524792fb5d8c3db4281ba70ba0', '6cae2b819f1496cf1fd010223fc9c3e442fe35d5a8ea0743d296247dcb7d2110e48105e05519225e', 0, '2021-07-04 18:04:14'),
('bc529c1eb641d487badb238992f65a595d08cf0c1f979386c700dc24b8ebe6360493348539d1a8d9', '390ff372599d7581396677ab90f9f49f07aafdeebb8c1d242944cacb309dc2ff71d91d4511a7ebbc', 0, '2021-07-07 21:46:21'),
('bd23ec9f0a794bb77732cf731b32b7165f9728ca88a3423d8ababf8aa30e8daad87d1508a7320df2', 'c311af1a92df73a34726c347331edbfed1c1471884b7b0e8de90135ab69c82265217d0e37acf7b62', 0, '2021-06-13 11:18:19'),
('be092436dfabd503f513a692b65361f3baec87645b6984a2c638a4e69a5c1cd870d8689de3ce46fc', '3ba5c1df464d7cdba16c6c002a34fd5492793a84806865f21879d44c390902a5902ab176faf2f29d', 0, '2021-06-23 09:18:25'),
('c1ede7a4b13cbd820b3495e2306e5259aaf80dcaac9a866219635f6d40e306804021bf53a27ce63a', 'cba330a328622fe8dfc555a81805d7ae304e6743f1de632892ce3140597a3703ebd080e05a381fb5', 0, '2021-07-03 22:04:54'),
('c277ad2861505988ed35dd80bf59293433fdc7a763faa12cdb69af07e2f8c02d18050211fe1d4920', '0b5f63b775a92b23b2f62e6f022db8bcacfcc536e0ffc17a8f33e706c04da26db47ba801e67157b3', 0, '2021-06-09 19:46:54'),
('c2e2571ad6e0cc6c5d7a1dc3606448d2ce822d1fd1791980316633a4fae5eec8e4f8fc10532565cc', 'a342b5ff76d407ea0b103583d2c9ade7de1064aaf9036ebadceaa48cf229f40efc36fe3f62f625a4', 0, '2021-05-19 15:19:19'),
('c4f362a0a9999e91832dfaa2035159b9b8721fbf1f6b2f1e39b35bd775befebe6407a7272c09cde0', 'a9e8a5a79dbcf3a706bbfd3a46f49fdeff107e9e1c0d8b4660e878732f031d49461e68617ef1e095', 0, '2021-05-19 16:23:55'),
('ca8610aff5724e5a684760e1d4adb118c20d130ab0003d977f0ca3a8c3bbfd894d572a4c4ab41728', 'b6d1a2b155c24a03285e9a58cd8b93731ab3c6f7eb8f1bddbd103b8741988ea2b8985741803b6900', 0, '2021-06-05 10:39:07'),
('cb6954ef319431e11c96b22604f686f5766d2233cd65b66341e8066ba4d01c2e6e32d72108928247', 'd6383e2152acfc8190cf7b7995b725a53b6a520522f2f982353c9001e4d960dcde087edb43c820a6', 0, '2021-06-09 15:50:44'),
('cc8ea5dc8d1511daa76d5db5d10dc592fd0bb7ff2a4329399a0d70237651ab2443ffc18ac39dbfbe', '36ab8bea414e775b15b1643c84deaf20c7100238748f45b472a518c301816976a376205354268207', 0, '2021-07-10 00:55:40'),
('cdc3ef8fc6f1aa39b3e6c3aa8bb7ad6eabaac00ad07fd40910098e9d73b0708262ccbfea4f21ca77', '4bd15f7b64c05e0edf9445e0ff748c0ed1d532ec68eb740f76b24edc75f548651c059ac1407bd126', 0, '2021-06-06 14:42:34'),
('d040d685d6d06a453d6564b519758968bff87555f6943aa2468a1ac4b562c937a741a1f0b0e0bac5', 'fc33839fd8a7b736c5ac68ec7629f11e6bc92d4553dc681aa45def46ebc1c1d83f075b2d3144b6c9', 0, '2021-06-07 18:50:52'),
('d5263b266231f12182a2c6c22e681b0037ab42b906c71314a999edfb2b461cf02ada39cce8593a87', 'a404dfd6fb679cc191947ff584d33a666b78d31178e18784a3908142d5eb1e0d131ba38450f3bbef', 0, '2021-05-29 11:38:38'),
('de2e16bd52c8ec3c122d9e4374ce21d72dbc30e1306e410ff82823d43dfc42e9b663a69f4715976c', '8d42125183eb65e1b6fd65dd7fe76be348f5e027410cdd0f233b2b2ee49c9c2b942cff8c9db442f1', 0, '2021-07-10 00:33:02'),
('e0f809923482ed8d454f2b3cfb2214f6426c0c55f6c15d580ea89961804400dd0dfb63a511f35d3a', '979bbcdb98b9e9f5478a177a78acd9df5fabdb9fe1a124298350090c05fb52dc756b2f6571ab8c82', 0, '2021-07-04 11:50:43'),
('e12640a8122dc574fd237189e9403f928556fe9bbe54b8cff874f1cc0d89e2fb7aa4fe6086461b1d', '82fd9eb1fb1d9df8e02170321210b2a500b3efc1a68fd7ed067eff518c1110a0b693b7fc380465e7', 0, '2021-07-09 22:55:39'),
('e1f6d1eaf470b8c28d8c08aff2e17eb3cefb01d1619402edcc57668888fe09a7e9240a59985d202d', 'b1053bac7237b4ad6a106db508871856aa045ef1a38c4e87fc62a1040e6e04109ba413b2fde33be2', 0, '2021-06-07 18:38:19'),
('e2f77cc3a87378216f6aae60cf5a86feb0ade73ae6cb60117ab7312376568dfbc301c07599c5f854', '7e4d516d87e6db48cc8f36c582e4f81a72748afff74c152a9652f738d780e9fd42c3661ed0230fc3', 0, '2021-07-04 21:08:03'),
('e4c7ca542fd0de74e4d969e6f9298f1c6f6498ddfb1ec6bbd0bba4653941ba1fa25d7b4e43276ab5', '6901014f9281bd92b5ac9b0c1fc1617cbc2419abe091000d6c7a85882a15fa0a844ca72cf0e920a4', 0, '2021-06-05 10:39:54'),
('e4fd05bac4a66e432a3b71812e61025f5eb3f451c8edc06a74882e6304bf418c2e792b55201f02d4', '07dfccbe2b6350c947f123b40eefe9e6737ae8c1c846b37471e7d60b51fde98a689e7911c92f3bb2', 0, '2021-06-17 20:57:42'),
('e6ae1381cb3cf3afbc9f57ce85b8b07b165a63139e802434af6e79e116db0fab351ce128fe7eda99', 'c708bf4195ee7104917e93dfeb44f16d32d2c08292c5ed681e04b185e2b8c2dfb65840da70c63caf', 0, '2021-07-03 15:34:07'),
('e8a7731b2901fe74c2bac04e408ec1f58f7a13d3b402643a7461e76e8520360bf1dd2fa70dcea249', '79862d4af2e3be942008bab51017fc5856e2578f2efbf9263020bdff932e3e25d582a70499e90668', 0, '2021-06-09 16:19:21'),
('e95f620777ba37ec6128fd9049693564183e07f1bbd8c7b61f9bfa95df6bd658a9b6996bfdbfbeba', '7846098b815af8d7848e10e2810a21c8f958ff51b16a6e060dc9c8babfdb71c860e66cfa35d773fc', 0, '2021-07-10 10:48:37'),
('ead1bb89e6c5476e69764c8256518d2fc2d71c32da45e9f82413a561f6a9b8e222a26d72e76873df', 'dffccd88e85921fe5fd97ba75d970713519ebf5425d9af079e2556f311d0fff40641a3feb32be5a4', 0, '2021-06-23 01:39:22'),
('ee46290efab42a6a46f846f70ecff24aada8a28090df83a709f34265ab11366c926a0646d1ac944f', '425b1d1e87d08778d6a560bf057a634a00450475bf52ad57264cc206df7944ec2059670f54be14cd', 0, '2021-05-19 15:18:26'),
('eeac03d4e04a866fb4b9d9735c15aa111ee2ad67dd73d43ce27c69a4a124943a2afcd1e5ad2e14e4', 'ac02b5d5b1376328dcea817bcfb377c614967784956f6035ddc7d0ee916bc4cdad9f87b7f9be597f', 0, '2021-07-10 00:56:56'),
('eff440069208978c742ab3434a11e5a6e0556e9a29c4e732a62af9d51596b181b1f0f9d9d1d7a2b4', '26297f6e803c2f14d7d2791b467486132ece957be0c7d5e5dddeef3ae140a73e1d20d09aba21232f', 0, '2021-05-29 11:40:15'),
('f43e103b1937efde1eb5b54632ac81c763a3b8f208d19edcb1d51f1754b77bb53617d8f18481783f', '7382d1a7436b3035a1918f6ac5dc22dba4270f9ac3a1ee660fd496da87631ce7d7ee6253ea1d9f96', 0, '2021-06-12 19:03:24'),
('f83b90a02d195ffe54f33f3af0a295909350629e2a6c887ac6a285c538f06b2cc0efd37623cc067d', '18cd0bea86790c755396a192c313d4061bf99b6d2c327ea97f01430a4c53f76bc05835edfa83502d', 0, '2021-07-10 00:30:50'),
('fa3887add9d0d38a79b5f2105a3a449e64006ff688cf5b520600e59d02d6f919d2020b628c0e0f06', 'bb0200e00ef67226827f652a5deab16de7408c56df00cf94892dbd93735945d903578deddbb457fa', 0, '2021-07-09 22:47:15'),
('fca071adca5e1f88398fbc4af6aba12fa04aa001433f6de86c1fe24510b3202215672c41c005d708', 'de11535e0d2a1574bcd3adf81715fc81f0f19ee97ec059a02f6140a3621a85957633610df75bb062', 0, '2021-07-04 11:50:01'),
('feaadf6547d3f51a67a39cd4eebc276f697b934d033124dbd4aef939183c40c4d79a558470215b9f', 'bd9496afaeec88654c154e5f152166cba8f39f9a20738879bbcf250e0c5fbe8ed0a212b1d40225c3', 0, '2021-07-04 18:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_tarif`
--

CREATE TABLE `pengajuan_tarif` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `status_verifikasi` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengajuan_ke` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE `pengiriman` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_resi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengangkutan` enum('kilo','koli','dimensional') COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat` int(11) DEFAULT NULL,
  `dimensi` int(11) DEFAULT NULL,
  `total_unit` int(11) DEFAULT NULL,
  `jenis_pengiriman` enum('regular','express') COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota_asal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi_tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota_tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan_tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelurahan_tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos_tujuan` int(11) NOT NULL,
  `alamat_tujuan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `nama_pengirim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penerima` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_pengirim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_penerima` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarif` int(11) NOT NULL,
  `jumlah_hari_telat` int(11) DEFAULT NULL,
  `verifikasi` enum('pending','decline','accepted') COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `perkiraan_tanggal_sampai` date DEFAULT NULL,
  `tanggal_sampai` date DEFAULT NULL,
  `id_kelompok_pengiriman` int(11) DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengiriman`
--

INSERT INTO `pengiriman` (`id`, `no_resi`, `jenis_pengangkutan`, `jenis_barang`, `nama_barang`, `berat`, `dimensi`, `total_unit`, `jenis_pengiriman`, `kota_asal`, `provinsi_tujuan`, `kota_tujuan`, `kecamatan_tujuan`, `kelurahan_tujuan`, `kode_pos_tujuan`, `alamat_tujuan`, `id_member`, `nama_pengirim`, `nama_penerima`, `no_telp_pengirim`, `no_telp_penerima`, `tarif`, `jumlah_hari_telat`, `verifikasi`, `keterangan`, `id_perusahaan`, `id_cabang`, `perkiraan_tanggal_sampai`, `tanggal_sampai`, `id_kelompok_pengiriman`, `longitude`, `latitude`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'awey123', 'kilo', 'makanan', 'sambal', 1, NULL, NULL, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'MANDALAJATI', 'KARANG PAMULANG', 40194, 'JL. PASIR IMPUN', NULL, 'naufal', 'ryan', '081234567890', '081234567890', 12500, NULL, 'pending', NULL, 30, 1, NULL, NULL, 12, '107.67654918647', '-6.8965202005246', NULL, NULL, NULL),
(2, 'berlin123', 'kilo', 'makanan', 'buah', 15, NULL, NULL, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'TASIKMALAYA', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'ederson', 'moraes', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(3, 'bogota123', 'koli', 'kendaraan', 'motor', NULL, NULL, 7, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BEKASI', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'yameric', 'laporta', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(4, 'tokyo123', 'koli', 'kendaraan', 'motor', NULL, NULL, 5, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'SARIJADI', 'SUKASARI', 40151, 'JL. SARI ASIH', NULL, 'john', 'stones', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 30, 1, NULL, NULL, 12, '107.57963682184', '-6.8755887376771', NULL, NULL, NULL),
(5, 'helsinki123', 'koli', 'kendaraan', 'motor', NULL, NULL, 2, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'TASIKMALAYA', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'kyle', 'walker', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(6, 'oslo123', 'kilo', 'makanan', 'cireng', 11, NULL, NULL, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'benjamin', 'mendy', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(7, 'rio123', 'kilo', 'makanan', 'eksrim', 16, NULL, NULL, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'rodrigo', 'moreno', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(8, 'kevin123', 'koli', 'kendaraan', 'motor', NULL, NULL, 8, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'TASIKMALAYA', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'kevin', 'bruyne', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(9, 'silva123', 'dimensional', 'cargo', 'bomb', NULL, 10, NULL, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BEKASI', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'david', 'silva', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(10, 'raheem123', 'kilo', 'makanan', 'cilor', 7, NULL, NULL, 'regular', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'raheem', 'sterling', '081234567890', '081234567890', 12500, NULL, 'pending', NULL, 30, 1, NULL, NULL, 12, '107.63433946154', '-6.9081899817026', NULL, NULL, NULL),
(11, 'mahrez123', 'koli', 'kendaraan', 'motor', NULL, NULL, 6, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'TASIKMALAYA', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'riyad', 'mahrez', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL),
(12, 'kun123', 'kilo', 'makanan', 'kopi', 10, NULL, NULL, 'express', 'KABUPATEN PELALAWAN', 'JAWA BARAT', 'BANDUNG', 'CIBEUNYING KIDUL', 'CICADAS', 40121, 'sukarasa', NULL, 'kun', 'aguero', '081234567890', '081234567890', 20000, NULL, 'pending', NULL, 29, 1, NULL, NULL, 0, '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id`, `nama_perusahaan`, `alamat`, `email`, `no_telp`, `logo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(29, 'Codeffee', 'bandung dong', 'codeffe@gmail.com', '2345', 'rn.png', '2020-05-19 15:05:04', '2020-05-19 15:05:04', NULL),
(30, 'Burhan Aji', 'Bandung Timur Cibiru', 'burhanaji@gmail.com', '0987654321', 'xx.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regional`
--

CREATE TABLE `regional` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regional`
--

INSERT INTO `regional` (`id`, `id_perusahaan`, `provinsi`, `kota`, `alamat`, `no_telp`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 30, 'aa', 'aa', 'aa', '', '0', NULL, NULL, NULL),
(17, 29, '11', '1101', 'aaw', '', '0', '2020-06-06 23:40:06', '2020-06-07 10:35:48', '2020-06-07 10:35:48'),
(18, 29, 'ACEH', 'KABUPATEN PESISIR SELATAN', 'asd', '', '0', '2020-06-07 10:32:17', '2020-06-07 10:32:17', NULL),
(19, 30, 'RIAU', 'KABUPATEN PELALAWAN', 'cinta', '12345', 'pelalawan@gmail', '2020-06-07 10:34:06', '2020-06-20 22:37:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report_mitra`
--

CREATE TABLE `report_mitra` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `id_pengiriman` int(11) NOT NULL,
  `jenis_masalah` enum('Telat','Masalah Kendaraan','Kecelakaan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `report_mitra`
--

INSERT INTO `report_mitra` (`id`, `id_perusahaan`, `id_mitra`, `id_pengiriman`, `jenis_masalah`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL),
(3, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL),
(4, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL),
(5, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL),
(6, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL),
(7, 29, 1, 1, 'Telat', 'sdf', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_pengiriman`
--

CREATE TABLE `status_pengiriman` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pengiriman` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `asal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengiriman` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moda` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pengangkutan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarif_normal` int(11) NOT NULL,
  `durasi` int(11) NOT NULL,
  `minimal_berat` int(11) DEFAULT NULL,
  `maksimal_berat` int(11) DEFAULT NULL,
  `minimal_jumlah` int(11) DEFAULT NULL,
  `maksimal_jumlah` int(11) DEFAULT NULL,
  `minimal_dimensi` int(11) DEFAULT NULL,
  `maksimal_dimensi` int(11) DEFAULT NULL,
  `status_verifikasi` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `keterangan_penolakan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id`, `id_perusahaan`, `id_mitra`, `asal`, `tujuan`, `jenis_pengiriman`, `moda`, `jenis_pengangkutan`, `nama_barang`, `tarif_normal`, `durasi`, `minimal_berat`, `maksimal_berat`, `minimal_jumlah`, `maksimal_jumlah`, `minimal_dimensi`, `maksimal_dimensi`, `status_verifikasi`, `keterangan_penolakan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 29, 1, 'KABUPATEN PELALAWAN', 'BANDUNG', 'regular', 'darat', 'kilo', NULL, 3100, 2, 20, 30, NULL, NULL, NULL, NULL, '1', NULL, NULL, '2020-06-17 00:23:24', NULL),
(2, 29, 2, 'KABUPATEN PELALAWAN', 'BANDUNG', 'regular', 'darat', 'kilo', NULL, 1400, 2, 5, 30, NULL, NULL, NULL, NULL, '1', '', NULL, NULL, NULL),
(4, 29, 1, 'KABUPATEN PELALAWAN', 'BANDUNG', 'regular', 'darat', 'koli', NULL, 3000, 1, NULL, NULL, 1, 5, NULL, NULL, '1', '', NULL, NULL, NULL),
(5, 1, 2, 'Bandung', 'jakarta', 'regular', 'darat', 'kilo', NULL, 12500, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2', '', '2020-07-10 10:30:33', '2020-07-10 10:30:33', NULL),
(6, 29, 4, 'Bandung', 'Jakarta', 'reguler', 'darat', 'kilo', 'asd', 12500, 3, 1, 8, 1, 10, 1, 1, '2', 'Harga yang ditawarkan terlalu mahal per barangnya', '2020-07-10 11:25:20', '2020-07-11 02:01:38', '2020-07-11 02:01:38'),
(7, 30, 4, 'Bandung', 'Jakarta', 'reguler', 'darat', 'kilo', '', 12500, 3, 3, 8, 1, 10, 0, 0, '1', '', '2020-07-10 22:45:16', '2020-07-10 22:45:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tarif_pelanggan`
--

CREATE TABLE `tarif_pelanggan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `asal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tarif_pelanggan`
--

INSERT INTO `tarif_pelanggan` (`id`, `id_perusahaan`, `asal`, `tujuan`, `tarif`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 29, 'KABUPATEN KEPULAUAN SERIBU', 'KABUPATEN CIANJUR', 3000, '2020-06-14 12:05:39', '2020-06-14 13:19:54', '2020-06-14 13:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_perusahaan` int(11) DEFAULT NULL,
  `id_regional` int(11) DEFAULT NULL,
  `id_cabang` int(11) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verifikasi` int(11) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `id_perusahaan`, `id_regional`, `id_cabang`, `role`, `verifikasi`, `email_verified_at`, `email_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26, 'poisonjoker6@gmail.com', '$2y$10$VUfBnHiy6XzVXdz//lwcnOG1b8RcWyksLaNiOad3m9VdALE9KCZH6', 29, 19, NULL, 'ceo', 1, '2020-05-20 16:03:25', 'Wyx5Rf6a9oQAvxFvmbprZbbS1f01vrLJ', '2020-05-19 15:05:04', '2020-06-06 17:03:01', NULL),
(37, 'anggy@gmail.com', '$2y$10$VUfBnHiy6XzVXdz//lwcnOG1b8RcWyksLaNiOad3m9VdALE9KCZH6', 29, 19, NULL, 'regional', 2, '2020-05-20 16:03:25', 'Wyx5Rf6a9oQAvxFvmbprZbbS1f01vrLJ', '2020-06-07 21:31:04', '2020-06-07 22:35:13', NULL),
(44, 'reghinaputria@gmail.com', '$2y$10$ctTp1ykVE1.4JX.AqYBNvu4OhLPvKxZF6nBI/z1kg4UwmpvTq15s.', 29, 19, 1, 'cabang', 2, NULL, NULL, '2020-06-12 20:58:33', '2020-06-12 20:58:33', NULL),
(45, 'admin@sukarasa.coma', '$2y$10$Umr383GfPDEGAB.Gm0fY0eVQWGrXxI.Mqtl8ZCCXuuXtazRTIA71.', 29, 19, 2, 'cabang', 2, NULL, NULL, '2020-06-12 21:05:39', '2020-06-12 21:53:17', '2020-06-12 21:53:17'),
(47, 'kurir1@gmail.com', '$2y$10$VUfBnHiy6XzVXdz//lwcnOG1b8RcWyksLaNiOad3m9VdALE9KCZH6', NULL, NULL, NULL, 'mitra', 2, NULL, NULL, NULL, NULL, NULL),
(48, 'kurir2gmail.com', '$2y$10$VUfBnHiy6XzVXdz//lwcnOG1b8RcWyksLaNiOad3m9VdALE9KCZH6', NULL, NULL, NULL, 'mitra', 2, NULL, NULL, NULL, NULL, NULL),
(50, 'frhnmln20@gmail.com', '$2y$10$MHneycldx0p6iii8z01EWuwd1Ot4PWUz4oysVcjvN66IMpi8dyFrS', NULL, NULL, NULL, 'mitra', 1, NULL, NULL, '2020-07-09 22:47:06', '2020-07-09 22:47:06', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biodata`
--
ALTER TABLE `biodata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pengajuan_tarif`
--
ALTER TABLE `detail_pengajuan_tarif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelompok_pengiriman`
--
ALTER TABLE `kelompok_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lobi_mitra`
--
ALTER TABLE `lobi_mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lobi_pengiriman`
--
ALTER TABLE `lobi_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitra`
--
ALTER TABLE `mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pengajuan_tarif`
--
ALTER TABLE `pengajuan_tarif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_mitra`
--
ALTER TABLE `report_mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pengiriman`
--
ALTER TABLE `status_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tarif_pelanggan`
--
ALTER TABLE `tarif_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biodata`
--
ALTER TABLE `biodata`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_pengajuan_tarif`
--
ALTER TABLE `detail_pengajuan_tarif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelompok_pengiriman`
--
ALTER TABLE `kelompok_pengiriman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lobi_mitra`
--
ALTER TABLE `lobi_mitra`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lobi_pengiriman`
--
ALTER TABLE `lobi_pengiriman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `mitra`
--
ALTER TABLE `mitra`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengajuan_tarif`
--
ALTER TABLE `pengajuan_tarif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengiriman`
--
ALTER TABLE `pengiriman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `regional`
--
ALTER TABLE `regional`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `report_mitra`
--
ALTER TABLE `report_mitra`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status_pengiriman`
--
ALTER TABLE `status_pengiriman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tarif_pelanggan`
--
ALTER TABLE `tarif_pelanggan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
