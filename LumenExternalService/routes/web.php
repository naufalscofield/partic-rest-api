<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/mitra', 'MitraController@store');
$router->get('/mitra', 'MitraController@get');
$router->get('/getKendaraan/{id}', 'MitraController@getKendaraan');

$router->get('/mitra/{id}', 'MitraController@show');

$router->get('/mitra/getMitraByIdUser/{idUser}', 'MitraController@showByIdUser');

$router->get('/mitra/getMitraByKota/{asal}/{tujuan}', 'MitraController@getMitraByKota');

$router->get('/mitra/getReportMitra/{idPerusahaan}', 'MitraController@getReportMitra');

$router->put('/mitra/verifikasiMitra/{idMitra}', 'MitraController@verifikasiMitra');


$router->get('/member/getMemberByIdPerusahaan/{idPerusahaan}/{no}', 'MemberController@getByIdPerusahaan');
$router->post('/member', 'MemberController@store');
$router->delete('/member/delete/{id}', 'MemberController@delete');
$router->put('/member/verifikasi/{id}', 'MemberController@verifikasi');

$router->get('/getMemberByIdUsers/{id}', 'MemberController@getMemberByIdUsers');
$router->get('/getPengirimanByIdUsers/{id}', 'MemberController@getPengirimanByIdUsers');
$router->post('/pengajuanMember', 'MemberController@pengajuanMember');
$router->put('/updateStatusMitra/{id}', 'MitraController@updateStatusMitra');
$router->get('/getBbmByIdBbm/{id}', 'MitraController@getBbmByIdBbm');
$router->put('/updateKendaraanMitra/{id}', 'MitraController@updateKendaraanMitra');
$router->get('/getBbm', 'MitraController@getBbm');



