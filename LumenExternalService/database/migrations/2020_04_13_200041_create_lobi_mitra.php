<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLobiMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lobi_mitra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_perusahaan');
            $table->integer('id_mitra');
            $table->integer('id_tarif');
            $table->integer('berat_total');
            $table->integer('dimensi_total');
            $table->integer('unit_total');
            $table->integer('id_kelompok_pengiriman');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lobi_mitra');
    }
}
