<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function verifikasi($id)
    {
        $member = Member::find($id);

        $member->verifikasi = 1;
        $member->save();

        $resp = [
            'status' => 200,
            'dataMember' => $member
        ];

        return response()->json($resp, 200);
    }

    public function store(Request $request)
    {        
        $member = new Member;
            $member->id_user         = $request->id_user;
            $member->id_perusahaan   = $request->id_perusahaan;
            $member->verifikasi      = $request->verifikasi;
        $member->save();

        return response()->json($member,200);
    }

    public function pengajuanMember(Request $request)
    {        
        
        $member = new Member;
            $member->id_user         = $request->id_user;
            $member->id_perusahaan   = $request->id_perusahaan;
            $member->verifikasi      = 2;
        $member->save();

        return response()->json($request->all(),200);
    }

    public function getByIdPerusahaan(Request $request, $idPerusahaan, $no)
    {
        // return $idPerusahaan;
        // return response()->json($idPerusahaan,200);

        if ($no == "0")
        {
            $getMember = Member::where('id_perusahaan', $idPerusahaan)->get();
        } else if ($no == "1")
        {
            $getMember = DB::table('member')
                        ->where('id_perusahaan', $idPerusahaan)
                        ->where('verifikasi', 1)
                        ->get();
        } else if ($no == "2")
        {
            $getMember = DB::table('member')
                        ->where('id_perusahaan', $idPerusahaan)
                        ->where('verifikasi', 2)
                        ->get();
        }

        return response()->json($getMember,200);
    }

    public function getMemberByIdUsers($id)
    {
        // return response()->json($id);
        $getDataMember = DB::table('member')
                    ->where('member.id_user', $id)
                    ->join('perusahaan', 'perusahaan.id', 'member.id_perusahaan')
                    ->select('member.*', 'perusahaan.nama_perusahaan as nama_perusahaan_tampilan', 'perusahaan.no_telp as no_telp_tampilan')
                    ->get();
        return response()->json($getDataMember,200);
    }

    public function getPengirimanByIdUsers($id)
    {
        $getMember = DB::table('member')
                    ->where('member.id_user', $id)
                    // ->join('perusahaan', 'perusahaan.id', 'member.id_perusahaan')
                    ->get();

        $resp = [];
        foreach ($getMember as $key) {
            $getPengirimanMember = DB::table('pengiriman')
                    ->where('id_member', $key->id)
                    ->where('tanggal_sampai', null)
                    ->join('perusahaan', 'perusahaan.id', 'pengiriman.id_perusahaan')
                    ->join('cabang', 'cabang.id', 'pengiriman.id_cabang')
                    ->get();
                    
                    foreach ($getPengirimanMember as $key2) {
                        array_push($resp, $key2);
                    }
        }
        return response()->json($resp,200);
    }

    public function delete($id)
    {
        // return json_decode($id);
        $delete = Member::find($id);
        // return response()->json($delete);
        $delete->delete();
        return $this->successResponse($delete,200);
    }



}
