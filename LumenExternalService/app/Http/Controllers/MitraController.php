<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Mitra;
use App\Models\ReportMitra;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MitraController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get()
    {
        $getMitra = Mitra::all();
        return response()->json($getMitra);
    }

    public function show($id)
    {
        $mitra = Mitra::find($id);

        $user   = DB::table('users')
                    ->where('id', $mitra->id_user)
                    ->first();
        $biodata= DB::table('biodata')
                    ->where('id_user', $mitra->id_user)
                    ->first();
        $data = [
            'mitra' => $mitra,
            'user' => $user,
            'biodata' => $biodata,
        ];

        return response()->json($data);
    }

    public function showByIdUser($idUser)
    {

        $mitra  = Mitra::where('id_user', $idUser)->first();

        return response()->json($mitra,200);
    }

    public function store(Request $request)
    {        
        // $regional = Regional::create($request->all());
        $mitra = new Mitra;

            $mitra->id_user           = $request->id_user;
            $mitra->jenis_kendaraan   = $request->jenis_kendaraan;
            $mitra->nama_kendaraan    = $request->nama_kendaraan;
            $mitra->plat_kendaraan    = $request->plat_kendaraan;
            $mitra->tahun_kendaraan   = $request->tahun_kendaraan;
            $mitra->domisili          = $request->domisili;
            $mitra->foto_stnk         = $request->foto_stnk;
            $mitra->foto_sim          = $request->foto_sim;
            $mitra->id_bbm          = $request->id_bbm;
            $mitra->verifikasi          = $request->verifikasi;
            $mitra->status_ketersediaan          = $request->status_ketersediaan;

        $mitra->save();
        return response()->json($mitra,200);

    }

    public function getByIdPerusahaan(Request $request, $idMitra)
    {
        $getMitra = Mitra::where('id', $idMitra)->get();

        return response()->json($getMitra,200);
    }

    public function getKendaraan($id)
    {
        $getMitra = Mitra::where('mitra.id', $id)
                ->select('mitra.*', 'bbm.nama_bbm')
                ->join('bbm','bbm.id','mitra.id_bbm')
                ->get();

        return response()->json($getMitra,200);
    }

    public function verifikasiMitra($idMitra)
    {
        $mitra = Mitra::find($idMitra);

        $mitra->verifikasi = 1;
        $mitra->save();

        $resp = [
            'status' => 200,
            'dataMitra' => $mitra
        ];

        return response()->json($resp, 200);
    }

    public function updateStatusMitra($id)
    {
        $mitra = Mitra::find($id);

        $mitra->status_ketersediaan = 'On Trip';
        $mitra->save();

        return response()->json($mitra, 200);
    }

    public function getMitraByKota($asal, $tujuan)
    {
        $kotaAsal = str_replace('-', ' ', $asal);
        $kotaTujuan = str_replace('-', ' ', $tujuan);

        $tarif = DB::table('tarif')
                    // ->where('id_perusahaan', $idPerusahaan)
                    ->where('asal', $kotaTujuan)
                    // ->where('tujuan', $kotaTujuan)
                    ->get();
                    // return response()->json($tarif);
        $data = [];
        foreach($tarif as $el)
        {
            $mitra      = Mitra::find($el->id_mitra);

            $user       = DB::table('users')
                        ->where('id', $mitra->id_user)
                        ->first();

            $biodata    = DB::table('biodata')
                        ->where('id_user', $mitra->id_user)
                        ->first();

            $lobi       = DB::table('lobi_mitra')
                        ->where('id_mitra', $el->id_mitra)
                        ->count();

            $dataLobi   = DB::table('lobi_mitra')
                        ->where('id_mitra', $el->id_mitra)
                        ->get();
            

            $beratTotalLobi     = 0;
            $unitTotalLobi      = 0;
            $dimensiTotalLobi   = 0;

            if ($lobi != 0)
            {
               
                foreach ($dataLobi as $lob)
                {
                    $beratTotalLobi     += $lob->berat_total;
                    $dimensiTotalLobi   += $lob->dimensi_total;
                    $unitTotalLobi      += $lob->unit_total;
                }
            }

            $value  = [
                'mitra'             => $mitra,
                'user'              => $user,
                'biodata'           => $biodata,
                'tarif'             => $el,
                'lobi'              => $lobi,
                'berat_total_lobi'  => $beratTotalLobi,
                'dimensi_total_lobi'=> $dimensiTotalLobi,
                'unit_total_lobi'   => $unitTotalLobi
            ];
            
            
            
            array_push($data, $value);
            
        }
        
        return response()->json($data, 200);
    }

    public function getReportMitra($idPerusahaan)
    {
        $report     = ReportMitra::where('id_perusahaan', $idPerusahaan)->get();
        $totReport  = ReportMitra::where('id_perusahaan', $idPerusahaan)->count();

        $resp = [
            'report' => $report,
            'total'  => $totReport
        ];

        return response()->json($resp, 200);
    }

    public function getBbmByIdBbm($id)
    {
        $bbm   = DB::table('bbm')
                    ->where('id', $id)
                    ->first();

        return response()->json($bbm, 200);
    }

    public function getBbm()
    {
        $bbm   = DB::table('bbm')
                    ->get();

        return response()->json($bbm, 200);
    }

     public function updateKendaraanMitra(Request $request, $id)
    {
        // return response()->json($request->all());

        // Kondisi tidak update semua foto
        if ($request->foto_sim == '-' && $request->foto_stnk == '-')
        {

            $updateBiodata = Mitra::where('id_user', $id)->first();
            $updateBiodata->jenis_kendaraan     = $request->jenis_kendaraan;
            $updateBiodata->nama_kendaraan      = $request->nama_kendaraan;
            $updateBiodata->plat_kendaraan      = $request->plat_kendaraan;
            $updateBiodata->tahun_kendaraan     = $request->tahun_kendaraan;
            $updateBiodata->id_bbm     = $request->id_bbm;
            $updateBiodata->domisili            = $request->domisili;
            $updateBiodata->status_ketersediaan = $request->status_ketersediaan;
            $updateBiodata->save();

            return response()->json($updateBiodata,200);
        } 
        // Kondisi tidak update foto diri dan stnk
        else if ($request->foto_sim == '-')
        {

            $updateBiodata = Mitra::where('id_user', $id)->first();
            $updateBiodata->jenis_kendaraan     = $request->jenis_kendaraan;
            $updateBiodata->nama_kendaraan      = $request->nama_kendaraan;
            $updateBiodata->plat_kendaraan      = $request->plat_kendaraan;
            $updateBiodata->tahun_kendaraan     = $request->tahun_kendaraan;
            $updateBiodata->domisili            = $request->domisili;
            $updateBiodata->id_bbm     = $request->id_bbm;
            $updateBiodata->status_ketersediaan = $request->status_ketersediaan;
            $updateBiodata->foto_stnk            = $request->foto_stnk;
            $updateBiodata->save();

            return response()->json($updateBiodata,200);
        } 
        // Kondisi tidak update foto diri dan npwp
        else if ($request->foto_stnk == '-')
        {
            
            $updateBiodata = Mitra::where('id_user', $id)->first();
            $updateBiodata->jenis_kendaraan     = $request->jenis_kendaraan;
            $updateBiodata->nama_kendaraan      = $request->nama_kendaraan;
            $updateBiodata->plat_kendaraan      = $request->plat_kendaraan;
            $updateBiodata->tahun_kendaraan     = $request->tahun_kendaraan;
            $updateBiodata->id_bbm     = $request->id_bbm;
            $updateBiodata->domisili            = $request->domisili;
            $updateBiodata->status_ketersediaan = $request->status_ketersediaan;
            $updateBiodata->foto_sim            = $request->foto_sim;
            $updateBiodata->save();

            return response()->json($updateBiodata,200);
        } 
        // Kondisi update semua foto
        else 
        {
            
            $updateBiodata = Mitra::where('id_user', $id)->first();
            $updateBiodata->jenis_kendaraan     = $request->jenis_kendaraan;
            $updateBiodata->nama_kendaraan      = $request->nama_kendaraan;
            $updateBiodata->plat_kendaraan      = $request->plat_kendaraan;
            $updateBiodata->tahun_kendaraan     = $request->tahun_kendaraan;
            $updateBiodata->domisili            = $request->domisili;
            $updateBiodata->id_bbm     = $request->id_bbm;
            $updateBiodata->status_ketersediaan = $request->status_ketersediaan;
            $updateBiodata->foto_sim            = $request->foto_sim;
            $updateBiodata->foto_stnk            = $request->foto_stnk;
            $updateBiodata->save();

            return response()->json($updateBiodata,200);
        }
    }

}
